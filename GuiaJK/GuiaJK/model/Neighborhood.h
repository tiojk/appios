//  GuiaJK
//
//  Created by pierreabreup on 7/12/15.
//  Copyright (c) 2015 Hand Mob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AFNetworking.h"
//#import "AFHTTPRequestOperationManager.h"
//#import "AFHTTPSessionManager.h"

@interface Neighborhood : NSObject
@property (strong, nonatomic) NSString *currentNeighborhoodName;
@property (strong, nonatomic) NSString *extraNeighborhoodName;
@property (strong, nonatomic) NSString *advertisesByText;
@property (strong, nonatomic) NSString *advertisesByCategory;
@property (assign,nonatomic) NSInteger advertisesMaxDistance;
@property (strong, nonatomic) NSString *advertisesSort;
@property (assign,nonatomic) BOOL advertisesWithDelivery;
@property (assign,nonatomic) BOOL advertisesWithPromotion;
@property (assign,nonatomic) BOOL advertisesWithOpened;
@property (assign,nonatomic) BOOL advertisesWithMoreDistance;
@property (strong, nonatomic) NSString *advertisesByLatLong;
@property (assign, nonatomic) NSInteger advertiseByID;
@property (strong, nonatomic) NSArray *neighborhoodsList;
@property (strong, nonatomic) NSArray *neighborhoodCovers;
@property (strong, nonatomic) NSArray *neighborhoodCategories;
@property (strong, nonatomic) NSArray *neighborhoodAdvertises;
@property (strong, nonatomic) NSArray *neighborhoodAdvertisesAutoComplete;
@property (strong, nonatomic) NSArray *neighborhoodAdvertisesCategoriesAutoComplete;
@property (strong, nonatomic) NSArray *advertisesCategories;
@property (strong, nonatomic) NSArray *nearbyNeighborhoods;
@property (strong, nonatomic) NSDictionary *advertiseLoaded;

+ (Neighborhood *)instance;
- (void)loadCoversAndCategoriesSuccess:(void(^)(AFHTTPRequestSerializer *operation,id response))callbackSuccess
                               failure:(void(^)(AFHTTPRequestSerializer *operation,NSError *error))callbackFailure;

- (void)loadCategoriesFromCurrentLatLngSuccess:(void(^)(AFHTTPRequestSerializer *operation,id response))callbackSuccess
                                       failure:(void(^)(AFHTTPRequestSerializer *operation,NSError *error))callbackFailure;

- (NSArray *)neighborhoodCategories;

- (void)searchAdvertisesAutocompleteSuccess:(void(^)(AFHTTPRequestSerializer *operation,id response))callbackSuccess
                                    failure:(void(^)(AFHTTPRequestSerializer *operation,NSError *error))callbackFailure;

- (void)searchAdvertisesByURL:(NSString *)searchURL Success:(void(^)(AFHTTPRequestSerializer *operation,id response))callbackSuccess
                      failure:(void(^)(AFHTTPRequestSerializer *operation,NSError *error))callbackFailure;

- (void)searchAdvertisesSuccess:(void(^)(AFHTTPRequestSerializer *operation,id response))callbackSuccess
                        failure:(void(^)(AFHTTPRequestSerializer *operation,NSError *error))callbackFailure;

- (void)searchNeighborhoodAutocomplete:(NSString *)term success:(void(^)(AFHTTPRequestSerializer *operation,id response))callbackSuccess
                               failure:(void(^)(AFHTTPRequestSerializer *operation,NSError *error))callbackFailure;


@end
