//
//  GuiaJK
//
//  Created by pierreabreup on 7/12/15.
//  Copyright (c) 2015 Hand Mob. All rights reserved.
//

#import "Neighborhood.h"
#import "Constants.h"


static Neighborhood *sharedInstance = nil;

@interface Neighborhood ()

@end

@implementation Neighborhood

@synthesize currentNeighborhoodName,extraNeighborhoodName,nearbyNeighborhoods,
neighborhoodsList,advertisesByCategory,advertisesByText,advertisesByLatLong,
advertiseByID,advertiseLoaded,advertisesSort;

+ (Neighborhood *)instance{
  if (!sharedInstance){
    sharedInstance = [Neighborhood alloc];
  }
  
  return sharedInstance;
}

- (void)loadCoversAndCategoriesSuccess:(void(^)(AFHTTPRequestSerializer *operation,id response))callbackSuccess
  failure:(void(^)(AFHTTPRequestSerializer *operation,NSError *error))callbackFailure{
    
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  NSString *neighborhoodName = [self currentNeighborhoodName];
  if ([self.extraNeighborhoodName length] > 0){
    neighborhoodName = self.extraNeighborhoodName;
  }
  NSDictionary *parameters = @{@"n": [neighborhoodName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]};
  
 //NSLog(@"BEGIN loadCoversAndCategoriesSuccess REQUEST %@",URL_NEIGHBORHOOD_COVERS_AND_CATEGORIES);
  //NSLog(@"parametr %@",parameters);
  
  [manager GET:URL_NEIGHBORHOOD_COVERS_AND_CATEGORIES parameters:parameters success:^(NSURLSessionTask *operation, id responseObject) {
    NSLog(@"SUCESS loadCoversAndCategoriesSuccess REQUEST");
    
    @try {
      NSDictionary *jsonData = [NSDictionary dictionaryWithDictionary:responseObject];
      self.neighborhoodCovers     = (NSArray *)[jsonData objectForKey:@"covers"];
      self.neighborhoodCategories = (NSArray *)[jsonData objectForKey:@"categories"];
      if (!self.nearbyNeighborhoods){
        self.nearbyNeighborhoods = (NSArray *)[jsonData objectForKey:@"nearby_neighborhoods"];
      }
      
      
      if ([self.neighborhoodCovers isEqual:[NSNull null]]){
        @throw [NSException
                exceptionWithName:@"LoadCoversAndCategories"
                reason:@"neighborhoodCovers is null. Something wrong on server side"
                userInfo:nil];
      }
      if([self.neighborhoodCategories isEqual:[NSNull null]]) {
        @throw [NSException
                exceptionWithName:@"LoadCoversAndCategories"
                reason:@"neighborhoodCovers is null. Something wrong on server side"
                userInfo:nil];
      }
    }
    @catch (NSException *exception) {
      NSLog(@"Exception: %@",exception);
      responseObject = [NSNull null];
    }
    
    
    callbackSuccess(operation,responseObject);
    
  } failure:^(NSURLSessionTask *operation, NSError *error) {
    [Constants alertRequestError];
     NSLog(@"FAIL loadCoversAndCategoriesSuccess REQUEST: %@", error);
    
    callbackFailure(operation,error);
  }];
}


- (void)loadCategoriesFromCurrentLatLngSuccess:(void(^)(AFHTTPRequestSerializer *operation,id response))callbackSuccess
                               failure:(void(^)(AFHTTPRequestSerializer *operation,NSError *error))callbackFailure{
  
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  NSMutableArray *adIds = [NSMutableArray array];
  for (NSDictionary* advertise in self.neighborhoodAdvertises){
    [adIds addObject:[advertise objectForKey:@"id"]];
  }
  NSDictionary *parameters = @{@"ids": [adIds componentsJoinedByString:@","]};
  
  [manager GET:URL_CATEGORIES_BY_AD_IDS parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
    //NSLog(@"SUCESS loadCategoriesByAdIds REQUEST");
    
    @try {
      NSDictionary *jsonData = [NSDictionary dictionaryWithDictionary:responseObject];
      self.advertisesCategories = (NSArray *)[jsonData objectForKey:@"categories"];
      
      if([self.advertisesCategories isEqual:[NSNull null]]) {
        @throw [NSException
                exceptionWithName:@"loadCategoriesByAdIds"
                reason:@"advertisesCategories is null. Something wrong on server side"
                userInfo:nil];
      }
    }
    @catch (NSException *exception) {
      NSLog(@"Exception: %@",exception);
      responseObject = [NSNull null];
    }
    
    
    callbackSuccess(operation,responseObject);
    
  } failure:^(NSURLSessionTask *operation, NSError *error) {
    [Constants alertRequestError];
    NSLog(@"FAIL loadCategoriesByAdIds REQUEST: %@", error);
    
    callbackFailure(operation,error);
  }];
}

- (void)searchNeighborhoodAutocomplete:(NSString *)term success:(void(^)(AFHTTPRequestSerializer *operation,id response))callbackSuccess
                                       failure:(void(^)(AFHTTPRequestSerializer *operation,NSError *error))callbackFailure{
  
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  NSMutableArray *adIds = [NSMutableArray array];
  for (NSDictionary* advertise in self.neighborhoodAdvertises){
    [adIds addObject:[advertise objectForKey:@"id"]];
  }
  NSDictionary *parameters = @{@"n": [term stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]};
  
  [manager GET:URL_AUTOCOMPLETE_NEIGHBORHOOD parameters:parameters success:^(NSURLSessionTask *operation, id responseObject) {
    callbackSuccess(operation,responseObject);
    
  } failure:^(NSURLSessionTask *operation, NSError *error) {
    [Constants alertRequestError];
    NSLog(@"FAIL loadCategoriesByAdIds REQUEST: %@", error);
    
    callbackFailure(operation,error);
  }];
}

- (void)searchAdvertisesSuccess:(void(^)(AFHTTPRequestSerializer *operation,id response))callbackSuccess
                               failure:(void(^)(AFHTTPRequestSerializer *operation,NSError *error))callbackFailure{
  
  [self searchAdvertisesByURL:URL_SEARCH_ADVERTISES Success:callbackSuccess failure:callbackFailure];
}

- (void)searchAdvertisesAutocompleteSuccess:(void(^)(AFHTTPRequestSerializer *operation,id response))callbackSuccess
                        failure:(void(^)(AFHTTPRequestSerializer *operation,NSError *error))callbackFailure{
  
  [self searchAdvertisesByURL:URL_AUTOCOMPLETE_SEARCH_ADVERTISES Success:callbackSuccess failure:callbackFailure];
}

- (void)searchAdvertisesByURL:(NSString *)searchURL Success:(void(^)(AFHTTPRequestSerializer *operation,id response))callbackSuccess
                        failure:(void(^)(AFHTTPRequestSerializer *operation,NSError *error))callbackFailure{
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  
  
  NSString *neighborhoodName = [self currentNeighborhoodName];
  if ([self.extraNeighborhoodName length] > 0){
    neighborhoodName = self.extraNeighborhoodName;
  }
  
  NSDictionary *neighborhhodParam = @{@"n": [neighborhoodName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]};
  NSMutableDictionary  *parameters = [NSMutableDictionary dictionaryWithDictionary:neighborhhodParam];
  
  if (self.advertisesByText) {
    [parameters setValue:[self.advertisesByText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] forKey:@"t"];
  }
  if (self.advertisesByCategory) {
    [parameters setValue:[self.advertisesByCategory stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]  forKey:@"c"];
  }
  if (self.advertisesByLatLong) {
    [parameters setValue:self.advertisesByLatLong forKey:@"l"];
  }

  if (self.advertisesMaxDistance && self.advertisesMaxDistance > 0){
    [parameters setValue:[@(self.advertisesMaxDistance) stringValue] forKey:@"maxdistance"];
  }
  if (self.advertisesWithDelivery){
    [parameters setValue:@"1" forKey:@"isdelivery"];
  }
  if (self.advertisesWithPromotion){
    [parameters setValue:@"1" forKey:@"haspromotion"];
  }
  if (self.advertisesWithOpened){
    [parameters setValue:@"1" forKey:@"isopened"];
  }
  if (self.advertisesWithMoreDistance){
    [parameters setValue:@"1" forKey:@"islongdistance"];
  }
  
  if (self.advertisesSort){
    [parameters setValue:self.advertisesSort forKey:@"sort_by"];
  }
  
  if (self.advertiseByID && self.advertiseByID > 0) {
    [parameters setValue:[@(self.advertiseByID) stringValue] forKey:@"id"];
  }
  
  NSLog(@"BEGIN searchAdvertisesSuccess REQUEST %@",searchURL);
  
  [manager GET:searchURL parameters:parameters success:^(NSURLSessionTask *operation, id responseObject) {
    NSLog(@"SUCESS searchAdvertisesSuccess REQUEST");
    
    @try {
      NSDictionary *jsonData = [NSDictionary dictionaryWithDictionary:responseObject];
      if ([searchURL isEqualToString:URL_AUTOCOMPLETE_SEARCH_ADVERTISES]){
        self.neighborhoodAdvertisesAutoComplete = (NSArray *)[jsonData objectForKey:@"advertises"];
        self.neighborhoodAdvertisesCategoriesAutoComplete = (NSArray *)[jsonData objectForKey:@"categories"];
        
        
        if ([self.neighborhoodAdvertisesAutoComplete isEqual:[NSNull null]]){
          @throw [NSException
                  exceptionWithName:@"searchAdvertises"
                  reason:@"neighborhoodAdvertises is null. Something wrong on server side"
                  userInfo:nil];
        }
      }
      else{
        if (self.advertiseByID && self.advertiseByID > 0){
          NSArray *ads = (NSArray *)[jsonData objectForKey:@"advertises"];
          if ([ads count ] > 0){
            self.advertiseLoaded = (NSDictionary *)[ads objectAtIndex:0];
          }
          else{
            self.advertiseLoaded = nil;
          }
          
        }
        else{
          self.neighborhoodAdvertises = (NSArray *)[jsonData objectForKey:@"advertises"];
          
          
          if ([self.neighborhoodAdvertises isEqual:[NSNull null]]){
            @throw [NSException
                    exceptionWithName:@"searchAdvertises"
                    reason:@"neighborhoodAdvertises is null. Something wrong on server side"
                    userInfo:nil];
          }
        }
      }
      
    }
    @catch (NSException *exception) {
      NSLog(@"Exception: %@",exception);
      responseObject = [NSNull null];
    }
    
    
    callbackSuccess(operation,responseObject);
    
  } failure:^(NSURLSessionTask *operation, NSError *error) {
    [Constants alertRequestError];
    NSLog(@"FAIL searchAdvertisesSuccess REQUEST: %@", error);
    
    callbackFailure(operation,error);
  }];
}

@end
