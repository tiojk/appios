//
//  DetailView.m
//  GuiaJK
//
//  Created by pierreabreup on 8/2/15.
//  Copyright (c) 2015 Hand Mob. All rights reserved.
//

#import "DetailView.h"
#import "Neighborhood.h"
#import "Constants.h"
#import "UIImageView+WebCache.h"
#import "SearchResultView.h"
#import "Flurry.h"
#import "CommentsView.h"

#define kDirectionsURL @"https://maps.googleapis.com/maps/api/directions/json?"

#define IS_RETINA ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale >= 2.0))

@import GoogleMaps;

@interface DetailView ()
  @property NSString *lat;
  @property NSString *lng;
  @property NSString *storeName;
  @property NSString *adTitle;
  @property NSString *storeAddress;
  @property NSArray *promotions;
  @property NSString *adID;
  @property GMSMapView *gMapView;
  @property UIView *backgroundModal;
  @property UIView *contentModalView;
  @property UIView *body;
  @property BOOL isFreeAd;
  @property NSDictionary *advertise;
  @property UIScrollView *scrollView;
  @property UITextField *userNameField;
  @property UITextView *userCommentField;
  @property NSInteger selectedRate;
  @property NSMutableArray *photos;
  @property NSArray *adPhotos;
  @property MWPhotoBrowser *photoBrowser;
  @property CommentsView *commentsView;
  @property UIActivityViewController *activityViewController;
@end

@implementation DetailView
@synthesize advertiseIndex, isDetailFromID;

- (void)drawRect:(CGRect)rect {
  if (isDetailFromID){
    self.advertise = [[Neighborhood instance] advertiseLoaded];
  }
  else{
    self.advertise = [[[Neighborhood instance] neighborhoodAdvertises] objectAtIndex:advertiseIndex];
    [[Neighborhood instance] setAdvertiseLoaded:nil];
  }
  
  self.isFreeAd = YES;
  
  CGFloat marginLeft = 10;
  CGFloat marginTop = 10;
  CGFloat lastScrollContentTop = 5;
  
  self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,
                                                                   0,
                                                                   self.frame.size.width,
                                                                   self.frame.size.height)];
  [self.scrollView setShowsVerticalScrollIndicator:YES];
  [self.scrollView setShowsHorizontalScrollIndicator:NO];
  [self.scrollView setBackgroundColor:[UIColor clearColor]];
  [self.scrollView setUserInteractionEnabled:YES];
  [self addSubview:self.scrollView];
  
  if ([[self.advertise objectForKey:@"payed"] boolValue]){
    NSString *adImgeURL = [[self.advertise objectForKey:@"image"] objectForKey:@"url"];
    UIImageView *adImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.scrollView.frame.size.width, 133)];
    if ([adImgeURL isEqual:[NSNull null]]){
      [adImage setImage:[UIImage imageNamed:@"no-image"]];
    }else{
      [adImage sd_setImageWithURL:[NSURL URLWithString:adImgeURL] placeholderImage:[UIImage imageNamed:@"loading-image"]];
    }
    [self.scrollView addSubview:adImage];
    if ([[self.advertise objectForKey:@"isopened"] boolValue]){
      UIImageView *adOpen = [[UIImageView alloc] initWithFrame:CGRectMake(adImage.frame.size.width - 63, adImage.frame.size.height - 25, 53, 18.5)];
      [adOpen setImage:[UIImage imageNamed:@"ad-open"]];
      [self.scrollView addSubview:adOpen];
    }
    
    lastScrollContentTop += adImage.frame.size.height;
  }
  
  UIImageView *shareButtons = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"share-buttons"]];
  [shareButtons setUserInteractionEnabled:YES];
  [shareButtons setFrame:CGRectMake(marginLeft, lastScrollContentTop, 301, 40.5)];
  
  CGRect shareBtFrame = CGRectMake(0, 5, 97, 31);
  UIButton *whatsShare = [UIButton buttonWithType:UIButtonTypeCustom];
  [whatsShare setFrame:shareBtFrame];
  [whatsShare setImage:[UIImage imageNamed:@"blank"] forState:UIControlStateNormal];
  [whatsShare addTarget:self action:@selector(shareToWhats) forControlEvents:UIControlEventTouchUpInside];
  [shareButtons addSubview:whatsShare];
  
  shareBtFrame.origin.x = SumFrameXWidth(shareBtFrame);
  UIButton *mailShare = [UIButton buttonWithType:UIButtonTypeCustom];
  [mailShare setFrame:shareBtFrame];
  [mailShare addTarget:self action:@selector(shareToMail) forControlEvents:UIControlEventTouchUpInside];
  [shareButtons addSubview:mailShare];
  
  shareBtFrame.origin.x = SumFrameXWidth(shareBtFrame);
  UIButton *contactShare = [UIButton buttonWithType:UIButtonTypeCustom];
  [contactShare setFrame:shareBtFrame];
  [contactShare addTarget:self action:@selector(addToContact) forControlEvents:UIControlEventTouchUpInside];
  [shareButtons addSubview:contactShare];
  
  [self.scrollView addSubview:shareButtons];
  
  lastScrollContentTop += shareButtons.frame.size.height + marginTop;
  
  self.body = [[UIView alloc] initWithFrame:CGRectMake(marginLeft,
                                                       lastScrollContentTop,
                                                       shareButtons.frame.size.width,
                                                       370)];

  
  [self.body setBackgroundColor:[UIColor whiteColor]];
  self.body.layer.borderWidth = 1;
  self.body.layer.borderColor = Rgb2UIColor(213, 213, 213).CGColor;
  [self.scrollView addSubview:self.body];
  
  CGFloat lastTop = 10;
  UIColor *textColor = Rgb2UIColor(114, 114, 114);


  self.lat = [[self.advertise objectForKey:@"location"] objectForKey:@"lat"];
  self.lng = [[self.advertise objectForKey:@"location"] objectForKey:@"lon"];
  self.storeName = [[self.advertise objectForKey:@"store"] objectForKey:@"name"];
  self.storeAddress = [[self.advertise objectForKey:@"store"] objectForKey:@"address"];
  self.adID = [self.advertise objectForKey:@"id"];
  self.adTitle = [self.advertise objectForKey:@"title"];
  
  NSDictionary *context = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"Advertise Detail", @"Page",
                           self.adTitle, @"Advertise_Title",
                           [[Neighborhood instance] currentNeighborhoodName],@"Neighborhood", nil];
  
  [Flurry logEvent:@"PageView" withParameters:context];
  
  CGFloat distanceW = 52;
  CGFloat textsWidth = (self.body.frame.size.width - marginLeft - distanceW);
  
  UILabel *adTitle = [[UILabel alloc] initWithFrame:CGRectMake(marginLeft, lastTop, textsWidth, 20)];
  [adTitle setBackgroundColor:[UIColor clearColor]];
  [adTitle setNumberOfLines:3];
  [adTitle setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-Bd" size:16.f]];
  [adTitle setTextColor:textColor];
  [adTitle setText:self.adTitle];
  [self.body addSubview:adTitle];
  
  NSString *distanceText = [[self.advertise objectForKey:@"store"] objectForKey:@"distance"];
  if (![distanceText isEqualToString:@"0"]){
    UILabel *adDistance = [[UILabel alloc] initWithFrame:CGRectMake(SumFrameXWidth(adTitle.frame)+5,
                                                                      adTitle.frame.origin.y+2, distanceW, 15)];
    [adDistance setNumberOfLines:1];
    [adDistance setBackgroundColor:[UIColor clearColor]];
    [adDistance setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-BdIt" size:12.f]];
    [adDistance setTextColor:Rgb2UIColor(148, 148, 148)];
    [adDistance setText:distanceText];
    [adDistance setAdjustsFontSizeToFitWidth:YES];
    [self.body addSubview:adDistance];
  }
  
  lastTop =  (SumFrameYHeight(adTitle.frame) - 5);
  
  UILabel *categoriesNames = [[UILabel alloc] initWithFrame:CGRectMake(marginLeft, lastTop, textsWidth, 20)];
  [categoriesNames setBackgroundColor:[UIColor clearColor]];
  [categoriesNames setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.f]];
  [categoriesNames setTextColor:Rgb2UIColor(176, 176, 176)];
  [categoriesNames setText:[self.advertise objectForKey:@"categories_names"]];
  [self.body addSubview:categoriesNames];
  
  lastTop =  SumFrameYHeight(categoriesNames.frame)+marginTop;
  
  UIView *starsView = [[UIView alloc] initWithFrame:CGRectMake(adTitle.frame.origin.x, lastTop, 148.5, 24)];
  [starsView setBackgroundColor:Rgb2UIColor(240, 240, 240)];
  starsView.layer.cornerRadius = 5;
  [self.body addSubview:starsView];
  
  UIButton *starButton = [UIButton buttonWithType:UIButtonTypeCustom];
  [starButton setFrame:CGRectMake(SumFrameXWidth(starsView.frame)+10, starsView.frame.origin.y, 46, 23.5)];
  [starButton setImage:[UIImage imageNamed:@"bt-send-rate"] forState:UIControlStateNormal];
  [starButton addTarget:self action:@selector(showRateModal) forControlEvents:UIControlEventTouchUpInside];
  [self.body addSubview:starButton];
  
  CGFloat rating = [[self.advertise objectForKey:@"rating"] floatValue];
  NSArray *ratingCp = nil;
  if (rating > 0){
    ratingCp = [[self.advertise objectForKey:@"rating"] componentsSeparatedByString:@"."];
  }
  
  CGFloat starWidth = 14.5;
  CGRect lastStarFrame;
  CGFloat starX = 0;
  BOOL shouldAddHalf = NO;
  for (CGFloat x = 1; x <= 5; x++) {
    if (x == 1){
      starX = 5;
    }
    UIImageView *star = [[UIImageView alloc] initWithFrame:CGRectMake(starX, 5, starWidth, 16)];
    [starsView addSubview:star];
    
    lastStarFrame = star.frame;
    
    if (ratingCp){
      if (x < [[ratingCp objectAtIndex:0] floatValue]){
        [star setImage:[UIImage imageNamed:@"icon-star-full"]];
      }
      if (x > [[ratingCp objectAtIndex:0] floatValue]){
        [star setImage:[UIImage imageNamed:@"icon-star-empty"]];
      }
      if (shouldAddHalf){
        [star setImage:[UIImage imageNamed:@"icon-star-half"]];
        shouldAddHalf = NO;
      }
      if (x == [[ratingCp objectAtIndex:0] floatValue]){
        [star setImage:[UIImage imageNamed:@"icon-star-full"]];
        
        if ([[ratingCp objectAtIndex:1] floatValue] > 0){
          shouldAddHalf = YES;
        }
      }
      
    }
    else{
      [star setImage:[UIImage imageNamed:@"icon-star-empty"]];
    }
    
    starX = SumFrameXWidth(star.frame);
  }
  
  NSInteger totalRate = [[self.advertise objectForKey:@"rate_count"] integerValue];
  NSString *rateText = @"avaliação";
  if (totalRate > 0){
    rateText = @"avaliações";
  }
  UILabel *rateCount = [[UILabel alloc] initWithFrame:CGRectMake(SumFrameXWidth(lastStarFrame)+10, lastStarFrame.origin.y - 3, 100, starsView.frame.size.height - 2)];
  [rateCount setText:[NSString stringWithFormat:@"%ld %@", (long)totalRate, rateText]];
  [rateCount setTextColor:Rgb2UIColor(114, 114, 114)];
  [rateCount setFont:[UIFont fontWithName:@"HelveticaNeue" size:9.f]];
  [starsView addSubview:rateCount];
  
  lastTop = (SumFrameYHeight(starsView.frame)+marginTop+5);
  
  CGFloat adDetailHeight = 60;
  
  UITextView *adDetail = [[UITextView alloc] initWithFrame:CGRectMake(marginLeft, lastTop, textsWidth, adDetailHeight)];
  [adDetail setBackgroundColor:[UIColor clearColor]];
  [adDetail setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.f]];
  [adDetail setTextColor:textColor];
  [adDetail setText:[self.advertise objectForKey:@"details"]];
  [adDetail setEditable:NO];
  adDetail.textContainerInset = UIEdgeInsetsZero;
  adDetail.textContainer.lineFragmentPadding = 0;
  [self.body addSubview:adDetail];
  
  lastTop = SumFrameYHeight(adDetail.frame)+marginTop+10;
  
  NSArray *phones = [[self.advertise objectForKey:@"store"] objectForKey:@"phones"];
  
  
  NSInteger totalPhones = [phones count];
  if (totalPhones > 0){
    int callButtonHeight = 36;
    CGFloat phonesBackgoundHeight = callButtonHeight * totalPhones;
    
    UIView *phonesBackground = [[UIView alloc] initWithFrame:CGRectMake(marginLeft+15, lastTop, 233, phonesBackgoundHeight)];
    [phonesBackground setBackgroundColor:Rgb2UIColor(176, 227, 130)];
    phonesBackground.layer.cornerRadius = 5;
    phonesBackground.layer.borderWidth = 1;
    phonesBackground.layer.borderColor = Rgb2UIColor(176, 227, 130).CGColor;
    phonesBackground.clipsToBounds = YES;
    [self.body addSubview:phonesBackground];

    
    CGRect adPhoneFrame;
    UIButton *callButton;
    for (int x = 0; x < totalPhones; x++) {
      adPhoneFrame = CGRectMake(20, callButtonHeight * x, phonesBackground.frame.size.width, callButtonHeight);
      
      callButton = [UIButton buttonWithType:UIButtonTypeCustom];
      [callButton setFrame:adPhoneFrame];
      [[callButton titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-BdIt" size:15.f]];
      [callButton setTitleColor:Rgb2UIColor(100, 129, 73) forState:UIControlStateNormal];
      [callButton setTitle:[[phones objectAtIndex:x] objectForKey:@"label"] forState:UIControlStateNormal];
      [callButton setTag:600+x];
      [callButton addTarget:self action:@selector(doCall:) forControlEvents:UIControlEventTouchUpInside];
      
      UIImageView *phoneIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-phone-green"]];
      [phoneIcon setFrame:CGRectMake(40, 8, 15, 14.5)];
      [callButton addSubview:phoneIcon];
      
      [phonesBackground addSubview:callButton];
      
      if (x < (totalPhones - 1)){
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(5, SumFrameYHeight(callButton.frame), callButton.frame.size.width - 10, 1)];
        [line setBackgroundColor:Rgb2UIColor(198, 235, 165)];
        [phonesBackground addSubview:line];
      }
      
    }
    
    lastTop = SumFrameYHeight(phonesBackground.frame) + marginTop;
  }
  
  UIImageView *workHourTitle = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title-work-hour"]];
  [workHourTitle setFrame:CGRectMake(marginLeft, lastTop+5, 273, 23)];
  [self.body addSubview:workHourTitle];
  
  lastTop = SumFrameYHeight(workHourTitle.frame);
  
  UIView *workHourArea = [[UIView alloc] initWithFrame:CGRectMake(3, lastTop, workHourTitle.frame.size.width, 49)];
  [workHourArea setBackgroundColor:[UIColor clearColor]];
  
  NSArray *weekNames = [NSArray arrayWithObjects:@"SEG",@"TER",@"QUA",@"QUI",@"SEX",@"SAB",@"DOM",@"FERIADOS", nil];
  NSArray *workHours = [[self.advertise objectForKey:@"store"] objectForKey:@"work_hour"];
  NSInteger weekColumnX = 0;
  BOOL openIconAdded = NO;
  NSInteger weekNumber = [Constants currentWeekNumber];
  
  for (int x=0; x < [workHours count]; x++) {
    NSDictionary *week = [workHours objectAtIndex:x];
    if ([[week objectForKey:@"open"] length] > 0 && [[week objectForKey:@"close"] length]){
      if (openIconAdded == NO){
        UIImageView *openIconColumn = [[UIImageView alloc] initWithFrame:CGRectMake(weekColumnX, 23, 11.5, 25.5)];
        [openIconColumn setImage:[UIImage imageNamed:@"icon-open-close"]];
        [workHourArea addSubview:openIconColumn];
        weekColumnX += (SumFrameXWidth(openIconColumn.frame) - 5);
        
        openIconAdded = YES;
      }
      
      UIView *weekColumn = [[UIView alloc] initWithFrame:CGRectMake(weekColumnX, 5, 30.5, 42.5)];
      if (x == weekNumber){
        weekColumn.layer.cornerRadius = 5;
        weekColumn.layer.borderWidth = 1;
        weekColumn.layer.borderColor = Rgb2UIColor(196, 196, 196).CGColor;
        [weekColumn setBackgroundColor:Rgb2UIColor(242, 242, 242)];
        [weekColumn setFrame:CGRectMake(weekColumnX, 3, 35, 52)];
      }
      
      CGFloat weekNameW = 26.5;
      if ([[weekNames objectAtIndex:x] length] > 3){
        weekNameW = 53;
      }
      UILabel *weekname = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, weekNameW, 10)];
      [weekname setText:[weekNames objectAtIndex:x]];
      [weekname setBackgroundColor:[UIColor clearColor]];
      [weekname setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-Bd" size:9.f]];
      [weekname setTextColor:Rgb2UIColor(83, 83, 83)];
      [weekname setAdjustsFontSizeToFitWidth:YES];
      [weekColumn addSubview:weekname];
      
      UILabel *open = [[UILabel alloc] initWithFrame:CGRectMake(5, SumFrameYHeight(weekname.frame)+2, 26.5, 10)];
      [open setText:[[week objectForKey:@"open"] stringByReplacingOccurrencesOfString:@":" withString:@"h"]];
      [open setBackgroundColor:[UIColor clearColor]];
      [open setFont:[UIFont fontWithName:@"HelveticaNeue" size:9.f]];
      [open setTextColor:Rgb2UIColor(114, 114, 114)];
      [open setAdjustsFontSizeToFitWidth:YES];
      [weekColumn addSubview:open];
      
      UIView *line = [[UIView alloc] initWithFrame:CGRectMake(5, SumFrameYHeight(open.frame)+5, open.frame.size.width, 2)];
      [line setBackgroundColor:Rgb2UIColor(196, 196, 196)];
      [weekColumn addSubview:line];
      
      UILabel *close = [[UILabel alloc] initWithFrame:CGRectMake(5, SumFrameYHeight(line.frame)+3, 26.5, 10)];
      [close setText:[[week objectForKey:@"close"] stringByReplacingOccurrencesOfString:@":" withString:@"h"]];
      [close setBackgroundColor:[UIColor clearColor]];
      [close setFont:[UIFont fontWithName:@"HelveticaNeue" size:9.f]];
      [close setTextColor:Rgb2UIColor(114, 114, 114)];
      [close setAdjustsFontSizeToFitWidth:YES];
      [weekColumn addSubview:close];
       
      
      [workHourArea addSubview:weekColumn];
      weekColumnX += (weekColumn.frame.size.width + 3);
    }
  }
  
  [self.body addSubview:workHourArea];
  
  lastTop = SumFrameYHeight(self.body.frame) + 10;
  
  self.promotions = [self.advertise objectForKey:@"promotions"];
  
  UIImageView *promotionHeaderTitle;
  if ([self.promotions count] > 0){
    promotionHeaderTitle = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title-promotion"]];
    [promotionHeaderTitle setFrame:CGRectMake(marginLeft, lastTop, 292, 27)];
    [self.scrollView addSubview:promotionHeaderTitle];
    
    lastTop = SumFrameYHeight(promotionHeaderTitle.frame)+10;
  }
  
  CGFloat boxPromotionH = 98;
  for (int x=0; x < [self.promotions count]; x++) {
    NSDictionary *promotion = [self.promotions objectAtIndex:x];
    NSString *promotionImageURL = [promotion objectForKey:@"image"];
    
    UIView *boxPromotion = [[UIView alloc] initWithFrame:CGRectMake(marginLeft, lastTop, promotionHeaderTitle.frame.size.width, boxPromotionH)];
    [boxPromotion setBackgroundColor:Rgb2UIColor(253, 236, 123)];
    
    UIImageView *promotionImage = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 88, 82)];
    promotionImage.layer.cornerRadius = 5;
    promotionImage.clipsToBounds = YES;
    if ([promotionImageURL isEqual:[NSNull null]]){
      [promotionImage setImage:[UIImage imageNamed:@"no-image"]];
    }else{
      [promotionImage sd_setImageWithURL:[NSURL URLWithString:promotionImageURL] placeholderImage:[UIImage imageNamed:@"loading-image"]];
    }
    [boxPromotion addSubview:promotionImage];
    
    
    UILabel *promoTitle = [[UILabel alloc] initWithFrame:CGRectMake(SumFrameXWidth(promotionImage.frame)+5, 8, 185, 14)];
    [promoTitle setNumberOfLines:1];
    [promoTitle setBackgroundColor:[UIColor clearColor]];
    [promoTitle setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-Bd" size:14.f]];
    [promoTitle setTextColor:Rgb2UIColor(162, 78, 3)];
    [promoTitle setAdjustsFontSizeToFitWidth:YES];
    [promoTitle setText:[promotion objectForKey:@"title"]];
    [boxPromotion addSubview:promoTitle];
    
    UILabel *promotioText = [[UILabel alloc] initWithFrame:CGRectMake(
                                                                      promoTitle.frame.origin.x,
                                                                      SumFrameYHeight(promoTitle.frame),
                                                                      promoTitle.frame.size.width, 31)];
    [promotioText setNumberOfLines:3];
    [promotioText setBackgroundColor:[UIColor clearColor]];
    [promotioText setFont:[UIFont fontWithName:@"HelveticaNeue" size:10.f]];
    [promotioText setTextColor:Rgb2UIColor(162, 78, 3)];
    [promotioText setAdjustsFontSizeToFitWidth:YES];
    [promotioText setText:[promotion objectForKey:@"text"]];
    [boxPromotion addSubview:promotioText];
    
    
    UILabel *promotioPrice = [[UILabel alloc] initWithFrame:CGRectMake(
                                                                       promoTitle.frame.origin.x,
                                                                       SumFrameYHeight(promotioText.frame) + 10,
                                                                       110, 23)];
    [promotioPrice setNumberOfLines:1];
    [promotioPrice setBackgroundColor:Rgb2UIColor(255, 162, 0)];
    [promotioPrice setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-Bd" size:14.f]];
    [promotioPrice setTextColor:[UIColor whiteColor]];
    [promotioPrice setAdjustsFontSizeToFitWidth:YES];
    promotioPrice.layer.cornerRadius = 5;
    promotioPrice.clipsToBounds = YES;
    [promotioPrice setText:[NSString stringWithFormat:@" %@", [promotion objectForKey:@"price"]]];
    [boxPromotion addSubview:promotioPrice];
    
    
    UIButton *btConditions = [UIButton buttonWithType:UIButtonTypeCustom];
    [btConditions setFrame:CGRectMake(SumFrameXWidth(promotioPrice.frame) + 20, promotioPrice.frame.origin.y, 48, 23.5)];
    [btConditions setBackgroundImage:[UIImage imageNamed:@"bt-show-conditions"] forState:UIControlStateNormal];
    [btConditions addTarget:self action:@selector(showPromotionCondition:) forControlEvents:UIControlEventTouchUpInside];
    [btConditions setTag:800+x];
    [boxPromotion addSubview:btConditions];
    
    
    [self.scrollView addSubview:boxPromotion];
    
    lastTop += (boxPromotion.frame.size.height + 10);
    
  }
  
//  if ([self.promotions count] > 1){
//    lastTop = (lastTop - boxPromotionH);
//  }
  
  
  UIImageView *mapTitle = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title-ad-map"]];
  [mapTitle setFrame:CGRectMake(marginLeft, lastTop, 292, 24)];
  [self.scrollView addSubview:mapTitle];
  
  lastTop = SumFrameYHeight(mapTitle.frame);
  
  GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[self.lat doubleValue]
                                                          longitude: [self.lng doubleValue]
                                                               zoom:17];
  
  self.gMapView = [GMSMapView mapWithFrame:CGRectMake(marginLeft, lastTop,
                                                      mapTitle.frame.size.width,
                                                      111.5) camera:camera];
  self.gMapView.myLocationEnabled = YES;
  self.gMapView.settings.myLocationButton = YES;
  
  GMSMarker *marker = [[GMSMarker alloc] init];
  marker.position = CLLocationCoordinate2DMake([self.lat doubleValue], [self.lng doubleValue]);
  marker.title = self.storeName;
  marker.snippet = self.storeAddress;
  marker.map = self.gMapView;
  
  [self.scrollView addSubview:self.gMapView];
  
  lastTop = SumFrameYHeight(self.gMapView.frame);
  
  UIView *addressBackground = [[UIView alloc] initWithFrame:CGRectMake(marginLeft, lastTop, mapTitle.frame.size.width, 50)];
  [addressBackground setBackgroundColor:[UIColor whiteColor]];
  
  UILabel *address = [[UILabel alloc] initWithFrame:CGRectMake(marginLeft, 5, 184, 30)];
  [address setNumberOfLines:2];
  [address setBackgroundColor:[UIColor clearColor]];
  [address setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-LtIt" size:12.f]];
  [address setTextColor:textColor];
  [address setAdjustsFontSizeToFitWidth:YES];
  [address setText:self.storeAddress];
  [addressBackground addSubview:address];
  
  UIButton *traceRoute = [UIButton buttonWithType:UIButtonTypeCustom];
  [traceRoute setFrame:CGRectMake(SumFrameXWidth(address.frame), address.frame.origin.y, 93.5, 30)];
  [traceRoute setBackgroundImage:[UIImage imageNamed:@"bt-trace-route"] forState:UIControlStateNormal];
  [traceRoute addTarget:self action:@selector(drawRoute) forControlEvents:UIControlEventTouchUpInside];
  [addressBackground addSubview:traceRoute];
  
  [self.scrollView addSubview:addressBackground];
  
  lastTop = SumFrameYHeight(addressBackground.frame)+15;
  
  
  self.adPhotos = [self.advertise objectForKey:@"photos"];
  
  if ([self.adPhotos count] > 0){
    UIImageView *titleGallery = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title-gallery"]];
    [titleGallery setFrame:CGRectMake(marginLeft, lastTop, 289, 22)];
    [self.scrollView addSubview:titleGallery];
    
    lastTop = SumFrameYHeight(titleGallery.frame)+5;
    
    
    self.photos = [NSMutableArray array];
    for (NSInteger i=0; i < [self.adPhotos count]; i++) {
      [self.photos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[[self.adPhotos objectAtIndex:i] objectForKey:@"image"]]]];
    }
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    UICollectionView *gallery = [[UICollectionView alloc] initWithFrame:CGRectMake(titleGallery.frame.origin.x,
                                                                        lastTop,
                                                                        titleGallery.frame.size.width,
                                                                        121.5)
                                        collectionViewLayout:flowLayout];
    
    [gallery setBackgroundColor:[UIColor clearColor]];
    [gallery setShowsVerticalScrollIndicator:NO];
    [gallery setShowsHorizontalScrollIndicator:YES];
    [gallery setUserInteractionEnabled:YES];
    [gallery setDataSource:self];
    [gallery setDelegate:self];
    [gallery registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"photoGalleryCell"];
    [self.scrollView addSubview:gallery];
    
    lastTop = SumFrameYHeight(gallery.frame)+10;
  }
  
  NSArray *comments = [self.advertise objectForKey:@"comments"];
  UIImageView *titleComment = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title-comments"]];
  [titleComment setFrame:CGRectMake(marginLeft, lastTop, 290, 22)];
  [self.scrollView addSubview:titleComment];
  
  lastTop = SumFrameYHeight(titleComment.frame)+10;
  if ([comments count] > 0){
    CGFloat commentsViewHeight = ([comments count] * [CommentsView cellHeight]) + 20;
    self.commentsView = [[CommentsView alloc] initWithFrame:CGRectMake(titleComment.frame.origin.x, lastTop, titleComment.frame.size.width, commentsViewHeight)];
    [self.commentsView setBackgroundColor:[UIColor clearColor]];
    [self.commentsView setComments:comments];
    [self.scrollView addSubview:self.commentsView];
    lastTop = SumFrameYHeight(self.commentsView.frame);
  }
  
  UIButton *sendRateBlueButton = [UIButton buttonWithType:UIButtonTypeCustom];
  [sendRateBlueButton setFrame:CGRectMake(titleComment.frame.origin.x, lastTop, 287, 32)];
  [sendRateBlueButton setImage:[UIImage imageNamed:@"bt-send-rate-blue"] forState:UIControlStateNormal];
  [sendRateBlueButton addTarget:self action:@selector(showRateModal) forControlEvents:UIControlEventTouchUpInside];
  [self.scrollView addSubview:sendRateBlueButton];
  
  lastTop = SumFrameYHeight(sendRateBlueButton.frame);
  
  [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, lastTop+100)];
  
}

- (NSString *)advertiseShareText{
  NSMutableArray *text = [NSMutableArray array];
  [text addObject:self.adTitle];
  
  NSArray *phones = [[self.advertise objectForKey:@"store"] objectForKey:@"phones"];
  if (phones){
    NSMutableArray *phonesNumbers = [NSMutableArray array];
    for (int x = 0; x < [phones count]; x++) {
      [phonesNumbers addObject:[[phones objectAtIndex:x] objectForKey:@"label"]];
    }
    [text addObject:[phonesNumbers componentsJoinedByString:@", "]];
  }
  
  
  if (self.storeAddress && [self.storeAddress length] > 0){
    [text addObject:self.storeAddress];
  }
  
  [text addObject:@"enviado atraés do aplicativo O JK. Baixe grátis em https://itunes.apple.com/br/app/o-jk-revista-de-bairro/id1174112158?l=en&mt=8"];
  
  return [text componentsJoinedByString:@" - "];
}


- (void)shareToWhats{
  if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
    [Constants alertWithMessage:@"Recurso disponível somente para iPhone"];
    return;
  }
  self.activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[[self advertiseShareText]] applicationActivities:nil];
  [(UIViewController *)[[self nextResponder] nextResponder] presentViewController:self.activityViewController animated:YES completion:nil];
}

- (void)shareToMail{
  self.activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[[self advertiseShareText]] applicationActivities:nil];
  [(UIViewController *)[[self nextResponder] nextResponder] presentViewController:self.activityViewController animated:YES completion:nil];
}

- (void)backContactController{
  [[(UIViewController *)[[self nextResponder] nextResponder] navigationController] popViewControllerAnimated:YES];
}
- (void)addToContact{
  if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
    [Constants alertWithMessage:@"Recurso disponível somente para iPhone"];
    return;
  }
  NSArray *phones = [[self.advertise objectForKey:@"store"] objectForKey:@"phones"];
  
  if ([phones count] == 0){
    [Constants alertWithMessage:@"Sem telefones para adicionar"];
    return;
  }
  
  CNContactStore *store = [[CNContactStore alloc] init];
  CNMutableContact *contact = [[CNMutableContact alloc] init];
  contact.givenName = self.adTitle;
  
  CNLabeledValue *homePhone = [CNLabeledValue labeledValueWithLabel:CNLabelHome value:[CNPhoneNumber phoneNumberWithStringValue:[[phones objectAtIndex:0] objectForKey:@"call"]]];
  contact.phoneNumbers = @[homePhone];
  
  CNContactViewController *controller = [CNContactViewController viewControllerForUnknownContact:contact];
  controller.contactStore = store;
  controller.allowsEditing = NO;
  controller.delegate = self;
  UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
  [backButton setBackgroundImage:[UIImage imageNamed:@"icon-back"] forState:UIControlStateNormal];
  [backButton setFrame:CGRectMake(0, 0, 38.5, 35.5)];
  [backButton addTarget:self action:@selector(backContactController) forControlEvents:UIControlEventTouchUpInside];
  
  controller.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
  
  [[(UIViewController *)[[self nextResponder] nextResponder] navigationController] pushViewController:controller animated:YES];
}

- (void)showPromotionCondition:(id)sender{
  CGRect windowFrame = [[[UIApplication sharedApplication] keyWindow] frame];
  
  UIView *content = [[UIView alloc] init];
  
  CGFloat headerW = 250.5;
  CGFloat headerX = (windowFrame.size.width - 20 - headerW) / 2;
  UIImageView *header = [[UIImageView alloc] initWithFrame:CGRectMake(headerX, 10, headerW, 22)];
  [header setImage:[UIImage imageNamed:@"title-conditions"]];
  [content addSubview:header];
  
  NSInteger index = ([sender tag] - 800);
  if (index  < [self.promotions count]){
    CGFloat conditionsHeight = 400;
    UITextView *conditions = [[UITextView alloc] initWithFrame:CGRectMake(header.frame.origin.x - 5, SumFrameYHeight(header.frame), header.frame.size.width, conditionsHeight)];
    [conditions setEditable:NO];
    [conditions setBackgroundColor:[UIColor clearColor]];
    [conditions setFont:[UIFont fontWithName:@"HelveticaNeueLTStd" size:14.f]];
    [conditions setTextColor:Rgb2UIColor(81, 79, 79)];
    [conditions setText:[[self.promotions objectAtIndex:index] objectForKey:@"conditions"]];
    [content addSubview:conditions];
    
    [self showContentModal:content];
  }
  
}

- (void)showRateModal{
  self.selectedRate = 0;
  
  CGRect windowFrame = [[[UIApplication sharedApplication] keyWindow] frame];
  
  UIView *content = [[UIView alloc] init];
  
  CGFloat headerW = 250.5;
  CGFloat headerX = (windowFrame.size.width - 20 - headerW) / 2;
  UIImageView *header = [[UIImageView alloc] initWithFrame:CGRectMake(headerX, 10, headerW, 17.5)];
  [header setImage:[UIImage imageNamed:@"title-rate"]];
  [content addSubview:header];
  
  UIView *starsBg = [[UIView alloc] initWithFrame:CGRectMake(header.frame.origin.x,
                                                             SumFrameYHeight(header.frame)+10,
                                                             header.frame.size.width, 42.5)];
  [starsBg setBackgroundColor:Rgb2UIColor(240, 240, 240)];
  [starsBg setTag:BG_STARS_VIEW];
  starsBg.layer.cornerRadius = 5;
  [content addSubview:starsBg];
  
  CGFloat starX = 0;
  for (CGFloat x = 1; x <= 5; x++) {
    if (x == 1){
      starX = 15;
    }
    UIButton *star = [UIButton buttonWithType:UIButtonTypeCustom];
    [star setBackgroundImage:[UIImage imageNamed:@"icon-start-empty-big"] forState:UIControlStateNormal];
    [star setFrame:CGRectMake(starX, 5, 28, 27)];
    [star addTarget:self action:@selector(changeStarsImage:) forControlEvents:UIControlEventTouchUpInside];
    [star setTag:900+x];
    [starsBg addSubview:star];
    starX = SumFrameXWidth(star.frame)+15;
  }
  
  CGRect headerFrame = header.frame;
  headerFrame.origin.y = SumFrameYHeight(starsBg.frame)+10;
  UIImageView *commentHeader = [[UIImageView alloc] initWithFrame:headerFrame];
  [commentHeader setImage:[UIImage imageNamed:@"title-comment"]];
  [content addSubview:commentHeader];
  
  CGRect fieldFrame = commentHeader.frame;
  fieldFrame.origin.y = SumFrameYHeight(commentHeader.frame)+10;
  fieldFrame.size.height = 36;
  self.userNameField = [[UITextField alloc] initWithFrame:fieldFrame];
  [self.userNameField setBackgroundColor:Rgb2UIColor(240, 240, 240)];
  [self.userNameField setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.f]];
  [self.userNameField setTextColor:Rgb2UIColor(157, 156, 156)];
  [self.userNameField setReturnKeyType:UIReturnKeyDone];
  [self.userNameField setDelegate:self];
  [self.userNameField setText:@"  Nome"];
  self.userNameField.layer.cornerRadius = 5;
  [content addSubview:self.userNameField];
  
  fieldFrame = commentHeader.frame;
  fieldFrame.origin.y = SumFrameYHeight(self.userNameField.frame)+10;
  fieldFrame.size.height = 100;
  
  self.userCommentField = [[UITextView alloc] initWithFrame:fieldFrame];
  [self.userCommentField setBackgroundColor:Rgb2UIColor(240, 240, 240)];
  [self.userCommentField setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.f]];
  [self.userCommentField setEditable:YES];
  [self.userCommentField setText:@"Comentário"];
  [self.userCommentField setReturnKeyType:UIReturnKeyDone];
  [self.userCommentField setTextColor:Rgb2UIColor(157, 156, 156)];
  [self.userCommentField setDelegate:self];
  self.userCommentField.layer.cornerRadius = 5;
  [content addSubview:self.userCommentField];
  
  UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
  [sendButton setBackgroundImage:[UIImage imageNamed:@"bt-send-rate-form"] forState:UIControlStateNormal];
  [sendButton setFrame:CGRectMake(self.userCommentField.frame.origin.x, SumFrameYHeight(self.userCommentField.frame)+10, 253, 33.5)];
  [sendButton addTarget:self action:@selector(sendRate) forControlEvents:UIControlEventTouchUpInside];
  [content addSubview:sendButton];
  
  [self showContentModal:content];
  
  CGRect contentModalViewFrame = self.contentModalView.frame;
  contentModalViewFrame.origin.y = 80;
  contentModalViewFrame.size.height += 50;
  [self.contentModalView setFrame:contentModalViewFrame];
}

- (void)sendRate{
  if (self.selectedRate == 0){
    [Constants alertWithMessage:@"Informe quantas estrelas é este lugar merece."];
    return;
  }
  NSString *userName = self.userNameField.text;
  NSString *userComment = self.userCommentField.text;
  
  if ([userName isEqualToString:@"  Nome"]){
    userName = @"";
  }
  if ([userComment isEqualToString:@"Comentário"]){
    userComment = @"";
  }
  
  if (userName.length > 0 && userComment.length == 0){
    [Constants alertWithMessage:@"Escreva seu comentário sobre este local."];
    return;
  }
  if (userName.length == 0 && userComment.length > 0){
    [Constants alertWithMessage:@"Informe seu nome."];
    return;
  }
  
  [Constants showIndicatorAtView:self];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  NSDictionary *parameters = @{@"score":[NSString stringWithFormat: @"%ld", (long)self.selectedRate],
                               @"id":self.adID,
                               @"user_name":userName,
                               @"user_comment":userComment,
                               };
  
    [manager POST:URL_RATE parameters:parameters progress: nil success:^(NSURLSessionTask *operation, id responseObject) {
    [Constants hideIndicator];
    [Constants alertWithMessage:@"Avaliação enviada com sucesso!"];
    [self closeContentModal];
    
  } failure:^(NSURLSessionTask *operation, NSError *error) {
    [Constants hideIndicator];
    [Constants alertWithMessage:@"Ocorreu um problema ao tentar enviar a avaliação. Tente mais tarde."];
    [self closeContentModal];
  }];
}

- (void)showContentModal:(UIView *)content{
  CGRect windowFrame = [[[UIApplication sharedApplication] keyWindow] frame];
  
  self.backgroundModal = [[UIView alloc] initWithFrame:CGRectMake(0, 0, windowFrame.size.width, windowFrame.size.height)];
  [self.backgroundModal setBackgroundColor:[UIColor blackColor]];
  [self.backgroundModal setAlpha:0.7];
  [self.backgroundModal setTag:2000];
  [[(UIViewController *)[[self nextResponder] nextResponder] view] addSubview:self.backgroundModal];
  
  UITapGestureRecognizer *singleBackgroundModalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleBackgroundModalTap:)];
  [[(UIViewController *)[[self nextResponder] nextResponder] view] addGestureRecognizer:singleBackgroundModalTap];
  
  self.contentModalView = content;
  [self.contentModalView setFrame:CGRectMake(10, (self.backgroundModal.frame.size.height / 2) - 100, windowFrame.size.width - 20, (self.backgroundModal.frame.size.height / 2))];
  [self.contentModalView setBackgroundColor:[UIColor whiteColor]];
  
  [[(UIViewController *)[[self nextResponder] nextResponder] view] addSubview:self.contentModalView];
  [UIView animateWithDuration:0.50 animations:^{self.contentModalView.alpha = 1;}];
}

- (void)closeContentModal{
  if (self.contentModalView){
    [UIView animateWithDuration:0.50 animations:^{self.contentModalView.alpha = 0;}];
    [UIView animateWithDuration:0.50 animations:^{self.backgroundModal.alpha = 0;}];
    [self performSelector:@selector(resetContentModal) withObject:self afterDelay:1];
  }
}

- (void)resetContentModal{
  [self.contentModalView removeFromSuperview];
  [self.backgroundModal removeFromSuperview];
  self.backgroundModal = nil;
  self.contentModalView = nil;
}

- (void)doCall:(id)sender{

  NSInteger index = ([sender tag] - 600);
  NSArray *phones = [[self.advertise objectForKey:@"store"] objectForKey:@"phones"];
  if (index < [phones count]){
    NSString *toCall = [[phones objectAtIndex:index] objectForKey:@"call"];
    NSString *phoneNumber = [@"tel://" stringByAppendingString:toCall];
    
    NSDictionary *context = [NSDictionary dictionaryWithObjectsAndKeys:
                             toCall, @"Advertise_number",
                             self.adTitle, @"Advertise_Title",
                             [[Neighborhood instance] currentNeighborhoodName],@"Neighborhood",nil];
    
    [Flurry logEvent:@"CallToAdvertise" withParameters:context];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
  }
}


- (void)drawRoute{
  NSDictionary *context = [NSDictionary dictionaryWithObjectsAndKeys:
                           self.adTitle, @"Advertise_Title",
                           [[Neighborhood instance] currentNeighborhoodName],@"Neighborhood",nil];
  [Flurry logEvent:@"MapRouteToAdvertise" withParameters:context];
  
  UIView *content = [[UIView alloc] init];
  
  CGFloat headerW = 250.5;
  UIImageView *header = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, headerW, 12)];
  [header setImage:[UIImage imageNamed:@"title-navigation"]];
  [content addSubview:header];
  
  UIButton *wazeBt = [UIButton buttonWithType:UIButtonTypeSystem];
  [wazeBt setTitle:@"        Waze" forState:UIControlStateNormal];
  [wazeBt setTitleColor:Rgb2UIColor(114, 114, 114) forState:UIControlStateNormal];
  [wazeBt setFrame:CGRectMake(10, SumFrameYHeight(header.frame)+10, 200, 75)];
  [wazeBt addTarget:self action:@selector(nagivateWithWaze) forControlEvents:UIControlEventTouchUpInside];
  [content addSubview:wazeBt];
  
  UIImageView *wazeIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 65, 65)];
  [wazeIcon setImage:[UIImage imageNamed:@"icon-waze"]];
  [wazeBt addSubview:wazeIcon];
  
  UIButton *gmapBt = [UIButton buttonWithType:UIButtonTypeSystem];
  [gmapBt setTitle:@"              Google Maps" forState:UIControlStateNormal];
  [gmapBt setTitleColor:Rgb2UIColor(114, 114, 114) forState:UIControlStateNormal];
  [gmapBt setFrame:CGRectMake(10, SumFrameYHeight(wazeBt.frame)+20, 200, 75)];
  [gmapBt addTarget:self action:@selector(nagivateWithGmaps) forControlEvents:UIControlEventTouchUpInside];
  [content addSubview:gmapBt];
  
  UIImageView *gmapsIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 65, 63)];
  [gmapsIcon setImage:[UIImage imageNamed:@"icon-google-maps"]];
  [gmapBt addSubview:gmapsIcon];
  
  [self showContentModal:content];

}

- (void)nagivateWithWaze{
  [self closeContentModal];
  
  NSString *url = [NSString stringWithFormat:@"waze://?ll=%@,%@&navigate=yes",self.lat, self.lng];
  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (void)nagivateWithGmaps{
  [self closeContentModal];
  
  CLLocation *userLocation = [self.gMapView myLocation];
  NSString *url = [NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%@,%@&directionsmode=driving",userLocation.coordinate.latitude,userLocation.coordinate.longitude,self.lat, self.lng];
  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (void)handleBackgroundModalTap:(UITapGestureRecognizer *)recognizer {
  UIView* view = recognizer.view;
  CGPoint loc = [recognizer locationInView:view];
  UIView* subview = [view hitTest:loc withEvent:nil];
  if (subview.tag == self.backgroundModal.tag){
    [self closeContentModal];
  }
  
}

- (void)changeStarsImage:(id)sender{
  UIView *bgStars = [self.contentModalView viewWithTag:BG_STARS_VIEW];
  for (UIView *v in [bgStars subviews]) {
    if ([v tag] > 900 && [v tag] < 906){
      if ([v tag] <= [sender tag]){
        [(UIButton *)v setBackgroundImage:[UIImage imageNamed:@"icon-star-full-big"] forState:UIControlStateNormal];
        self.selectedRate = ([v tag] - 900);
      }
      else{
        [(UIButton *)v setBackgroundImage:[UIImage imageNamed:@"icon-start-empty-big"] forState:UIControlStateNormal];
      }
    }
  }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
  if ([textField.text isEqualToString:@"  Nome"]) {
    textField.text = @"";
  }
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
  if ([textView.text isEqualToString:@"Comentário"]) {
    textView.text = @"";
  }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
  [textField resignFirstResponder];
  return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
  if([text isEqualToString:@"\n"]) {
    [textView resignFirstResponder];
    return NO;
  }
  
  return YES;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
  return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  return [self.adPhotos count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
  return CGSizeMake(112.5, 112.5);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoGalleryCell" forIndexPath:indexPath];
  
  NSString *photoURL = [[self.adPhotos objectAtIndex:indexPath.row] objectForKey:@"image"];
  UIImageView *photo = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
  [photo sd_setImageWithURL:[NSURL URLWithString:photoURL] placeholderImage:[UIImage imageNamed:@"loading-image"]];
  photo.layer.cornerRadius = 5;
  photo.layer.borderWidth = 2;
  photo.layer.borderColor = Rgb2UIColor(255, 255, 255).CGColor;
  photo.clipsToBounds = YES;
  [cell.contentView addSubview:photo];
  
  
  return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
  self.photoBrowser = [[MWPhotoBrowser alloc] initWithDelegate:self];
  
  // Set options
  self.photoBrowser.displayActionButton = YES;
  self.photoBrowser.displayNavArrows = YES;
  self.photoBrowser.displaySelectionButtons = NO;
  self.photoBrowser.zoomPhotosToFill = YES;
  self.photoBrowser.alwaysShowControls = YES;
  self.photoBrowser.enableGrid = NO;
  self.photoBrowser.startOnGrid = NO;
  self.photoBrowser.autoPlayOnAppear = NO;

  // Optionally set the current visible photo before displaying
  [self.photoBrowser setCurrentPhotoIndex:indexPath.row];
  
  UIButton *btClosePhotoBrowser = [UIButton buttonWithType:UIButtonTypeCustom];
  [btClosePhotoBrowser setFrame:CGRectMake(0, 20, 32, 32)];
  [btClosePhotoBrowser setImage:[UIImage imageNamed:@"icon-close"] forState:UIControlStateNormal];
  [btClosePhotoBrowser addTarget:self action:@selector(closePhotoBroswer) forControlEvents:UIControlEventTouchUpInside];
  [self.photoBrowser.view addSubview:btClosePhotoBrowser];
  
  
  [(UIViewController *)[[self nextResponder] nextResponder] presentViewController:self.photoBrowser animated:YES completion:nil];
  
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
  return self.photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
  if (index < self.photos.count) {
    return [self.photos objectAtIndex:index];
  }
  return nil;
}

- (void)closePhotoBroswer{
  [self.photoBrowser dismissViewControllerAnimated:YES completion:^{
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
  }];
}

- (void)contactViewController:(CNContactViewController *)viewController didCompleteWithContact:(CNContact *)contact{
  [self backContactController];
}
@end
