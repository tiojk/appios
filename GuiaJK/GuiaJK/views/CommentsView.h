//
//  CommentsView.h
//  GuiaJK
//
//  Created by pierreabreup on 6/19/16.
//  Copyright © 2016 Hand Mob. All rights reserved.
//

#import <UIKit/UIKit.h>
#define COMMENT_USER_NAME 300
#define COMMENT_DATE 301
#define COMMENT_TEXT 302
#define COMMENT_STARS 303

@interface CommentsView : UIView <UICollectionViewDataSource, UICollectionViewDelegate>
  @property (strong,nonatomic) NSArray *comments;
  @property (strong,nonatomic) UICollectionView *collection;

  - (void) appendComment:(NSDictionary *)comment;
+ (CGFloat) cellHeight;
@end
