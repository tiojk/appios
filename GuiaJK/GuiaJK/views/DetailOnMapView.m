//
//  DetailOnMapView.m
//  GuiaJK
//
//  Created by pierreabreup on 6/25/16.
//  Copyright © 2016 Hand Mob. All rights reserved.
//

#import "DetailOnMapView.h"
#import "Neighborhood.h"
#import "Constants.h"
#import "UIImageView+WebCache.h"
#import "DetailViewController.h"

@interface DetailOnMapView ()
  @property  UICollectionView *collectionView;
@end

@implementation DetailOnMapView
@synthesize advertiseIndex;

- (void)drawRect:(CGRect)rect {
  self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,0,self.frame.size.width,self.frame.size.height)
                                           collectionViewLayout:[[UICollectionViewFlowLayout alloc] init]];
  
  [self.collectionView setBackgroundColor:[UIColor clearColor]];
  [self.collectionView setShowsVerticalScrollIndicator:YES];
  [self.collectionView setShowsHorizontalScrollIndicator:NO];
  [self.collectionView setUserInteractionEnabled:YES];
  [self.collectionView setDataSource:self];
  [self.collectionView setDelegate:self];
  [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"detailOnMapCell"];
  [self addSubview:self.collectionView];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
  return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
  if ([self isPayedAdvertise]){
    return CGSizeMake(collectionView.frame.size.width, 98);
  }
  
  return CGSizeMake(collectionView.frame.size.width, 45);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"detailOnMapCell" forIndexPath:indexPath];
  
  cell.backgroundColor = [UIColor whiteColor];
  UIColor *textColor = Rgb2UIColor(114, 114, 114);
  
  if (![cell.contentView viewWithTag:AD_TITLE_VIEW]){
    UIImageView *adImage = [[UIImageView alloc] init];
    adImage.layer.cornerRadius = 5;
    adImage.clipsToBounds = YES;
    [adImage setTag:AD_IMAGE_VIEW];
    [cell.contentView addSubview:adImage];
    
    UIImageView *adOpen = [[UIImageView alloc] init];
    [adOpen setImage:[UIImage imageNamed:@"ad-open"]];
    [adOpen setHidden:YES];
    [adOpen setTag:AD_OPEN_VIEW];
    [cell.contentView addSubview:adOpen];
    
    UILabel *adTitle = [[UILabel alloc] init];
    [adTitle setBackgroundColor:[UIColor clearColor]];
    [adTitle setTag:AD_TITLE_VIEW];
    [adTitle setNumberOfLines:1];
    [adTitle setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-Hv" size:13.f]];
    [adTitle setTextColor:Rgb2UIColor(70, 68, 68)];
    [cell.contentView addSubview:adTitle];
    
    UILabel *adDetails = [[UILabel alloc] init];
    [adDetails setBackgroundColor:[UIColor clearColor]];
    [adDetails setTag:AD_DETAIL_VIEW];
    [adDetails setNumberOfLines:1];
    [adDetails setFont:[UIFont fontWithName:@"HelveticaNeue" size:9.f]];
    [adDetails setTextColor:textColor];
    [cell.contentView addSubview:adDetails];
    
    UIImageView *stars = [[UIImageView alloc] init];
    [stars setBackgroundColor:[UIColor clearColor]];
    [stars setImage:[UIImage imageNamed:@"bg-stars"]];
    [stars setTag:AD_STARS];
    [cell.contentView addSubview:stars];
    
    UIImageView *hasPromotion = [[UIImageView alloc] init];
    [hasPromotion setBackgroundColor:[UIColor clearColor]];
    [hasPromotion setImage:[UIImage imageNamed:@"has-promotion"]];
    [hasPromotion setTag:AD_HAS_PROMOTION_VIEW];
    [cell.contentView addSubview:hasPromotion];
    
    
    UIImageView *iconPhone = [[UIImageView alloc] init];
    [iconPhone setImage:[UIImage imageNamed:@"icon-phone"]];
    [iconPhone setTag:AD_PHONEICON_VIEW];
    [cell.contentView addSubview:iconPhone];
    
    UILabel *adPhone = [[UILabel alloc] init];
    [adPhone setNumberOfLines:1];
    [adPhone setBackgroundColor:[UIColor clearColor]];
    [adPhone setTag:AD_PHONE_VIEW];
    [adPhone setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-Hv" size:11.f]];
    [adPhone setTextColor:textColor];
    [cell.contentView addSubview:adPhone];
    
    UIView *grayLine = [[UIView alloc] init];
    [grayLine setBackgroundColor:Rgb2UIColor(229,229,229)];
    [grayLine setTag:AD_GREY_LINE];
    [cell.contentView addSubview:grayLine];
    
    
    UILabel *adAddress = [[UILabel alloc] init];
    [adAddress setNumberOfLines:1];
    [adAddress setBackgroundColor:[UIColor clearColor]];
    [adAddress setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-LtIt" size:9.f]];
    [adAddress setTag:AD_ADRESS_VIEW];
    [adAddress setTextColor:textColor];
    [cell.contentView addSubview:adAddress];
    
    UILabel *adDistance = [[UILabel alloc] init];
    [adDistance setNumberOfLines:1];
    [adDistance setBackgroundColor:[UIColor clearColor]];
    [adDistance setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-BdIt" size:9.f]];
    [adDistance setTag:AD_DISTANCE_VIEW];
    [adDistance setTextColor:textColor];
    [cell.contentView addSubview:adDistance];
  }
  
  NSDictionary *advertise = [[[Neighborhood instance] neighborhoodAdvertises] objectAtIndex:self.advertiseIndex];
  
  UICollectionViewCell *advertiseCell;
  if ([self isPayedAdvertise]){
    advertiseCell = [self payedAdvertiseCell:cell withAd:advertise];
  }
  else{
    advertiseCell = [self freeAdvertiseCell:cell withAd:advertise];
  }

  if ([[advertise objectForKey:@"isopened"] boolValue]){
    [(UIImageView *)[advertiseCell.contentView viewWithTag:AD_OPEN_VIEW] setHidden:NO];
  }
  
  return advertiseCell;
}

- (BOOL)isPayedAdvertise{
  NSDictionary *advertise = [[[Neighborhood instance] neighborhoodAdvertises] objectAtIndex:self.advertiseIndex];
  
  return [[advertise objectForKey:@"payed"] boolValue];
}

- (UICollectionViewCell *)payedAdvertiseCell:(UICollectionViewCell *)cell withAd:(NSDictionary *)advertise{
  CGFloat marginText = 5;
  CGFloat lastTop = 5;
  CGFloat marginBottom = 5;
  
  UIImageView *adImage = (UIImageView *)[cell.contentView viewWithTag:AD_IMAGE_VIEW];
  NSString *adImgeURL = [self advertiseImageURL:advertise];
  if ([adImgeURL isEqual:[NSNull null]]){
    [adImage setImage:[UIImage imageNamed:@"no-image"]];
  }else{
    [adImage sd_setImageWithURL:[NSURL URLWithString:adImgeURL] placeholderImage:[UIImage imageNamed:@"no-image"]];
  }
  [adImage setFrame:CGRectMake(marginText, lastTop, 89, 86)];
  [adImage setHidden:NO];
  
  
  CGFloat adOpenW = 38;
  
  CGFloat titleX = adImage.frame.size.width + marginText+5;
  [(UILabel *)[cell.contentView viewWithTag:AD_TITLE_VIEW] setText:[advertise objectForKey:@"title"]];
  [(UILabel *)[cell.contentView viewWithTag:AD_TITLE_VIEW] setFrame:CGRectMake(titleX, lastTop, cell.frame.size.width - titleX - adOpenW - 4, 14.5)];
  
  CGFloat adOpenX = titleX + [cell.contentView viewWithTag:AD_TITLE_VIEW].frame.size.width - 5;
  [(UILabel *)[cell.contentView viewWithTag:AD_OPEN_VIEW] setFrame:CGRectMake(adOpenX, lastTop, adOpenW, 14.5)];
  
  lastTop += [cell.contentView viewWithTag:AD_OPEN_VIEW].frame.size.height;
  
  [(UILabel *)[cell.contentView viewWithTag:AD_DETAIL_VIEW] setText:[advertise objectForKey:@"details"]];
  [(UILabel *)[cell.contentView viewWithTag:AD_DETAIL_VIEW] setFrame:CGRectMake(titleX, lastTop, cell.frame.size.width - titleX, 15)];
  [(UILabel *)[cell.contentView viewWithTag:AD_DETAIL_VIEW] setHidden:NO];
  lastTop += [cell.contentView viewWithTag:AD_DETAIL_VIEW].frame.size.height+marginBottom;
  
  UIImageView *starsView = (UIImageView *)[cell.contentView viewWithTag:AD_STARS];
  [starsView setHidden:NO];
  [starsView setFrame:CGRectMake(titleX, lastTop, 107, 16)];
  
  CGFloat promotionX = starsView.frame.origin.x + starsView.frame.size.width + marginText;
  [(UIImageView *)[cell.contentView viewWithTag:AD_HAS_PROMOTION_VIEW] setFrame:CGRectMake(promotionX, lastTop, 70, 16)];
  NSArray *promotions = [advertise objectForKey:@"promotions"];
  if ([promotions count] == 0){
    [(UIImageView *)[cell.contentView viewWithTag:AD_HAS_PROMOTION_VIEW] setHidden:YES];
  }
  
  lastTop += [cell.contentView viewWithTag:AD_HAS_PROMOTION_VIEW].frame.size.height+marginBottom;
  
  
  [(UILabel *)[cell.contentView viewWithTag:AD_PHONEICON_VIEW] setFrame:CGRectMake(titleX, lastTop, 16.5, 17)];
  
  CGFloat phoneX = [cell.contentView viewWithTag:AD_PHONEICON_VIEW].frame.size.width + [cell.contentView viewWithTag:AD_PHONEICON_VIEW].frame.origin.x;
  [(UILabel *)[cell.contentView viewWithTag:AD_PHONE_VIEW] setText:[[advertise objectForKey:@"store"] objectForKey:@"phone_summary"]];
  [(UILabel *)[cell.contentView viewWithTag:AD_PHONE_VIEW] setFrame:CGRectMake(phoneX, lastTop+3, cell.frame.size.width - phoneX, 15)];
  lastTop += [cell.contentView viewWithTag:AD_PHONE_VIEW].frame.size.height+marginBottom;
  
  
  [(UILabel *)[cell.contentView viewWithTag:AD_GREY_LINE] setFrame:CGRectMake(titleX, lastTop-1, cell.frame.size.width - titleX, 1)];
  [(UILabel *)[cell.contentView viewWithTag:AD_GREY_LINE] setHidden:NO];
  lastTop += [cell.contentView viewWithTag:AD_GREY_LINE].frame.size.height;
  
  [(UILabel *)[cell.contentView viewWithTag:AD_ADRESS_VIEW] setText:[[advertise objectForKey:@"store"] objectForKey:@"address"]];
  [(UILabel *)[cell.contentView viewWithTag:AD_ADRESS_VIEW] setFrame:CGRectMake(titleX, lastTop, 156, 15)];
  
  [(UILabel *)[cell.contentView viewWithTag:AD_DISTANCE_VIEW] setText:[[advertise objectForKey:@"store"] objectForKey:@"distance"]];
  [(UILabel *)[cell.contentView viewWithTag:AD_DISTANCE_VIEW] setFrame:CGRectMake(adOpenX+5, lastTop, 35, 15)];
  
  
  //Fill starsImageView background
  for (UIView *v in [starsView subviews]) {
    [v removeFromSuperview];
  }
  CGFloat rating = [[advertise objectForKey:@"rating"] floatValue];
  NSArray *ratingCp = nil;
  if (rating > 0){
    ratingCp = [[advertise objectForKey:@"rating"] componentsSeparatedByString:@"."];
  }
  
  CGFloat starsWidth = 10;
  CGRect lastStarFrame;
  CGFloat starX = 0;
  BOOL shouldAddHalf = NO;
  for (CGFloat x = 1; x <= 5; x++) {
    if (x == 1){
      starX = 3;
    }
    UIImageView *star = [[UIImageView alloc] initWithFrame:CGRectMake(starX, 3,starsWidth, 9.5)];
    [starsView addSubview:star];
    lastStarFrame = star.frame;
    
    if (ratingCp){
      if (x < [[ratingCp objectAtIndex:0] floatValue]){
        [star setImage:[UIImage imageNamed:@"icon-star-full"]];
      }
      if (x > [[ratingCp objectAtIndex:0] floatValue]){
        [star setImage:[UIImage imageNamed:@"icon-star-empty"]];
      }
      if (shouldAddHalf){
        [star setImage:[UIImage imageNamed:@"icon-star-half"]];
        shouldAddHalf = NO;
      }
      if (x == [[ratingCp objectAtIndex:0] floatValue]){
        [star setImage:[UIImage imageNamed:@"icon-star-full"]];
        
        if ([[ratingCp objectAtIndex:1] floatValue] > 0){
          shouldAddHalf = YES;
        }
      }
      
    }
    else{
      [star setImage:[UIImage imageNamed:@"icon-star-empty"]];
    }
    starX = SumFrameXWidth(star.frame);
  }
  
  NSInteger totalRate = [[advertise objectForKey:@"rate_count"] integerValue];
  NSString *rateText = @"avaliação";
  if (totalRate > 0){
    rateText = @"avaliações";
  }
  UILabel *rateCount = [[UILabel alloc] initWithFrame:CGRectMake(SumFrameXWidth(lastStarFrame)+5, lastStarFrame.origin.y - 3, 100, starsView.frame.size.height - 2)];
  [rateCount setText:[NSString stringWithFormat:@"%ld %@", (long)totalRate, rateText]];
  [rateCount setTextColor:Rgb2UIColor(114, 114, 114)];
  [rateCount setFont:[UIFont fontWithName:@"HelveticaNeue" size:8.f]];
  [starsView addSubview:rateCount];
  
  
  return cell;
}

- (UICollectionViewCell *)freeAdvertiseCell:(UICollectionViewCell *)cell withAd:(NSDictionary *)advertise{
  CGFloat marginText = 5;
  CGFloat lastTop = 5;
  
  CGFloat adOpenW = 38;
  
  CGFloat titleX = marginText;
  [(UILabel *)[cell.contentView viewWithTag:AD_TITLE_VIEW] setText:[advertise objectForKey:@"title"]];
  [(UILabel *)[cell.contentView viewWithTag:AD_TITLE_VIEW] setFrame:CGRectMake(titleX, lastTop, cell.frame.size.width - titleX - adOpenW - 4, 14.5)];
  
  CGFloat adOpenX = titleX + [cell.contentView viewWithTag:AD_TITLE_VIEW].frame.size.width - 5;
  [(UILabel *)[cell.contentView viewWithTag:AD_OPEN_VIEW] setFrame:CGRectMake(adOpenX, lastTop, adOpenW, 14.5)];
  
  lastTop += [cell.contentView viewWithTag:AD_TITLE_VIEW].frame.size.height+8;
  
  [(UILabel *)[cell.contentView viewWithTag:AD_DISTANCE_VIEW] setText:[[advertise objectForKey:@"store"] objectForKey:@"distance"]];
  [(UILabel *)[cell.contentView viewWithTag:AD_DISTANCE_VIEW] setFrame:CGRectMake(titleX, lastTop, 35, 15)];
  
  CGFloat addressX = SumFrameXWidth([cell.contentView viewWithTag:AD_DISTANCE_VIEW].frame);
  [(UILabel *)[cell.contentView viewWithTag:AD_ADRESS_VIEW] setText:[[advertise objectForKey:@"store"] objectForKey:@"address"]];
  [(UILabel *)[cell.contentView viewWithTag:AD_ADRESS_VIEW] setFrame:CGRectMake(addressX, lastTop, 156, 15)];
  
  CGFloat phoneIconX = SumFrameXWidth([cell.contentView viewWithTag:AD_ADRESS_VIEW].frame);;
  [(UILabel *)[cell.contentView viewWithTag:AD_PHONEICON_VIEW] setFrame:CGRectMake(phoneIconX, lastTop-3, 16.5, 17)];
  
  CGFloat phoneX = SumFrameXWidth([cell.contentView viewWithTag:AD_PHONEICON_VIEW].frame);;
  [(UILabel *)[cell.contentView viewWithTag:AD_PHONE_VIEW] setText:[[advertise objectForKey:@"store"] objectForKey:@"phone_summary"]];
  [(UILabel *)[cell.contentView viewWithTag:AD_PHONE_VIEW] setFrame:CGRectMake(phoneX, lastTop, cell.frame.size.width - phoneX, 15)];
  
  
  [(UILabel *)[cell.contentView viewWithTag:AD_GREY_LINE] setHidden:YES];
  [(UILabel *)[cell.contentView viewWithTag:AD_DETAIL_VIEW] setHidden:YES];
  [(UIImageView *)[cell.contentView viewWithTag:AD_STARS] setHidden:YES];
  [(UIImageView *)[cell.contentView viewWithTag:AD_HAS_PROMOTION_VIEW] setHidden:YES];
  [(UIImageView *)[cell.contentView viewWithTag:AD_IMAGE_VIEW] setHidden:YES];
  
  return cell;
}

- (NSString *)advertiseImageURL:(NSDictionary *)advertise{
  NSString *highlightImgeURL = [[advertise objectForKey:@"highlight_image"] objectForKey:@"url"];
  if ([highlightImgeURL isEqual:[NSNull null]]) {
    NSString *defaultImgeURL = [[advertise objectForKey:@"image"] objectForKey:@"url"];
    return defaultImgeURL;
  }
  return highlightImgeURL;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
  
  DetailViewController *detailVC = [DetailViewController new];
  [detailVC setAdvertiseIndex:self.advertiseIndex];
  [[(UIViewController *)[[self nextResponder] nextResponder] navigationController] pushViewController:detailVC animated:YES];
}

@end
