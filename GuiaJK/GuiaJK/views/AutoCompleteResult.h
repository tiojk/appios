//
//  AutoCompleteResult.h
//  GuiaJK
//
//  Created by pierreabreup on 6/5/16.
//  Copyright © 2016 Hand Mob. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AutoCompleteResult : UIView <UITableViewDataSource,UITableViewDelegate>
  @property (strong, nonatomic) UITableView *result;
@end
