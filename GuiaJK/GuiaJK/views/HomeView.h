//
//  HomeView.h
//  GuiaJK
//
//  Created by pierreabreup on 7/19/15.
//  Copyright (c) 2015 Hand Mob. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KASlideShow.h"

#define CELL_ICON 299

@interface HomeView : UIView <UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate, UIScrollViewDelegate, KASlideShowDelegate>
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) NSMutableIndexSet *expandedSections;
@property (strong,nonatomic) NSArray *headerColors;
@property (strong, nonatomic) KASlideShow *slideshow;


- (void)reloadElements;
- (void)showSearchResultByLocation;
@end
