//
//  PickerNeighborhoodView.h
//  GuiaJK
//
//  Created by pierreabreup on 6/26/16.
//  Copyright © 2016 Hand Mob. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickerNeighborhoodView : UIView<UISearchBarDelegate,UICollectionViewDelegate,UICollectionViewDataSource>

- (void)addNeighborsForSearchByName:(NSString *)name;
- (void)addDefaultSelectedNeighborhoods;

@end
