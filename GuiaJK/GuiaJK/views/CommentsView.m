//
//  CommentsView.m
//  GuiaJK
//
//  Created by pierreabreup on 6/19/16.
//  Copyright © 2016 Hand Mob. All rights reserved.
//

#import "CommentsView.h"
#import "Constants.h"

@implementation CommentsView

@synthesize comments;
+ (CGFloat) cellHeight{
  return 112.5;
}

- (void)drawRect:(CGRect)rect {
  
  
  
  self.collection = [[UICollectionView alloc] initWithFrame:CGRectMake(0,0,self.frame.size.width,self.frame.size.height)
                                                 collectionViewLayout:[[UICollectionViewFlowLayout alloc] init]];
  
  [self.collection setBackgroundColor:[UIColor clearColor]];
  [self.collection setShowsVerticalScrollIndicator:NO];
  [self.collection setShowsHorizontalScrollIndicator:NO];
  [self.collection setUserInteractionEnabled:YES];
  [self.collection setDataSource:self];
  [self.collection setDelegate:self];
  [self.collection registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"commentCollectionCell"];
  
  [self addSubview:self.collection];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
  return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  return [self.comments count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
  return CGSizeMake(collectionView.frame.size.width, [CommentsView cellHeight]);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"commentCollectionCell" forIndexPath:indexPath];
  
  if (![cell.contentView viewWithTag:COMMENT_USER_NAME]){
    [cell.contentView setBackgroundColor:[UIColor whiteColor]];
     
    CGFloat dateWidth = 70;
    UILabel *user = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, collectionView.frame.size.width - dateWidth, 20)];
    [user setBackgroundColor:[UIColor clearColor]];
    [user setTag:COMMENT_USER_NAME];
    [user setNumberOfLines:1];
    [user setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-Md" size:15.f]];
    [user setTextColor:Rgb2UIColor(81, 79, 19)];
    [cell.contentView addSubview:user];
    
    UILabel *date = [[UILabel alloc] initWithFrame:CGRectMake(user.frame.size.width, 5, dateWidth, user.frame.size.height)];
    [date setBackgroundColor:[UIColor clearColor]];
    [date setTag:COMMENT_DATE];
    [date setNumberOfLines:1];
    [date setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.f]];
    [date setTextColor:Rgb2UIColor(163, 163, 160)];
    [cell.contentView addSubview:date];
    
    UIView *stars = [[UIView alloc] initWithFrame:CGRectMake(0, SumFrameYHeight(user.frame)+5, user.frame.size.width, 16)];
    [stars setBackgroundColor:[UIColor clearColor]];
    [stars setTag:COMMENT_STARS];
    [cell.contentView addSubview:stars];
    
    UITextView *text = [[UITextView alloc] initWithFrame:CGRectMake(5, SumFrameYHeight(stars.frame)+5, cell.frame.size.width, [CommentsView cellHeight] - SumFrameYHeight(stars.frame) - 40)];
    [text setBackgroundColor:[UIColor clearColor]];
    [text setTag:COMMENT_TEXT];
    [text setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.f]];
    [text setTextColor:Rgb2UIColor(81, 79, 19)];
    [text setEditable:NO];
    text.textContainerInset = UIEdgeInsetsZero;
    text.textContainer.lineFragmentPadding = 0;
    [cell.contentView addSubview:text];
  }
  
  NSDictionary *comment = [self.comments objectAtIndex:indexPath.row];
  [(UILabel *)[cell.contentView viewWithTag:COMMENT_USER_NAME] setText:[comment objectForKey:@"user"]];
  [(UILabel *)[cell.contentView viewWithTag:COMMENT_DATE] setText:[comment objectForKey:@"created"]];
  [(UITextView *)[cell.contentView viewWithTag:COMMENT_TEXT] setText:[comment objectForKey:@"text"]];
  
  UIView *starsView = (UIView *)[cell.contentView viewWithTag:COMMENT_STARS];
  
  //Fill starsImageView background
  for (UIView *v in [starsView subviews]) {
    [v removeFromSuperview];
  }
  
  NSInteger rating = [[comment objectForKey:@"rating"] integerValue];
  CGFloat starWidth = 14.5;
  CGRect lastStarFrame;
  CGFloat starX = 0;
  for (CGFloat x = 1; x <= 5; x++) {
    if (x == 1){
      starX = 5;
    }
    UIImageView *star = [[UIImageView alloc] initWithFrame:CGRectMake(starX, 0, starWidth, 16)];
    [starsView addSubview:star];
    
    lastStarFrame = star.frame;
    
    if (x <= rating){
      [star setImage:[UIImage imageNamed:@"icon-star-full"]];
    }
    else{
      [star setImage:[UIImage imageNamed:@"icon-star-empty"]];
    }
    
    starX = SumFrameXWidth(star.frame);
  }
  
  return cell;
}


- (void) appendComment:(NSDictionary *)comment{
  
}
@end
