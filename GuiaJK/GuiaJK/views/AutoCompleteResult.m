//
//  AutoCompleteResult.m
//  GuiaJK
//
//  Created by pierreabreup on 6/5/16.
//  Copyright © 2016 Hand Mob. All rights reserved.
//

#import "AutoCompleteResult.h"
#import "Neighborhood.h"
#import "Constants.h"
#import "DetailViewController.h"
#import "SearchResultViewController.h"

@interface AutoCompleteResult ()
@property NSMutableArray *resultData;
@end

@implementation AutoCompleteResult
@synthesize result;

- (void)drawRect:(CGRect)rect {
  self.resultData = [NSMutableArray array];
  NSArray *advertises = [[Neighborhood instance] neighborhoodAdvertisesAutoComplete];
  NSArray *subcategories = [[Neighborhood instance] neighborhoodAdvertisesCategoriesAutoComplete];
  for (int x=0; x < [subcategories count]; x++) {
    [self.resultData addObject:[subcategories objectAtIndex:x]];
  }
  for (int i=0; i < [advertises count]; i++) {
    [self.resultData addObject:[advertises objectAtIndex:i]];
  }
  
  self.result = [[UITableView alloc] initWithFrame:CGRectMake(0,0,
                                                                    self.frame.size.width,
                                                                    self.frame.size.height) style:UITableViewStyleGrouped];
  self.result.delegate = self;
  self.result.dataSource = self;
  self.result.sectionHeaderHeight = 0.0;
  self.result.sectionFooterHeight = 0.0;
  [self.result setBackgroundColor:[UIColor whiteColor]];
  [self addSubview:result];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  return [self.resultData count];
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
  if (section == 0)
    return CGFLOAT_MIN;
  
  return tableView.sectionHeaderHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *CellIdentifier = @"autoCompleteCell";
  
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
  }

  NSDictionary *data = [self.resultData objectAtIndex:indexPath.row];
  cell.textLabel.text = [data objectForKey:@"title"];
  [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:10.f]];
  [cell.textLabel setTextColor:Rgb2UIColor(114, 114, 114)];
  
  return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  NSDictionary *data = [self.resultData objectAtIndex:indexPath.row];
  
  if ([[data objectForKey:@"type"] isEqualToString:@"advertise"]){
    DetailViewController *detailVC = [DetailViewController new];
    [detailVC setAdvertiseID:[[data objectForKey:@"id"] integerValue]];
    NSString *class_name = NSStringFromClass([[[self nextResponder] nextResponder] class]);
    if ([class_name containsString:@"Controller"]){
      [[(UIViewController *)[[self nextResponder] nextResponder] navigationController] pushViewController:detailVC animated:YES];
    }
    else{
      [[(UIViewController *)[[[self nextResponder] nextResponder] nextResponder] navigationController] pushViewController:detailVC animated:YES];
    }
  }
  else{
    
    NSString *class_name = NSStringFromClass([[[self nextResponder] nextResponder] class]);
    if ([class_name containsString:@"Controller"]){
      SearchResultViewController *searchVC = (SearchResultViewController *)[[self nextResponder] nextResponder];
      [searchVC setByCategory:[data objectForKey:@"title"]];
      [searchVC setShouldResetFilters:YES];
      [Constants removeAutocompleteResult];
      [searchVC.contentView.searchBarView resignFirstResponder];
      [searchVC searchAdvertises];
      
    }
    else{
      SearchResultViewController *searchVC = [SearchResultViewController new];
      [searchVC setEnableShowMap:NO];
      [searchVC setByText:@""];
      [searchVC setByHasPromotion:NO];
      [searchVC setShouldResetFilters:YES];
      [searchVC setByCategory:[data objectForKey:@"title"]];
      [[(UIViewController *)[[[self nextResponder] nextResponder] nextResponder] navigationController] pushViewController:searchVC animated:YES];
    }
  }
  
  
  
}
@end
