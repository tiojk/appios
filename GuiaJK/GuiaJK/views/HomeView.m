//
//  HomeView.m
//  GuiaJK
//
//  Created by pierreabreup on 7/19/15.
//  Copyright (c) 2015 Hand Mob. All rights reserved.
//

#import "HomeView.h"
#import "Constants.h"
#import "Neighborhood.h"
#import "SearchResultViewController.h"
#import "HomeViewController.h"
#import <SDWebImage/SDWebImageManager.h>
#import "DetailViewController.h"

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

@interface HomeView ()
  @property UITableView *categories;
  @property  CGFloat lastScrollViewTopElement;
  @property  CGFloat marginLeft;
  @property  CGFloat marginTop;
  @property  NSInteger bgHeaderColorIndex;
  @property  UISearchBar *boxSearch;
  @property  UILabel *noCovers;
  @property UIView *slidePagination;
  @property NSInteger addedImagesToSlide;
  @property NSArray *paginationIconX;
  @property UIImageView *iconSearch;
  @property CGFloat originalCategoriesHeight;
  @property CGFloat scrollViewBaseContentHeight;
@end

@implementation HomeView

- (void)reloadElements{
  if (self.categories){
    [Constants showIndicatorAtView:self];
    [self.slideshow stop];
    
    [self.expandedSections removeAllIndexes];
    
    CGRect categoriesFrame = self.categories.frame;
    categoriesFrame.size.height = ([[[Neighborhood instance] neighborhoodCategories] count] * 46);
    [self.categories setFrame:categoriesFrame];
    [self.categories reloadData];
    
    self.scrollViewBaseContentHeight += categoriesFrame.size.height;
    
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width,self.scrollViewBaseContentHeight)];
    [self.scrollView setUserInteractionEnabled:YES];
    [self setUserInteractionEnabled:YES];
    [self reloadSlideShow];
    [Constants hideIndicator];
  }else{
    [self addElementsToScrollView];
  }
  
  [Constants hideIndicator];

}

- (void)drawRect:(CGRect)rect {
  if (self.scrollView){
    return;
  }
  [Constants showIndicatorAtView:self.slideshow];
  
  if (!self.paginationIconX){
    self.paginationIconX = [NSArray arrayWithObjects:
                       [NSNumber numberWithFloat:135],
                       [NSNumber numberWithFloat:150],
                       [NSNumber numberWithFloat:166],nil];
  }
  self.marginLeft = 12;
  self.marginTop  = 10;
  self.bgHeaderColorIndex = 0;
  self.lastScrollViewTopElement = 0;
  
  self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,
                                                                   0,
                                                                   self.frame.size.width,
                                                                   self.frame.size.height)];
  [self.scrollView setShowsVerticalScrollIndicator:YES];
  [self.scrollView setShowsHorizontalScrollIndicator:NO];
  [self.scrollView setBackgroundColor:[UIColor clearColor]];
  [self.scrollView setUserInteractionEnabled:YES];
  [self.scrollView setDelegate:self];
  
  
  [self addSubview:self.scrollView];
}


- (void)addElementsToScrollView{
  [self showAdvertisesCover];
  [self showBoxSearch];
  
  UIButton* nearby = [UIButton buttonWithType:UIButtonTypeCustom];
  [nearby setBackgroundImage:[UIImage imageNamed:@"home-nearby"] forState:UIControlStateNormal];
  [nearby setFrame:CGRectMake(self.marginLeft, self.lastScrollViewTopElement, 285, 40)];
  [nearby addTarget:self action:@selector(searchAdvertisesByCurrentLocation) forControlEvents:UIControlEventTouchUpInside];
  [self.scrollView addSubview:nearby];
  
  self.lastScrollViewTopElement += nearby.frame.size.height + self.marginTop;
  
  UIButton* promotion = [UIButton buttonWithType:UIButtonTypeCustom];
  [promotion setBackgroundImage:[UIImage imageNamed:@"home-promotion"] forState:UIControlStateNormal];
  [promotion setFrame:CGRectMake(self.marginLeft, self.lastScrollViewTopElement, 285, 40)];
  [promotion addTarget:self action:@selector(searchAdvertisesByPromotion) forControlEvents:UIControlEventTouchUpInside];
  [self.scrollView addSubview:promotion];
  
  self.lastScrollViewTopElement += promotion.frame.size.height + self.marginTop;
  
  UIImageView *categoriesTitle = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title-categories"]];
  [categoriesTitle setFrame:CGRectMake(self.marginLeft, self.lastScrollViewTopElement, self.boxSearch.frame.size.width - 15, 21)];
  [self.scrollView addSubview:categoriesTitle];
  
  self.lastScrollViewTopElement += categoriesTitle.frame.size.height + self.marginTop;
  self.scrollViewBaseContentHeight = self.lastScrollViewTopElement;
  
  if (!self.expandedSections){
    self.expandedSections = [[NSMutableIndexSet alloc] init];
  }
  
  self.originalCategoriesHeight = ([[[Neighborhood instance] neighborhoodCategories] count] * 46);
  self.categories = [[UITableView alloc] initWithFrame:CGRectMake(
                                                                          self.marginLeft,
                                                                          self.lastScrollViewTopElement,
                                                                          categoriesTitle.frame.size.width,
                                                                          self.originalCategoriesHeight) style:UITableViewStyleGrouped];
  self.categories.delegate = self;
  [self.categories setScrollEnabled:NO];
  self.categories.dataSource = self;
  self.categories.sectionHeaderHeight = 0.0;
  self.categories.sectionFooterHeight = 0.0;
  [self.scrollView addSubview:self.categories];
  
  self.lastScrollViewTopElement += self.categories.frame.size.height + self.marginTop;
  
  [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width,self.lastScrollViewTopElement)];

}

- (void)showAdvertisesCover{
  CGRect coverFrame = CGRectMake(0, 0, self.frame.size.width, 215.0);
  
  self.slideshow = [[KASlideShow alloc] initWithFrame:coverFrame];
  [self.slideshow setDelay:3]; // Delay between transitions
  [self.slideshow setTransitionDuration:1]; // Transition duration
  [self.slideshow setTransitionType:KASlideShowTransitionSlide]; // Choose a transition type (fade or slide)
  [self.slideshow setImagesContentMode:UIViewContentModeScaleToFill]; // Choose a content mode for images to display
  [self.slideshow addGesture:KASlideShowGestureSwipe];
  self.slideshow.delegate = self;
  
  self.slidePagination = [[UIView alloc] initWithFrame:CGRectMake(0, coverFrame.size.height + self.marginTop, coverFrame.size.width, 11.5)];
  [self.slidePagination setBackgroundColor:[UIColor clearColor]];
  [self.scrollView addSubview:self.slidePagination];
  
  self.lastScrollViewTopElement = SumFrameYHeight(coverFrame) + self.slidePagination.frame.size.height + (self.marginTop * 2);

  [self reloadSlideShow];
  
  [self.scrollView addSubview:self.slideshow];
  
  UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSlideSingleTap:)];
  singleTapGestureRecognizer.numberOfTapsRequired = 1;
  [self.slideshow addGestureRecognizer:singleTapGestureRecognizer];
}

- (void)reloadSlideShow{
  [self.slideshow emptyAndAddImagesFromResources:[NSArray array]];
  [self removeAllSlidePaginationIcons];
  
  NSArray *covers = [[Neighborhood instance] neighborhoodCovers];
  
  if ([covers count] == 0){
    [self.slideshow setHidden:YES];
    [self showAdvertiseCoverBlankText:self.slideshow.frame];
    return;
  }
  [self.noCovers setHidden:YES];
  [self.slideshow setHidden:NO];
  

  self.addedImagesToSlide = 0;
  SDWebImageManager *manager = [SDWebImageManager sharedManager];
  for (NSDictionary *cover in covers) {
    NSString *imageURLString = [[cover objectForKey:@"neighborhood_image"] objectForKey:@"url"];
    [manager downloadImageWithURL:[NSURL URLWithString:imageURLString] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
      if (image)
      {
        [self.slideshow addImage:image];
        self.addedImagesToSlide++;
        
        if (self.addedImagesToSlide > 1){
          if (self.addedImagesToSlide == 2){
            [self addSlidePaginationIcon:[UIImage imageNamed:@"icon-slide-selected"]
                               atOriginX:[[self.paginationIconX objectAtIndex:0] floatValue]];
            [self performSelector:@selector(startAnimatedSlide) withObject:self afterDelay:1];
          }
          
          [self addSlidePaginationIcon:[UIImage imageNamed:@"icon-slide-unselected"]
                             atOriginX:[[self.paginationIconX objectAtIndex:(self.addedImagesToSlide - 1)] floatValue]];
        }
        
        
      }
    }];
  }
}

- (void)startAnimatedSlide{
  [self.slideshow start];
}

- (void)removeAllSlidePaginationIcons{
  NSArray *viewsToRemove = [self.slidePagination subviews];
  for (UIView *v in viewsToRemove) {
    [v removeFromSuperview];
  }
}

- (void)addSlidePaginationIcon:(UIImage *)icon atOriginX:(CGFloat)x{
  UIImageView *paginationIcon = [[UIImageView alloc] initWithFrame:CGRectMake(x, 0, 11, 11.5)];
  [paginationIcon setImage:icon];
  [self.slidePagination addSubview:paginationIcon];
}

- (void) kaSlideShowDidShowNext:(KASlideShow *) slideShow{
  [self setSlidePaginationIconSelected:slideShow];
}

- (void) kaSlideShowDidShowPrevious:(KASlideShow *) slideShow{
  [self setSlidePaginationIconSelected:slideShow];
}

- (void)setSlidePaginationIconSelected:(KASlideShow *) slideShow{
  [self removeAllSlidePaginationIcons];
  NSString *iconName;
  
  for (int i = 0; i < self.addedImagesToSlide; i++) {
    iconName = @"icon-slide-unselected";
    
    if (slideShow.currentIndex == i){
      iconName = @"icon-slide-selected";
    }
    
    [self addSlidePaginationIcon:[UIImage imageNamed:iconName] atOriginX:[[self.paginationIconX objectAtIndex:i] floatValue]];
  }
}

- (void)showAdvertiseCoverBlankText:(CGRect)coverFrame{
  if (!self.noCovers){
    self.noCovers = [[UILabel alloc]initWithFrame:coverFrame];
    [self.noCovers setText:@"Não há destaques para este bairro"];
    [self.noCovers setNumberOfLines:1];
    [self.noCovers setTextAlignment:NSTextAlignmentCenter];
    [self.noCovers setBackgroundColor:[UIColor whiteColor]];
    [self.noCovers setTextColor:Rgb2UIColor(27, 116, 183)];
    
    [self.scrollView addSubview:self.noCovers];
  }
  [self.noCovers setHidden:NO];

}

#pragma mark - Gesture Recognizers handling
- (void)handleSlideSingleTap:(id)sender{
  if ([[[Neighborhood instance] neighborhoodCovers] count] > 0) {
    [Neighborhood instance].neighborhoodAdvertises = [[Neighborhood instance] neighborhoodCovers];
    DetailViewController *detailVC = [DetailViewController new];
    [detailVC setAdvertiseIndex:self.slideshow.currentIndex];
    [[(UIViewController *)[[self nextResponder] nextResponder] navigationController] pushViewController:detailVC animated:YES];
  }
  

}

- (void)showBoxSearch{
  self.boxSearch = [[UISearchBar alloc] initWithFrame:CGRectMake((self.marginLeft/2 - 1), self.lastScrollViewTopElement, 295, 50.5)];
  [self.boxSearch setSearchFieldBackgroundImage:[UIImage imageNamed:@"box-search"] forState:UIControlStateNormal];
  [self.boxSearch setBackgroundColor:[UIColor clearColor]];
  [self.boxSearch setDelegate:self];
  [self.boxSearch setImage:[UIImage imageNamed:@"blank"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
  self.boxSearch.searchBarStyle = UISearchBarStyleMinimal;
  self.boxSearch.placeholder = @"    Pizza, sushi, carro, cinema...";
  
  self.iconSearch = [[UIImageView alloc] initWithFrame:CGRectMake(30, 10, 32, 32)];
  [self.iconSearch setImage:[UIImage imageNamed:@"icon-search"]];
   [self.boxSearch addSubview:self.iconSearch];

  [self.scrollView addSubview:self.boxSearch];
  
  self.lastScrollViewTopElement += self.boxSearch.frame.size.height + self.marginTop;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
  [self.boxSearch resignFirstResponder];
  [Constants removeAutocompleteResult];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
  [self.scrollView setContentOffset:CGPointMake(0, 120) animated:YES];
  self.boxSearch.placeholder = @"        Pizza, sushi, carro, cinema...";
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
  if([searchText length] == 0){
    [self.iconSearch setHidden:NO];
    [Constants removeAutocompleteResult];
  }
  else{
    [self.iconSearch setHidden:YES];
    if ([searchText length] >= 4){
      [Constants autocompleteResultFor:searchBar showAbove:YES];
    }
    else{
      [Constants removeAutocompleteResult];
    }
  }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
 [Constants removeAutocompleteResult];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
  if ([[searchBar text] length] < 5){
    [Constants alertWithMessage:@"Digite três ou mais letras para realizar a busca"];
    return;
  }
  
  [Constants removeAutocompleteResult];
  [searchBar resignFirstResponder];
  
  SearchResultViewController *searchVC = [SearchResultViewController new];
  [searchVC setByText:[searchBar text]];
  [searchVC setEnableShowMap:NO];
  [searchVC setShouldResetFilters:YES];
  [[(UIViewController *)[[self nextResponder] nextResponder] navigationController] pushViewController:searchVC animated:YES];
  
}

- (void)showSearchResultByLocation{
  SearchResultViewController *searchVC = [SearchResultViewController new];
  [searchVC setShouldResetFilters:YES];
  [searchVC setByText:nil];
  [searchVC setEnableSearchLocation:YES];
  [[Neighborhood instance] setAdvertisesByCategory:nil];
  [[Neighborhood instance] setAdvertisesByText:nil];
  [[(UIViewController *)[[self nextResponder] nextResponder] navigationController] pushViewController:searchVC animated:YES];
}

- (void)searchAdvertisesByCurrentLocation{
  //clear cache because users lat long might be changed
  if ([[Neighborhood instance] advertisesCategories] && [[Neighborhood instance] advertisesCategories] > 0){
    [[Neighborhood instance] setAdvertisesCategories:[NSArray array]];
  }
  
  [(HomeViewController *)[[self nextResponder] nextResponder] searchAdvertisesByCurrentLocation];
}

- (void)searchAdvertisesByPromotion{
  SearchResultViewController *searchVC = [SearchResultViewController new];
  [searchVC setEnableShowMap:NO];
  [searchVC setByHasPromotion:YES];
  [searchVC setByCategory:nil];
  [searchVC setShouldResetFilters:YES];
  [searchVC setByText:nil];
  [searchVC setByCurrentLocation:nil];
  [[Neighborhood instance] setAdvertisesByText:nil];
  [[Neighborhood instance] setAdvertisesByCategory:nil];
  [[(UIViewController *)[[self nextResponder] nextResponder] navigationController] pushViewController:searchVC animated:YES];
}


- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
  return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return [[[Neighborhood instance] neighborhoodCategories] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  if ([self tableView:tableView canCollapseSection:section])
  {
    if ([self.expandedSections containsIndex:section])
    {
      NSDictionary *category = (NSDictionary *)[[[Neighborhood instance] neighborhoodCategories] objectAtIndex:section];
      return [(NSArray *)[category objectForKey:@"children"] count] + 1; // return rows when expanded
    }
    
    return 1; // only top row showing
  }
  
  // Return the number of rows in the section.
  return 1;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
  return 30;
}


- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
  if (section == 0)
    return CGFLOAT_MIN;
  
  return tableView.sectionHeaderHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *CellIdentifier = @"Cell";
  
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
  }
  
  if ([cell viewWithTag:CELL_ICON]){
    [[cell viewWithTag:CELL_ICON] removeFromSuperview];
  }
  

  if (!indexPath.row)
  {
    NSDictionary *category = (NSDictionary *)[[[Neighborhood instance] neighborhoodCategories] objectAtIndex:indexPath.section];
    cell.textLabel.text = [NSString stringWithFormat:@"      %@",(NSString *)[category objectForKey:@"name"]];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    NSString *iconURLString = (NSString *)[category objectForKey:@"icon"];
    if (iconURLString && ![iconURLString isEqual:[NSNull null]]){
      [manager downloadImageWithURL:[NSURL URLWithString:iconURLString] options:0 progress:nil completed:^(UIImage *iconImage, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        if (iconImage)
        {
          UIImageView *imv = [[UIImageView alloc]initWithFrame:CGRectMake(5,5, 33.5, 33.5)];
          [imv setTag:CELL_ICON];
          [imv setImage:iconImage];
          [cell addSubview:imv];
        }
      }];
    }
    
    [cell setBackgroundColor:[Constants colorFromHexString:[category objectForKey:@"bgcolor"]]];
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-BlkIt" size:15.f]];
    
    if ([self.expandedSections containsIndex:indexPath.section])
    {
      
      UIImageView *imView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-minus"]];
      cell.accessoryView = imView;
    }
    else
    {
      
      UIImageView *imView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-plus"]];
      cell.accessoryView = imView;
    }
  }
  else
  {
    NSDictionary *category = (NSDictionary *)[[[Neighborhood instance] neighborhoodCategories] objectAtIndex:indexPath.section];
    NSArray *children = (NSArray *)[category objectForKey:@"children"];
    cell.textLabel.text = [children objectAtIndex:(indexPath.row - 1)];
    [cell.textLabel setTextColor:Rgb2UIColor(117, 117, 117)];
    cell.accessoryView = nil;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [cell setBackgroundColor:[UIColor whiteColor]];
    [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-It" size:15.f]];
  }

  return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  if ([self tableView:tableView canCollapseSection:indexPath.section])
  {
    if (!indexPath.row)
    {
      // only first row toggles exapand/collapse
      [tableView deselectRowAtIndexPath:indexPath animated:YES];
      
      NSInteger section = indexPath.section;
      BOOL currentlyExpanded = [self.expandedSections containsIndex:section];
      NSInteger rows;
      
      
      NSMutableArray *tmpArray = [NSMutableArray array];
      CGRect tableViewFrame = tableView.frame;
      CGSize scrollViewContent = self.scrollView.contentSize;
      CGFloat delta = 42;
      if (currentlyExpanded)
      {
        rows = [self tableView:tableView numberOfRowsInSection:section];
        [self.expandedSections removeIndex:section];
        tableViewFrame.size.height = (tableViewFrame.size.height - (rows * delta));
        scrollViewContent.height = (scrollViewContent.height - (rows * delta));
      }
      else
      {
        [self.expandedSections addIndex:section];
        rows = [self tableView:tableView numberOfRowsInSection:section];
        tableViewFrame.size.height = (tableViewFrame.size.height + (rows * delta));
        scrollViewContent.height = (scrollViewContent.height + (rows * delta));
      }
      [self.scrollView setContentSize:scrollViewContent];
      [self.categories setFrame:tableViewFrame];
      
      
      for (int i=1; i<rows; i++)
      {
        NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i
                                                       inSection:section];
        [tmpArray addObject:tmpIndexPath];
      }
      
      UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
      
      if (currentlyExpanded)
      {
        [tableView deleteRowsAtIndexPaths:tmpArray
                         withRowAnimation:UITableViewRowAnimationTop];
        
        UIImageView *imView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-plus"]];
        cell.accessoryView = imView;
      }
      else
      {
        [tableView insertRowsAtIndexPaths:tmpArray
                         withRowAnimation:UITableViewRowAnimationTop];
        
        UIImageView *imView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-minus"]];
        cell.accessoryView = imView;
      }
    }
    else{
      NSDictionary *category = (NSDictionary *)[[[Neighborhood instance] neighborhoodCategories] objectAtIndex:indexPath.section];
      NSArray *children = (NSArray *)[category objectForKey:@"children"];
      
      SearchResultViewController *searchVC = [SearchResultViewController new];
      [searchVC setEnableShowMap:NO];
      [searchVC setByText:@""];
      [searchVC setByHasPromotion:NO];
      [searchVC setShouldResetFilters:YES];
      [searchVC setByCategory:[children objectAtIndex:(indexPath.row - 1)]];
      [[(UIViewController *)[[self nextResponder] nextResponder] navigationController] pushViewController:searchVC animated:YES];
    }
  }
  
}

@end
