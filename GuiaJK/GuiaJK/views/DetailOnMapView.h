//
//  DetailOnMapView.h
//  GuiaJK
//
//  Created by pierreabreup on 6/25/16.
//  Copyright © 2016 Hand Mob. All rights reserved.
//

#import <UIKit/UIKit.h>
#define AD_IMAGE_VIEW 290
#define AD_TITLE_VIEW 291
#define AD_PHONE_VIEW 292
#define AD_ADRESS_VIEW 294
#define AD_STARS 295
#define AD_OPEN_VIEW 296
#define AD_DETAIL_VIEW 297
#define AD_HAS_PROMOTION_VIEW 298
#define AD_GREY_LINE 299
#define AD_DISTANCE_VIEW 300
#define AD_PHONEICON_VIEW 2921
#define PROMOTION_FILTER_BUTTON 401
#define OPENNOW_FILTER_BUTTON 402
#define FARAWAY_FILTER_BUTTON 403
#define MAX_DISTANCE_VIEW 3999


@interface DetailOnMapView : UIView <UICollectionViewDataSource, UICollectionViewDelegate>
  @property (assign,nonatomic) NSInteger advertiseIndex;
@end
