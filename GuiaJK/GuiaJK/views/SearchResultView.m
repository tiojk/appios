//
//  SearchResultView.m
//  GuiaJK
//
//  Created by pierreabreup on 8/2/15.
//  Copyright (c) 2015 Hand Mob. All rights reserved.
//

#import "SearchResultViewController.h"
#import "SearchResultView.h"
#import "Constants.h"
#import "Neighborhood.h"
#import "UIImageView+WebCache.h"
#import "DetailViewController.h"


@interface SearchResultView ()
@property  CGFloat marginLeft;
@property  CGFloat marginTop;
@property  UISearchBar *boxSearch;
@property UIImageView *iconSearch;
@property CGFloat collectionViewTop;
@property UIView *backgroundModal;
@property UIView *contentModalView;
@property UIButton* btDeliveryOn;
@property UIButton* btDeliveryOff;
@property UIButton* btMaxDistanceA;
@property UIButton* btMaxDistanceB;
@property UIButton* btMaxDistanceC;
@property UIButton* btMaxDistanceD;
@property UIImageView *hightlightTitle;
@end

@implementation SearchResultView
@synthesize hideSearchBar;

- (void)reloadElements{
  if (self.collectionView){
    [self.collectionView reloadData];
  }
  else{
    [self createCollectionView];
  }
  
  if ([[[Neighborhood instance] neighborhoodAdvertises] count] == 0){
    if (self.hideSearchBar){
      [Constants alertWithMessage:@"Nenhum anúncio próximo de você."];
    }else{
      [Constants alertWithMessage:@"Nenhum anúncio encontrado."];
    }


  }
}

- (UISearchBar *)searchBarView{
  return self.boxSearch;
}

- (void)drawRect:(CGRect)rect {
  self.marginLeft = 10;
  self.marginTop  = 10;
  
  CGFloat boxSearchHeigh = 33;
  CGFloat boxSearchTop = self.marginTop;
  if (self.hideSearchBar){
    boxSearchHeigh = 0;
    boxSearchTop = 0;
  }
  
  UIButton* filter = [UIButton buttonWithType:UIButtonTypeCustom];
  [filter setBackgroundImage:[UIImage imageNamed:@"bt-filter"] forState:UIControlStateNormal];
  [filter setFrame:CGRectMake(self.marginLeft+5, boxSearchTop, 49, 33.5)];
  [filter addTarget:self action:@selector(showFilterModal) forControlEvents:UIControlEventTouchUpInside];
  [self addSubview:filter];
  
  SearchResultViewController *vc = (SearchResultViewController *)[[self nextResponder] nextResponder];
  
  CGFloat boxX = filter.frame.origin.x + filter.frame.size.width;
  self.boxSearch = [[UISearchBar alloc] initWithFrame:CGRectMake(boxX, boxSearchTop, 194, boxSearchHeigh)];
  [self.boxSearch setSearchFieldBackgroundImage:[UIImage imageNamed:@"box-search-internal"] forState:UIControlStateNormal];
  [self.boxSearch setBackgroundColor:[UIColor clearColor]];
  [self.boxSearch setDelegate:self];
  [self.boxSearch setImage:[UIImage imageNamed:@"blank"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
  self.boxSearch.searchBarStyle = UISearchBarStyleMinimal;
  if (vc.byText && [vc.byText length] > 0){
    self.boxSearch.placeholder = vc.byText;
  }
  else{
    self.boxSearch.placeholder = @"Pizza, sushi, carro...";
  }
  
  
  self.iconSearch = [[UIImageView alloc] initWithFrame:CGRectMake(12, 10, 18, 17)];
  [self.iconSearch setImage:[UIImage imageNamed:@"icon-search"]];
  [self.boxSearch addSubview:self.iconSearch];
  
  [self addSubview:self.boxSearch];
  
  CGFloat mapX = self.boxSearch.frame.origin.x + self.boxSearch.frame.size.width;
  UIButton* map = [UIButton buttonWithType:UIButtonTypeCustom];
  [map setBackgroundImage:[UIImage imageNamed:@"bt-map"] forState:UIControlStateNormal];
  [map setFrame:CGRectMake(mapX, boxSearchTop, 49, 33.5)];
  [map addTarget:self action:@selector(showAdvertisesMap) forControlEvents:UIControlEventTouchUpInside];
  [self addSubview:map];
  
  CGFloat filtersButtonY = SumFrameYHeight(self.boxSearch.frame) + 10;
  UIButton *promotionFilter = [UIButton buttonWithType:UIButtonTypeCustom];
  [promotionFilter setTag:PROMOTION_FILTER_BUTTON];
  [promotionFilter setFrame:CGRectMake(filter.frame.origin.x, filtersButtonY, 94, 21)];
  [promotionFilter setTitle:@"Com Promoção" forState:UIControlStateNormal];
  
  UIColor *promotionTitle = Rgb2UIColor(111, 111, 110);
  UIColor *promotionBg = Rgb2UIColor(245, 245, 245);
  [promotionFilter setTitleColor:promotionTitle forState:UIControlStateNormal];
  [promotionFilter setBackgroundColor:promotionBg];
  [promotionFilter addTarget:self action:@selector(exposedFilterClicked:) forControlEvents:UIControlEventTouchUpInside];
  [promotionFilter.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:11.f]];
 promotionFilter.layer.cornerRadius = 5;
 promotionFilter.layer.borderWidth = 1;
 promotionFilter.layer.borderColor = Rgb2UIColor(201, 201, 201).CGColor;
  [self addSubview:promotionFilter];
  
  NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject: promotionFilter];
  UIButton *openNowFilter = [NSKeyedUnarchiver unarchiveObjectWithData: archivedData];
  [openNowFilter setTag:OPENNOW_FILTER_BUTTON];
  [openNowFilter setTitle:@"Aberto Agora" forState:UIControlStateNormal];
  [openNowFilter addTarget:self action:@selector(exposedFilterClicked:) forControlEvents:UIControlEventTouchUpInside];
  CGRect frame = openNowFilter.frame;
  frame.origin.x = (promotionFilter.frame.origin.x + promotionFilter.frame.size.width + 5);
  openNowFilter.layer.cornerRadius = 5;
  openNowFilter.layer.borderWidth = 1;
  openNowFilter.layer.borderColor = Rgb2UIColor(201, 201, 201).CGColor;
  [openNowFilter setFrame:frame];
  [self addSubview:openNowFilter];
  
  archivedData = [NSKeyedArchiver archivedDataWithRootObject: promotionFilter];
  UIButton *farAwayFilter = [NSKeyedUnarchiver unarchiveObjectWithData: archivedData];
  [farAwayFilter setTag:FARAWAY_FILTER_BUTTON];
  [farAwayFilter setTitle:@"Mais Distante" forState:UIControlStateNormal];
  [farAwayFilter addTarget:self action:@selector(exposedFilterClicked:) forControlEvents:UIControlEventTouchUpInside];
  frame = farAwayFilter.frame;
  frame.origin.x = (openNowFilter.frame.origin.x + openNowFilter.frame.size.width + 5);
  farAwayFilter.layer.cornerRadius = 5;
  farAwayFilter.layer.borderWidth = 1;
  farAwayFilter.layer.borderColor = Rgb2UIColor(201, 201, 201).CGColor;
  [farAwayFilter setFrame:frame];
  [self addSubview:farAwayFilter];
  
  
  if ([[Neighborhood instance] advertisesWithPromotion]){
    [promotionFilter setTitleColor:Rgb2UIColor(245, 245, 245) forState:UIControlStateNormal];
    [promotionFilter setBackgroundColor:Rgb2UIColor(111, 111, 110)];
  }
  
  self.hightlightTitle = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title-highlight"]];
  [self.hightlightTitle setFrame:CGRectMake(filter.frame.origin.x, SumFrameYHeight(promotionFilter.frame) + 10, 290, 17)];
  [self addSubview:self.hightlightTitle];
  
  self.collectionViewTop = SumFrameYHeight(self.hightlightTitle.frame)+5;
}

- (void)createCollectionView{
  UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];

  self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(self.marginLeft,
                                                                           self.collectionViewTop,
                                                                           self.frame.size.width - (self.marginLeft * 2) - 5,
                                                                           self.frame.size.height - self.collectionViewTop)
                         collectionViewLayout:flowLayout];

  [self.collectionView setBackgroundColor:[UIColor clearColor]];
  [self.collectionView setShowsVerticalScrollIndicator:YES];
  [self.collectionView setShowsHorizontalScrollIndicator:NO];
  [self.collectionView setUserInteractionEnabled:YES];
  [self.collectionView setDataSource:self];
  [self.collectionView setDelegate:self];
  [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
  [self addSubview:self.collectionView];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
  if ([[searchBar text] length] < 3){
    [Constants alertWithMessage:@"Digite três ou mais letras para realizar a busca"];
    return;
  }
  
  [Constants removeAutocompleteResult];
  [searchBar resignFirstResponder];
  
  SearchResultViewController *vc = (SearchResultViewController *)[[self nextResponder] nextResponder];
  [vc setByText:[searchBar text]];
  [vc setByCategory:nil];
  [vc searchAdvertises];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
  if([searchText length] == 0){
    [Constants removeAutocompleteResult];
    [self.iconSearch setHidden:NO];
  }
  else{
    [self.iconSearch setHidden:YES];
    
    if ([searchText length] >= 4){
      [Constants autocompleteResultFor:searchBar showAbove:NO];
    }
    else{
      [Constants removeAutocompleteResult];
    }
  }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
  //self.boxSearch.placeholder = @"        Pizza, sushi, carro...";
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
  [self.boxSearch resignFirstResponder];
  [Constants removeAutocompleteResult];
}



-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
  return 1;
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
  //return  UIEdgeInsetsMake(5,self.marginLeft - 3, 5, 0);
  return UIEdgeInsetsZero;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  return [[[Neighborhood instance] neighborhoodAdvertises] count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
  if ([self isPayedAdvertise:indexPath]){
    return CGSizeMake(collectionView.frame.size.width, 98);
  }

  return CGSizeMake(collectionView.frame.size.width, 45);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
  
  cell.backgroundColor = [UIColor whiteColor];
  UIColor *textColor = Rgb2UIColor(114, 114, 114);
  
  if (![cell.contentView viewWithTag:AD_TITLE_VIEW]){
    UIImageView *adImage = [[UIImageView alloc] init];
    adImage.layer.cornerRadius = 5;
    adImage.clipsToBounds = YES;
    [adImage setTag:AD_IMAGE_VIEW];
    [cell.contentView addSubview:adImage];
    
    UIImageView *adOpen = [[UIImageView alloc] init];
    [adOpen setImage:[UIImage imageNamed:@"ad-open"]];
    [adOpen setHidden:YES];
    [adOpen setTag:AD_OPEN_VIEW];
    [cell.contentView addSubview:adOpen];
  
    UILabel *adTitle = [[UILabel alloc] init];
    [adTitle setBackgroundColor:[UIColor clearColor]];
    [adTitle setTag:AD_TITLE_VIEW];
    [adTitle setNumberOfLines:1];
    [adTitle setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-Hv" size:13.f]];
    [adTitle setTextColor:Rgb2UIColor(70, 68, 68)];
    [cell.contentView addSubview:adTitle];
    
    UILabel *adDetails = [[UILabel alloc] init];
    [adDetails setBackgroundColor:[UIColor clearColor]];
    [adDetails setTag:AD_DETAIL_VIEW];
    [adDetails setNumberOfLines:1];
    [adDetails setFont:[UIFont fontWithName:@"HelveticaNeue" size:9.f]];
    [adDetails setTextColor:textColor];
    [cell.contentView addSubview:adDetails];
    
    UIImageView *stars = [[UIImageView alloc] init];
    [stars setBackgroundColor:[UIColor clearColor]];
    [stars setImage:[UIImage imageNamed:@"bg-stars"]];
    [stars setTag:AD_STARS];
    [cell.contentView addSubview:stars];
    
    UIImageView *hasPromotion = [[UIImageView alloc] init];
    [hasPromotion setBackgroundColor:[UIColor clearColor]];
    [hasPromotion setImage:[UIImage imageNamed:@"has-promotion"]];
    [hasPromotion setTag:AD_HAS_PROMOTION_VIEW];
    [cell.contentView addSubview:hasPromotion];
  
    
    UIImageView *iconPhone = [[UIImageView alloc] init];
    [iconPhone setImage:[UIImage imageNamed:@"icon-phone"]];
    [iconPhone setTag:AD_PHONEICON_VIEW];
    [cell.contentView addSubview:iconPhone];
    
    UILabel *adPhone = [[UILabel alloc] init];
    [adPhone setNumberOfLines:1];
    [adPhone setBackgroundColor:[UIColor clearColor]];
    [adPhone setTag:AD_PHONE_VIEW];
    [adPhone setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-Hv" size:11.f]];
    [adPhone setTextColor:textColor];
    [cell.contentView addSubview:adPhone];
    
    UIView *grayLine = [[UIView alloc] init];
    [grayLine setBackgroundColor:Rgb2UIColor(229,229,229)];
    [grayLine setTag:AD_GREY_LINE];
    [cell.contentView addSubview:grayLine];


    UILabel *adAddress = [[UILabel alloc] init];
    [adAddress setNumberOfLines:1];
    [adAddress setBackgroundColor:[UIColor clearColor]];
    [adAddress setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-LtIt" size:9.f]];
    [adAddress setTag:AD_ADRESS_VIEW];
    [adAddress setTextColor:textColor];
    [cell.contentView addSubview:adAddress];
    
    UILabel *adDistance = [[UILabel alloc] init];
    [adDistance setNumberOfLines:1];
    [adDistance setBackgroundColor:[UIColor clearColor]];
    [adDistance setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-BdIt" size:9.f]];
    [adDistance setTag:AD_DISTANCE_VIEW];
    [adDistance setTextColor:textColor];
    [cell.contentView addSubview:adDistance];
  }
  
  NSDictionary *advertise = [[[Neighborhood instance] neighborhoodAdvertises] objectAtIndex:indexPath.row];
  
  if (indexPath.row == 0){
    if ([self isPayedAdvertise:indexPath]){
      [self.hightlightTitle setHidden:NO];
    }else{
      [self.hightlightTitle setHidden:YES];
    }
  }
  
  UICollectionViewCell *advertiseCell;
  if ([self isPayedAdvertise:indexPath]){
    advertiseCell = [self payedAdvertiseCell:cell withAd:advertise];
  }
  else{
    advertiseCell = [self freeAdvertiseCell:cell withAd:advertise];
  }
  
  return advertiseCell;
}

- (BOOL)isPayedAdvertise:(NSIndexPath *)indexPath{
  NSDictionary *advertise = [[[Neighborhood instance] neighborhoodAdvertises] objectAtIndex:indexPath.row];
  
  return [[advertise objectForKey:@"payed"] boolValue];
}

- (UICollectionViewCell *)payedAdvertiseCell:(UICollectionViewCell *)cell withAd:(NSDictionary *)advertise{
  CGFloat marginText = 5;
  CGFloat lastTop = 5;
  CGFloat marginBottom = 5;
  
  UIImageView *adImage = (UIImageView *)[cell.contentView viewWithTag:AD_IMAGE_VIEW];
  NSString *adImgeURL = [self advertiseImageURL:advertise];
  if ([adImgeURL isEqual:[NSNull null]]){
    [adImage setImage:[UIImage imageNamed:@"no-image"]];
  }else{
    [adImage sd_setImageWithURL:[NSURL URLWithString:adImgeURL] placeholderImage:[UIImage imageNamed:@"no-image"]];
  }
  [adImage setFrame:CGRectMake(marginText, lastTop, 89, 86)];
  [adImage setHidden:NO];
  
  
  CGFloat adOpenW = 38;
  
  CGFloat titleX = adImage.frame.size.width + marginText+5;
  [(UILabel *)[cell.contentView viewWithTag:AD_TITLE_VIEW] setText:[advertise objectForKey:@"title"]];
  [(UILabel *)[cell.contentView viewWithTag:AD_TITLE_VIEW] setFrame:CGRectMake(titleX, lastTop, cell.frame.size.width - titleX - adOpenW - 4, 14.5)];
  
  CGFloat adOpenX = titleX + [cell.contentView viewWithTag:AD_TITLE_VIEW].frame.size.width - 5;
  [(UILabel *)[cell.contentView viewWithTag:AD_OPEN_VIEW] setFrame:CGRectMake(adOpenX, lastTop, adOpenW, 14.5)];
  BOOL shouldHideAOpen = YES;
  if ([[advertise objectForKey:@"isopened"] boolValue]){
    shouldHideAOpen = NO;
  }
  [(UILabel *)[cell.contentView viewWithTag:AD_OPEN_VIEW] setHidden:shouldHideAOpen];
  
  lastTop += [cell.contentView viewWithTag:AD_OPEN_VIEW].frame.size.height;
  
  [(UILabel *)[cell.contentView viewWithTag:AD_DETAIL_VIEW] setText:[advertise objectForKey:@"details"]];
  [(UILabel *)[cell.contentView viewWithTag:AD_DETAIL_VIEW] setFrame:CGRectMake(titleX, lastTop, cell.frame.size.width - titleX, 15)];
  [(UILabel *)[cell.contentView viewWithTag:AD_DETAIL_VIEW] setHidden:NO];
  lastTop += [cell.contentView viewWithTag:AD_DETAIL_VIEW].frame.size.height+marginBottom;
  
  UIImageView *starsView = (UIImageView *)[cell.contentView viewWithTag:AD_STARS];
  [starsView setHidden:NO];
  [starsView setFrame:CGRectMake(titleX, lastTop, 107, 16)];
  
  CGFloat promotionX = starsView.frame.origin.x + starsView.frame.size.width + marginText;
  [(UIImageView *)[cell.contentView viewWithTag:AD_HAS_PROMOTION_VIEW] setFrame:CGRectMake(promotionX, lastTop, 70, 16)];
  NSArray *promotions = [advertise objectForKey:@"promotions"];
  if ([promotions count] == 0){
    [(UIImageView *)[cell.contentView viewWithTag:AD_HAS_PROMOTION_VIEW] setHidden:YES];
  }
  else{
    [(UIImageView *)[cell.contentView viewWithTag:AD_HAS_PROMOTION_VIEW] setHidden:NO];
  }
  
  lastTop += [cell.contentView viewWithTag:AD_HAS_PROMOTION_VIEW].frame.size.height+marginBottom;
  
  
  [(UILabel *)[cell.contentView viewWithTag:AD_PHONEICON_VIEW] setFrame:CGRectMake(titleX, lastTop, 16.5, 17)];
  
  CGFloat phoneX = [cell.contentView viewWithTag:AD_PHONEICON_VIEW].frame.size.width + [cell.contentView viewWithTag:AD_PHONEICON_VIEW].frame.origin.x;
  [(UILabel *)[cell.contentView viewWithTag:AD_PHONE_VIEW] setText:[[advertise objectForKey:@"store"] objectForKey:@"phone_summary"]];
  [(UILabel *)[cell.contentView viewWithTag:AD_PHONE_VIEW] setFrame:CGRectMake(phoneX, lastTop+3, cell.frame.size.width - phoneX, 15)];
  lastTop += [cell.contentView viewWithTag:AD_PHONE_VIEW].frame.size.height+marginBottom;
  
  
  [(UILabel *)[cell.contentView viewWithTag:AD_GREY_LINE] setFrame:CGRectMake(titleX, lastTop-1, cell.frame.size.width - titleX, 1)];
  [(UILabel *)[cell.contentView viewWithTag:AD_GREY_LINE] setHidden:NO];
  lastTop += [cell.contentView viewWithTag:AD_GREY_LINE].frame.size.height;
  
  [(UILabel *)[cell.contentView viewWithTag:AD_ADRESS_VIEW] setText:[[advertise objectForKey:@"store"] objectForKey:@"address"]];
  [(UILabel *)[cell.contentView viewWithTag:AD_ADRESS_VIEW] setFrame:CGRectMake(titleX, lastTop, 156, 15)];
  
  [(UILabel *)[cell.contentView viewWithTag:AD_DISTANCE_VIEW] setText:[[advertise objectForKey:@"store"] objectForKey:@"distance"]];
  [(UILabel *)[cell.contentView viewWithTag:AD_DISTANCE_VIEW] setFrame:CGRectMake(adOpenX+5, lastTop, 40, 15)];
  
  
  //Fill starsImageView background
  for (UIView *v in [starsView subviews]) {
    [v removeFromSuperview];
  }
  CGFloat rating = [[advertise objectForKey:@"rating"] floatValue];
  NSArray *ratingCp = nil;
  if (rating > 0){
    ratingCp = [[advertise objectForKey:@"rating"] componentsSeparatedByString:@"."];
  }
  
  CGFloat starsWidth = 10;
  CGRect lastStarFrame;
  CGFloat starX = 0;
  BOOL shouldAddHalf = NO;
  for (CGFloat x = 1; x <= 5; x++) {
    if (x == 1){
      starX = 3;
    }
    UIImageView *star = [[UIImageView alloc] initWithFrame:CGRectMake(starX, 3,starsWidth, 9.5)];
    [starsView addSubview:star];
    lastStarFrame = star.frame;
    
    if (ratingCp){
      if (x < [[ratingCp objectAtIndex:0] floatValue]){
        [star setImage:[UIImage imageNamed:@"icon-star-full"]];
      }
      if (x > [[ratingCp objectAtIndex:0] floatValue]){
        [star setImage:[UIImage imageNamed:@"icon-star-empty"]];
      }
      if (shouldAddHalf){
        [star setImage:[UIImage imageNamed:@"icon-star-half"]];
        shouldAddHalf = NO;
      }
      if (x == [[ratingCp objectAtIndex:0] floatValue]){
        [star setImage:[UIImage imageNamed:@"icon-star-full"]];
        
        if ([[ratingCp objectAtIndex:1] floatValue] > 0){
          shouldAddHalf = YES;
        }
      }
      
    }
    else{
      [star setImage:[UIImage imageNamed:@"icon-star-empty"]];
    }
    starX = SumFrameXWidth(star.frame);
  }
  
  NSInteger totalRate = [[advertise objectForKey:@"rate_count"] integerValue];
  NSString *rateText = @"avaliação";
  if (totalRate > 0){
    rateText = @"avaliações";
  }
  UILabel *rateCount = [[UILabel alloc] initWithFrame:CGRectMake(SumFrameXWidth(lastStarFrame)+5, lastStarFrame.origin.y - 3, 100, starsView.frame.size.height - 2)];
  [rateCount setText:[NSString stringWithFormat:@"%ld %@", (long)totalRate, rateText]];
  [rateCount setTextColor:Rgb2UIColor(114, 114, 114)];
  [rateCount setFont:[UIFont fontWithName:@"HelveticaNeue" size:8.f]];
  [starsView addSubview:rateCount];
  
  
  return cell;
}

- (UICollectionViewCell *)freeAdvertiseCell:(UICollectionViewCell *)cell withAd:(NSDictionary *)advertise{
  CGFloat marginText = 5;
  CGFloat lastTop = 5;
  
  CGFloat adOpenW = 38;
  
  CGFloat titleX = marginText;
  [(UILabel *)[cell.contentView viewWithTag:AD_TITLE_VIEW] setText:[advertise objectForKey:@"title"]];
  [(UILabel *)[cell.contentView viewWithTag:AD_TITLE_VIEW] setFrame:CGRectMake(titleX, lastTop, cell.frame.size.width - titleX - adOpenW - 4, 14.5)];
  
  CGFloat adOpenX = titleX + [cell.contentView viewWithTag:AD_TITLE_VIEW].frame.size.width - 5;
  [(UILabel *)[cell.contentView viewWithTag:AD_OPEN_VIEW] setFrame:CGRectMake(adOpenX, lastTop, adOpenW, 14.5)];
  BOOL shouldHideAOpen = YES;
  if ([[advertise objectForKey:@"isopened"] boolValue]){
    shouldHideAOpen = NO;
  }
  [(UILabel *)[cell.contentView viewWithTag:AD_OPEN_VIEW] setHidden:shouldHideAOpen];
  
  lastTop += [cell.contentView viewWithTag:AD_TITLE_VIEW].frame.size.height+8;
  
  [(UILabel *)[cell.contentView viewWithTag:AD_DISTANCE_VIEW] setText:[[advertise objectForKey:@"store"] objectForKey:@"distance"]];
  [(UILabel *)[cell.contentView viewWithTag:AD_DISTANCE_VIEW] setFrame:CGRectMake(titleX, lastTop, 35, 15)];
  
  CGFloat addressX = SumFrameXWidth([cell.contentView viewWithTag:AD_DISTANCE_VIEW].frame);
  [(UILabel *)[cell.contentView viewWithTag:AD_ADRESS_VIEW] setText:[[advertise objectForKey:@"store"] objectForKey:@"address"]];
  [(UILabel *)[cell.contentView viewWithTag:AD_ADRESS_VIEW] setFrame:CGRectMake(addressX, lastTop, 156, 15)];
  
  CGFloat phoneIconX = SumFrameXWidth([cell.contentView viewWithTag:AD_ADRESS_VIEW].frame);;
  [(UILabel *)[cell.contentView viewWithTag:AD_PHONEICON_VIEW] setFrame:CGRectMake(phoneIconX, lastTop-3, 16.5, 17)];
  
  CGFloat phoneX = SumFrameXWidth([cell.contentView viewWithTag:AD_PHONEICON_VIEW].frame);;
  [(UILabel *)[cell.contentView viewWithTag:AD_PHONE_VIEW] setText:[[advertise objectForKey:@"store"] objectForKey:@"phone_summary"]];
  [(UILabel *)[cell.contentView viewWithTag:AD_PHONE_VIEW] setFrame:CGRectMake(phoneX, lastTop, cell.frame.size.width - phoneX, 15)];
  
  
  [(UILabel *)[cell.contentView viewWithTag:AD_GREY_LINE] setHidden:YES];
  [(UILabel *)[cell.contentView viewWithTag:AD_DETAIL_VIEW] setHidden:YES];
  [(UIImageView *)[cell.contentView viewWithTag:AD_STARS] setHidden:YES];
  [(UIImageView *)[cell.contentView viewWithTag:AD_HAS_PROMOTION_VIEW] setHidden:YES];
  [(UIImageView *)[cell.contentView viewWithTag:AD_IMAGE_VIEW] setHidden:YES];
  
  return cell;
}

- (NSString *)advertiseImageURL:(NSDictionary *)advertise{
  return [[advertise objectForKey:@"highlight_image"] objectForKey:@"url"];
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
  
  DetailViewController *detailVC = [DetailViewController new];
  [detailVC setAdvertiseIndex:indexPath.row];
  [[(UIViewController *)[[self nextResponder] nextResponder] navigationController] pushViewController:detailVC animated:YES];
}

- (void)showContentModal:(UIView *)content{
  CGRect windowFrame = [[[UIApplication sharedApplication] keyWindow] frame];
  
  self.backgroundModal = [[UIView alloc] initWithFrame:CGRectMake(0, 0, windowFrame.size.width, windowFrame.size.height)];
  [self.backgroundModal setBackgroundColor:[UIColor blackColor]];
  [self.backgroundModal setAlpha:0.7];
  [self.backgroundModal setTag:2000];
  [[(UIViewController *)[[self nextResponder] nextResponder] view] addSubview:self.backgroundModal];
  
  UITapGestureRecognizer *singleBackgroundModalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleBackgroundModalTap:)];
  [[(UIViewController *)[[self nextResponder] nextResponder] view] addGestureRecognizer:singleBackgroundModalTap];
  
  self.contentModalView = content;
  [self.contentModalView setFrame:CGRectMake(10, (self.backgroundModal.frame.size.height / 2) - 100, windowFrame.size.width - 20, (self.backgroundModal.frame.size.height / 2))];
  [self.contentModalView setBackgroundColor:[UIColor whiteColor]];
  
  [[(UIViewController *)[[self nextResponder] nextResponder] view] addSubview:self.contentModalView];
  [UIView animateWithDuration:0.50 animations:^{self.contentModalView.alpha = 1;}];
}


- (void)closeContentModal{
  if (self.contentModalView){
    [UIView animateWithDuration:0.50 animations:^{self.contentModalView.alpha = 0;}];
    [UIView animateWithDuration:0.50 animations:^{self.backgroundModal.alpha = 0;}];
    [self performSelector:@selector(resetContentModal) withObject:self afterDelay:1];
  }
}

- (void)resetContentModal{
  [self.contentModalView removeFromSuperview];
  [self.backgroundModal removeFromSuperview];
  self.backgroundModal = nil;
  self.contentModalView = nil;
}

- (void)showFilterModal{
  [self.boxSearch resignFirstResponder];
  CGRect windowFrame = [[[UIApplication sharedApplication] keyWindow] frame];
  
  UIView *content = [[UIView alloc] init];
  
  CGFloat headerW = 250.5;
  CGFloat headerX = (windowFrame.size.width - 20 - headerW) / 2;
  UIImageView *header = [[UIImageView alloc] initWithFrame:CGRectMake(headerX, 10, headerW, 12.5)];
  [header setImage:[UIImage imageNamed:@"title-filter"]];
  [content addSubview:header];
  
  UIView *deliveryView = [[UIView alloc] initWithFrame:CGRectMake(headerX-10, SumFrameYHeight(header.frame)+10, headerW+20, 40)];
  [deliveryView setBackgroundColor:Rgb2UIColor(238, 238, 238)];
  deliveryView.layer.cornerRadius = 5;
  [content addSubview:deliveryView];
  
  UILabel *deliveryTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 130, 30)];
  [deliveryTitle setText:@"Atende em Domicílio"];
  [deliveryTitle setTextColor:Rgb2UIColor(114, 114, 114)];
  [deliveryTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.f]];
  [deliveryView addSubview:deliveryTitle];
  
  NSString *btDeliveryOnImageName = @"icon-switch-neutral";
  NSString *btDeliveryOffImageName = @"icon-switch-off";
  if ([[Neighborhood instance] advertisesWithDelivery]){
    btDeliveryOnImageName = @"icon-switch-on";
    btDeliveryOffImageName = @"icon-switch-neutral";
  }
  
  self.btDeliveryOn = [UIButton buttonWithType:UIButtonTypeCustom];
  [self.btDeliveryOn setBackgroundImage:[UIImage imageNamed:btDeliveryOnImageName] forState:UIControlStateNormal];
  [self.btDeliveryOn setTag:3000];
  [self.btDeliveryOn setFrame:CGRectMake(SumFrameXWidth(deliveryTitle.frame), deliveryTitle.frame.origin.y+5, 25, 25)];
  [self.btDeliveryOn addTarget:self action:@selector(changeDeliveryState:) forControlEvents:UIControlEventTouchUpInside];
  [deliveryView addSubview:self.btDeliveryOn];
  
  UILabel *deliveryOnTitle = [[UILabel alloc] initWithFrame:CGRectMake(SumFrameXWidth(self.btDeliveryOn.frame),
                                                                       self.btDeliveryOn.frame.origin.y+3, 40, 20)];
  [deliveryOnTitle setText:@"Ligado"];
  [deliveryOnTitle setTextColor:Rgb2UIColor(114, 114, 114)];
  [deliveryOnTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:10.f]];
  [deliveryView addSubview:deliveryOnTitle];
  
  self.btDeliveryOff = [UIButton buttonWithType:UIButtonTypeCustom];
  [self.btDeliveryOff setBackgroundImage:[UIImage imageNamed:btDeliveryOffImageName] forState:UIControlStateNormal];
  [self.btDeliveryOff setTag:3001];
  [self.btDeliveryOff setFrame:CGRectMake(SumFrameXWidth(deliveryOnTitle.frame), self.btDeliveryOn.frame.origin.y, 25, 25)];
  [self.btDeliveryOff addTarget:self action:@selector(changeDeliveryState:) forControlEvents:UIControlEventTouchUpInside];
  [deliveryView addSubview:self.btDeliveryOff];
  
  UILabel *deliveryOffTitle = [[UILabel alloc] initWithFrame:CGRectMake(SumFrameXWidth(self.btDeliveryOff.frame),
                                                                       self.btDeliveryOff.frame.origin.y+3, 45, 20)];
  [deliveryOffTitle setText:@"Desligado"];
  [deliveryOffTitle setTextColor:Rgb2UIColor(114, 114, 114)];
  [deliveryOffTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:10.f]];
  [deliveryView addSubview:deliveryOffTitle];
  
  
  UIView *maxDistanceView = [[UIView alloc] initWithFrame:CGRectMake(deliveryView.frame.origin.x, SumFrameYHeight(deliveryView.frame)+10, deliveryView.frame.size.width, 70)];
  [maxDistanceView setBackgroundColor:Rgb2UIColor(238, 238, 238)];
  [maxDistanceView setTag:MAX_DISTANCE_VIEW];
  maxDistanceView.layer.cornerRadius = 5;
  [content addSubview:maxDistanceView];
  
  UILabel *maxDistanceTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 130, 20)];
  [maxDistanceTitle setText:@"Distância máxima"];
  [maxDistanceTitle setTextColor:Rgb2UIColor(114, 114, 114)];
  [maxDistanceTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.f]];
  [maxDistanceView addSubview:maxDistanceTitle];
  
  
  self.btMaxDistanceA = [UIButton buttonWithType:UIButtonTypeCustom];
  [self.btMaxDistanceA setFrame:CGRectMake(maxDistanceTitle.frame.origin.x, SumFrameYHeight(maxDistanceTitle.frame)+5, 25, 25)];
  [self.btMaxDistanceA setBackgroundImage:[UIImage imageNamed:@"icon-switch-neutral"] forState:UIControlStateNormal];
  [self.btMaxDistanceA setTag:4005];
  [self.btMaxDistanceA addTarget:self action:@selector(changeMaxDistanceState:) forControlEvents:UIControlEventTouchUpInside];
  [maxDistanceView addSubview:self.btMaxDistanceA];
  
  UILabel *distanceTitleA = [[UILabel alloc] initWithFrame:CGRectMake(SumFrameXWidth(self.btMaxDistanceA.frame),
                                                                        self.btMaxDistanceA.frame.origin.y + 8, 28, 10)];
  [distanceTitleA setText:@"5km"];
  [distanceTitleA setTextColor:Rgb2UIColor(114, 114, 114)];
  [distanceTitleA setFont:[UIFont fontWithName:@"HelveticaNeue" size:9.f]];
  [maxDistanceView addSubview:distanceTitleA];
  
  self.btMaxDistanceB = [UIButton buttonWithType:UIButtonTypeCustom];
  [self.btMaxDistanceB setFrame:CGRectMake(SumFrameXWidth(distanceTitleA.frame), self.btMaxDistanceA.frame.origin.y, 25, 25)];
  [self.btMaxDistanceB setBackgroundImage:[UIImage imageNamed:@"icon-switch-neutral"] forState:UIControlStateNormal];
  [self.btMaxDistanceB setTag:4010];
  [self.btMaxDistanceB addTarget:self action:@selector(changeMaxDistanceState:) forControlEvents:UIControlEventTouchUpInside];
  [maxDistanceView addSubview:self.btMaxDistanceB];
  
  UILabel *distanceTitleB = [[UILabel alloc] initWithFrame:CGRectMake(SumFrameXWidth(self.btMaxDistanceB.frame),
                                                                          distanceTitleA.frame.origin.y, distanceTitleA.frame.size.width, 10)];
  [distanceTitleB setText:@"10km"];
  [distanceTitleB setTextColor:Rgb2UIColor(114, 114, 114)];
  [distanceTitleB setFont:[UIFont fontWithName:@"HelveticaNeue" size:9.f]];
  [maxDistanceView addSubview:distanceTitleB];
  
  self.btMaxDistanceC = [UIButton buttonWithType:UIButtonTypeCustom];
  [self.btMaxDistanceC setFrame:CGRectMake(SumFrameXWidth(distanceTitleB.frame), self.btMaxDistanceA.frame.origin.y, 25, 25)];
  [self.btMaxDistanceC setBackgroundImage:[UIImage imageNamed:@"icon-switch-neutral"] forState:UIControlStateNormal];
  [self.btMaxDistanceC setTag:4015];
  [self.btMaxDistanceC addTarget:self action:@selector(changeMaxDistanceState:) forControlEvents:UIControlEventTouchUpInside];
  [maxDistanceView addSubview:self.btMaxDistanceC];
  
  UILabel *distanceTitleC = [[UILabel alloc] initWithFrame:CGRectMake(SumFrameXWidth(self.btMaxDistanceC.frame),
                                                                      distanceTitleA.frame.origin.y, distanceTitleA.frame.size.width, 10)];
  [distanceTitleC setText:@"15km"];
  [distanceTitleC setTextColor:Rgb2UIColor(114, 114, 114)];
  [distanceTitleC setFont:[UIFont fontWithName:@"HelveticaNeue" size:9.f]];
  [maxDistanceView addSubview:distanceTitleC];
  
  self.btMaxDistanceD = [UIButton buttonWithType:UIButtonTypeCustom];
  [self.btMaxDistanceD setFrame:CGRectMake(SumFrameXWidth(distanceTitleC.frame), self.btMaxDistanceA.frame.origin.y, 25, 25)];
  [self.btMaxDistanceD setBackgroundImage:[UIImage imageNamed:@"icon-switch-selected"] forState:UIControlStateNormal];
  [self.btMaxDistanceD setTag:4000];
  [self.btMaxDistanceD addTarget:self action:@selector(changeMaxDistanceState:) forControlEvents:UIControlEventTouchUpInside];
  [maxDistanceView addSubview:self.btMaxDistanceD];
  
  UILabel *distanceTitleD = [[UILabel alloc] initWithFrame:CGRectMake(SumFrameXWidth(self.btMaxDistanceD.frame),
                                                                      distanceTitleA.frame.origin.y, 90, 10)];
  [distanceTitleD setText:@"Qualquer distância"];
  [distanceTitleD setTextColor:Rgb2UIColor(114, 114, 114)];
  [distanceTitleD setFont:[UIFont fontWithName:@"HelveticaNeue" size:9.f]];
  [maxDistanceView addSubview:distanceTitleD];
  
  NSInteger selectedMaxDistance = [[Neighborhood instance] advertisesMaxDistance];
  NSInteger maxDistanceEl;
  for (UIView *v in [maxDistanceView subviews]){
    if ([v tag] > 3999 && [v tag] < 4020){
      maxDistanceEl = ([v tag] - 4000);
      if (maxDistanceEl == selectedMaxDistance){
        [(UIButton *)v setBackgroundImage:[UIImage imageNamed:@"icon-switch-selected"] forState:UIControlStateNormal];
      }
      else{
        [(UIButton *)v setBackgroundImage:[UIImage imageNamed:@"icon-switch-neutral"] forState:UIControlStateNormal];
      }
    }
  }
  
  UIView *sortView = [[UIView alloc] initWithFrame:CGRectMake(deliveryView.frame.origin.x, SumFrameYHeight(maxDistanceView.frame)+10, deliveryView.frame.size.width, 115)];
  [sortView setBackgroundColor:Rgb2UIColor(238, 238, 238)];
  [sortView setTag:SORT_VIEW];
  sortView.layer.cornerRadius = 5;
  [content addSubview:sortView];
  
  UILabel *sortTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 130, 25)];
  [sortTitle setText:@"Ordernar por"];
  [sortTitle setTextColor:Rgb2UIColor(114, 114, 114)];
  [sortTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.f]];
  [sortView addSubview:sortTitle];
  
  NSArray *sortOptions = [NSArray arrayWithObjects:
                          @"Nome crescente",
                          @"Avalição crescente",
                          @"Nome decrescente",
                          @"Avaliação decrescente",
                          @"Mais pertos de você", nil];
  
  CGFloat sortOptionY = SumFrameYHeight(sortTitle.frame);
  CGFloat sortOptionX = 3;
  for (int x = 0; x < [sortOptions count]; x++) {
    NSString *sortImageName = @"icon-switch-neutral";
    if (x == ([sortOptions count] - 1)){
      sortImageName = @"icon-switch-selected";
    }
    UIButton *sortOption = [UIButton buttonWithType:UIButtonTypeCustom];
    [sortOption setFrame:CGRectMake(sortOptionX, sortOptionY, 21, 21)];
    [sortOption setBackgroundImage:[UIImage imageNamed:sortImageName] forState:UIControlStateNormal];
    [sortOption setTag:6000+x];
    [sortOption addTarget:self action:@selector(changeSortOptionsState:) forControlEvents:UIControlEventTouchUpInside];
    [sortView addSubview:sortOption];
    
    CGFloat sortTitleWidth = 100;
    if (x == 3){
      sortTitleWidth = 120;
    }
    if (x == 4){
      sortTitleWidth = 120;
    }
    
    UILabel *sortTitle = [[UILabel alloc] initWithFrame:CGRectMake(SumFrameXWidth(sortOption.frame),
                                                                        sortOption.frame.origin.y+6, sortTitleWidth, 10)];
    [sortTitle setText:[sortOptions objectAtIndex:x]];
    [sortTitle setTextColor:Rgb2UIColor(114, 114, 114)];
    [sortTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:11.f]];
    [sortView addSubview:sortTitle];
    
    if (x == 1 || x == 3){
      sortOptionX = 3;
      sortOptionY += 30;
    }
    else{
      sortOptionX = SumFrameXWidth(sortTitle.frame)+6;
    }
    
  }
  
  if ([[Neighborhood instance] advertisesSort]){
    NSArray *sortOptionsQuery = [NSArray arrayWithObjects:@"title asc",@"rating asc",@"title desc",@"rating desc",@"",nil];
    NSInteger tag = 6000 + [sortOptionsQuery indexOfObject:[[Neighborhood instance] advertisesSort]];
    for (UIView *v in [[content viewWithTag:SORT_VIEW] subviews]) {
      if ([v tag] >= 6000 && [v tag] <= 6004){
        if ([v tag] == tag){
          [[Neighborhood instance] setAdvertisesSort: [sortOptionsQuery objectAtIndex:(tag - 6000)]];
          [(UIButton *)v setBackgroundImage:[UIImage imageNamed:@"icon-switch-selected"] forState:UIControlStateNormal];
        }
        else{
          [(UIButton *)v setBackgroundImage:[UIImage imageNamed:@"icon-switch-neutral"] forState:UIControlStateNormal];
        }
      }
    }
  }
  
  
  [self showContentModal:content];
}

- (void)changeDeliveryState:(id)sender{
  if ([sender tag] == 3000){
    [[Neighborhood instance] setAdvertisesWithDelivery:YES];
    [self.btDeliveryOn setBackgroundImage:[UIImage imageNamed:@"icon-switch-on"] forState:UIControlStateNormal];
    [self.btDeliveryOff setBackgroundImage:[UIImage imageNamed:@"icon-switch-neutral"] forState:UIControlStateNormal];
  }else{
    [[Neighborhood instance] setAdvertisesWithDelivery:NO];
    [self.btDeliveryOn setBackgroundImage:[UIImage imageNamed:@"icon-switch-neutral"] forState:UIControlStateNormal];
    [self.btDeliveryOff setBackgroundImage:[UIImage imageNamed:@"icon-switch-off"] forState:UIControlStateNormal];
  }
  
  [self filterAdvertises];
}

- (void)changeMaxDistanceState:(id)sender{
  NSInteger senderTag = [sender tag];
  
  UIView *maxDistanceView = [self.contentModalView viewWithTag:MAX_DISTANCE_VIEW];
  for (UIView *v in [maxDistanceView subviews]) {
    if ([v tag] > 3999 && [v tag] < 4020){
      if ([v tag] == senderTag){
        [[Neighborhood instance] setAdvertisesMaxDistance: (senderTag - 4000)];
        [(UIButton *)v setBackgroundImage:[UIImage imageNamed:@"icon-switch-selected"] forState:UIControlStateNormal];
      }
      else{
        [(UIButton *)v setBackgroundImage:[UIImage imageNamed:@"icon-switch-neutral"] forState:UIControlStateNormal];
      }
    }
  }
  
  [self filterAdvertises];
}

- (void)changeSortOptionsState:(id)sender{
  NSInteger senderTag = [sender tag];
  
  NSArray *sortOptionsQuery = [NSArray arrayWithObjects:@"title asc",@"rating asc",@"title desc",@"rating desc",@"",nil];
  
  UIView *sortView = [self.contentModalView viewWithTag:SORT_VIEW];
  for (UIView *v in [sortView subviews]) {
    if ([v tag] >= 6000 && [v tag] <= 6004){
      if ([v tag] == senderTag){
        [[Neighborhood instance] setAdvertisesSort: [sortOptionsQuery objectAtIndex:(senderTag - 6000)]];
        [(UIButton *)v setBackgroundImage:[UIImage imageNamed:@"icon-switch-selected"] forState:UIControlStateNormal];
      }
      else{
        [(UIButton *)v setBackgroundImage:[UIImage imageNamed:@"icon-switch-neutral"] forState:UIControlStateNormal];
      }
    }
  }
  
  [self filterAdvertises];
}



- (void)showAdvertisesMap{
  [(SearchResultViewController *)[[self nextResponder] nextResponder] showResultAsMap];
}


- (void)exposedFilterClicked:(id)sender{
  UIButton *clickedButton = (UIButton *)sender;
  switch ([clickedButton tag]) {
      case PROMOTION_FILTER_BUTTON:
        [[Neighborhood instance] setAdvertisesWithPromotion: ![[Neighborhood instance] advertisesWithPromotion]];
      break;
      case OPENNOW_FILTER_BUTTON:
        [[Neighborhood instance] setAdvertisesWithOpened: ![[Neighborhood instance] advertisesWithOpened]];
      break;
      case FARAWAY_FILTER_BUTTON:
        [[Neighborhood instance] setAdvertisesWithMoreDistance: ![[Neighborhood instance] advertisesWithMoreDistance]];
      break;
    default:
      break;
  }
  [self changeButtonBgColorToTextColor:clickedButton];
  [self filterAdvertises];
}

- (void)changeButtonBgColorToTextColor:(UIButton *)bt{
  UIColor *titleColor = [[bt titleLabel] textColor];
  UIColor *bgColor = [bt backgroundColor];
  
  [bt setTitleColor:bgColor forState:UIControlStateNormal];
  [bt setBackgroundColor:titleColor];
}

- (void)filterAdvertises{
  [self closeContentModal];
  [Constants showIndicatorAtView:self];

  SearchResultViewController *vc = (SearchResultViewController *)[[self nextResponder] nextResponder];
  [vc setByHasPromotion:[[Neighborhood instance] advertisesWithPromotion]];
  [vc searchAdvertises];
}

- (void)handleBackgroundModalTap:(UITapGestureRecognizer *)recognizer {
  UIView* view = recognizer.view;
  CGPoint loc = [recognizer locationInView:view];
  UIView* subview = [view hitTest:loc withEvent:nil];
  if (subview.tag == self.backgroundModal.tag){
    [self closeContentModal];
  }
  
}


@end
