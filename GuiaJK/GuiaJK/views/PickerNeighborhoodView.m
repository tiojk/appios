//
//  PickerNeighborhoodView.m
//  GuiaJK
//
//  Created by pierreabreup on 6/26/16.
//  Copyright © 2016 Hand Mob. All rights reserved.
//

#import "PickerNeighborhoodView.h"
#import "Constants.h"
#import "Neighborhood.h"
#import "BaseViewController.h"

@interface PickerNeighborhoodView()
  @property UISearchBar *boxSearch;
  @property UIImageView *iconSearch;
  @property UICollectionView *collectionView;
  @property UIScrollView *scrollView;
  @property NSMutableArray *neighsForSearchAds;
  @property CGFloat lastScrollChildrenX;
@end

@implementation PickerNeighborhoodView

- (void)drawRect:(CGRect)rect {
  self.neighsForSearchAds = [NSMutableArray array];
  
  UIImageView *title = [[UIImageView alloc] initWithFrame:CGRectMake(20, 10, 267, 19.5)];
  [title setImage:[UIImage imageNamed:@"title-add-neighborhood"]];
  [self addSubview:title];
  
  self.boxSearch = [[UISearchBar alloc] initWithFrame:CGRectMake(title.frame.origin.x-7, SumFrameYHeight(title.frame)+5, title.frame.size.width, 35)];
  [self.boxSearch setSearchFieldBackgroundImage:[UIImage imageNamed:@"bg-box-search-autocom-neigh"] forState:UIControlStateNormal];
  [self.boxSearch setBackgroundColor:[UIColor clearColor]];
  [self.boxSearch setDelegate:self];
  [self.boxSearch setImage:[UIImage imageNamed:@"blank"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
  self.boxSearch.searchBarStyle = UISearchBarStyleMinimal;
  self.boxSearch.placeholder=@"         ";
  [self addSubview:self.boxSearch];
  
  UIButton* iconCloseKeyboard = [UIButton buttonWithType:UIButtonTypeCustom];
  [iconCloseKeyboard setBackgroundImage:[UIImage imageNamed:@"icon-close-keyboard"] forState:UIControlStateNormal];
  [iconCloseKeyboard setFrame:CGRectMake(SumFrameXWidth(self.boxSearch.frame)+2, self.boxSearch.frame.origin.y+2, 33, 30)];
  [iconCloseKeyboard addTarget:self action:@selector(closeKeyboard) forControlEvents:UIControlEventTouchUpInside];
  [self addSubview:iconCloseKeyboard];
  
  self.iconSearch = [[UIImageView alloc] initWithFrame:CGRectMake(10, 7, 20, 22)];
  [self.iconSearch setImage:[UIImage imageNamed:@"icon-search"]];
  [self.boxSearch addSubview:self.iconSearch];
  
  CGRect frame = title.frame;
  frame.origin.y = SumFrameYHeight(self.boxSearch.frame)+10;
  
  if ([[[Neighborhood instance] nearbyNeighborhoods] count] > 0){
    title = [[UIImageView alloc] initWithFrame:frame];
    [title setImage:[UIImage imageNamed:@"title-add-near-neighbor"]];
    [self addSubview:title];
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(title.frame.origin.x,
                                                                             SumFrameYHeight(title.frame)+10,
                                                                             title.frame.size.width,
                                                                             60)
                                             collectionViewLayout:[[UICollectionViewFlowLayout alloc] init]];
    
    [self.collectionView setBackgroundColor:[UIColor clearColor]];
    [self.collectionView setShowsVerticalScrollIndicator:YES];
    [self.collectionView setShowsHorizontalScrollIndicator:NO];
    [self.collectionView setUserInteractionEnabled:YES];
    [self.collectionView setDataSource:self];
    [self.collectionView setDelegate:self];
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"nearNeighBorCellIdentifier"];
    [self addSubview:self.collectionView];
    
    frame.origin.y = SumFrameYHeight(self.collectionView.frame)+10;

  }
  
  title = [[UIImageView alloc] initWithFrame:frame];
  [title setImage:[UIImage imageNamed:@"title-added-neighbor"]];
  [self addSubview:title];
  
  frame.origin.y = SumFrameYHeight(title.frame)+10;
  frame.size.height = 35;
  self.scrollView = [[UIScrollView alloc] initWithFrame:frame];
  [self.scrollView setShowsVerticalScrollIndicator:NO];
  [self.scrollView setShowsHorizontalScrollIndicator:YES];
  [self.scrollView setBackgroundColor:[UIColor clearColor]];
  [self.scrollView setUserInteractionEnabled:YES];
  [self addSubview:self.scrollView];
  
  [self addDefaultSelectedNeighborhoods];
  
  [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width,self.scrollView.frame.size.height)];
  
  
  UIButton* btSearch = [UIButton buttonWithType:UIButtonTypeCustom];
  [btSearch setBackgroundImage:[UIImage imageNamed:@"bt-search-ad-for-neighbors"] forState:UIControlStateNormal];
  [btSearch setFrame:CGRectMake(frame.origin.x, SumFrameYHeight(self.scrollView.frame)+10, 267, 33)];
  [btSearch addTarget:self action:@selector(searchAdvertises) forControlEvents:UIControlEventTouchUpInside];
  [self addSubview:btSearch];
  
}

- (void)addDefaultSelectedNeighborhoods{
  NSArray *defaultNeighborhoods;
  if ([[[Neighborhood instance] extraNeighborhoodName] length] > 0){
    defaultNeighborhoods = [[[Neighborhood instance] extraNeighborhoodName] componentsSeparatedByString:@","];
  }
  else{
    defaultNeighborhoods = [NSArray arrayWithObject:[[Neighborhood instance] currentNeighborhoodName]];
  }
  
  self.lastScrollChildrenX = 0;
  
  [self.neighsForSearchAds removeAllObjects];
  for (UIView *v in self.scrollView.subviews) {
    if ([v isKindOfClass:[UIButton class]]){
      [v removeFromSuperview];
    }
    
  }
  
  for (int x=0; x < [defaultNeighborhoods count]; x++) {
    NSString *neighBorName = [defaultNeighborhoods objectAtIndex:x];
    [self.neighsForSearchAds addObject:neighBorName];
    
    UIButton* neighbor = [UIButton buttonWithType:UIButtonTypeCustom];
    [neighbor setTitle:neighBorName forState:UIControlStateNormal];
    [neighbor.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.f]];
    [neighbor setBackgroundColor:Rgb2UIColor(153, 153, 153)];
    [neighbor setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [neighbor setFrame:CGRectMake(self.lastScrollChildrenX, 0, ([neighBorName length] * 11), 25)];
    [neighbor addTarget:self action:@selector(removeNeighborsForSearch:) forControlEvents:UIControlEventTouchUpInside];
    [neighbor setTag:600];
    neighbor.layer.cornerRadius = 5;
    neighbor.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    neighbor.contentEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    
    UIImageView *iconClose = [[UIImageView alloc] initWithFrame:CGRectMake(neighbor.frame.size.width - 15, 6, 13, 13.5)];
    [iconClose setImage:[UIImage imageNamed:@"icon-close-grey"]];
    [neighbor addSubview:iconClose];
    
    [self.scrollView addSubview:neighbor];
    
    self.lastScrollChildrenX = SumFrameXWidth(neighbor.frame)+5;
  }

}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
  if([searchText length] == 0){
    [self.iconSearch setHidden:NO];
    [Constants removeAutocompleteResult];
  }
  else{
    [self.iconSearch setHidden:YES];
    if ([searchText length] >= 4){
      [Constants autocompleteNeighborResultFor:searchBar andPickerView:self showAbove:NO];
    }
    else{
      [Constants removeAutocompleteResult];
    }
  }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
  [Constants removeAutocompleteResult];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
  if ([[searchBar text] length] < 5){
    [Constants alertWithMessage:@"Digite três ou mais letras para realizar a busca"];
    return;
  }
  
  [Constants removeAutocompleteResult];
  [searchBar resignFirstResponder];
  
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
  return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  return [[[Neighborhood instance] nearbyNeighborhoods] count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
  NSString *neighBorName = [[[Neighborhood instance] nearbyNeighborhoods] objectAtIndex:indexPath.row];
  return CGSizeMake(([neighBorName length] * 9), 25);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"nearNeighBorCellIdentifier" forIndexPath:indexPath];
  
  cell.backgroundColor = [UIColor whiteColor];
  UIButton* neighbor = [UIButton buttonWithType:UIButtonTypeCustom];
  [neighbor setTitle:[[[Neighborhood instance] nearbyNeighborhoods] objectAtIndex:indexPath.row] forState:UIControlStateNormal];
  [neighbor setBackgroundColor:Rgb2UIColor(209, 209, 209)];
  [neighbor setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [neighbor setFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
  [neighbor.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.f]];
  [neighbor addTarget:self action:@selector(addNeighborsForSearchFromCollection:) forControlEvents:UIControlEventTouchUpInside];
  neighbor.layer.cornerRadius = 5;
  [cell.contentView addSubview:neighbor];
  
  
  return cell;
}

- (void)addNeighborsForSearchByName:(NSString *)name{
  [self.neighsForSearchAds addObject:name];
  UIButton* neighbor = [UIButton buttonWithType:UIButtonTypeCustom];
  [neighbor setTitle:name forState:UIControlStateNormal];
  [neighbor.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.f]];
  [neighbor setBackgroundColor:Rgb2UIColor(153, 153, 153)];
  [neighbor setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [neighbor setFrame:CGRectMake(self.lastScrollChildrenX, 0, ([name length] * 10), 25)];
  [neighbor addTarget:self action:@selector(removeNeighborsForSearch:) forControlEvents:UIControlEventTouchUpInside];
  [neighbor setTag:(600 + self.neighsForSearchAds.count - 1)];
  neighbor.layer.cornerRadius = 5;
  neighbor.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
  neighbor.contentEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
  
  UIImageView *iconClose = [[UIImageView alloc] initWithFrame:CGRectMake(neighbor.frame.size.width - 15, 6, 13, 13.5)];
  [iconClose setImage:[UIImage imageNamed:@"icon-close-grey"]];
  [neighbor addSubview:iconClose];
  
  [self.scrollView addSubview:neighbor];
  
  self.lastScrollChildrenX += neighbor.frame.size.width + 5;
  
  [self changeScrollViewContentSize];
  
  [self.boxSearch resignFirstResponder];
  [Constants removeAutocompleteResult];
  
}


- (void)addNeighborsForSearchFromCollection:(id)sender{
  NSString *title = [[(UIButton *)sender titleLabel] text];
  [self.neighsForSearchAds addObject:title];
  UIButton* neighbor = [UIButton buttonWithType:UIButtonTypeCustom];
  [neighbor setTitle:title forState:UIControlStateNormal];
  [neighbor.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.f]];
  [neighbor setBackgroundColor:Rgb2UIColor(153, 153, 153)];
  [neighbor setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [neighbor setFrame:CGRectMake(self.lastScrollChildrenX, 0, ([title length] * 10), 25)];
  [neighbor addTarget:self action:@selector(removeNeighborsForSearch:) forControlEvents:UIControlEventTouchUpInside];
  [neighbor setTag:(600 + self.neighsForSearchAds.count - 1)];
  neighbor.layer.cornerRadius = 5;
  neighbor.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
  neighbor.contentEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
  
  UIImageView *iconClose = [[UIImageView alloc] initWithFrame:CGRectMake(neighbor.frame.size.width - 15, 6, 13, 13.5)];
  [iconClose setImage:[UIImage imageNamed:@"icon-close-grey"]];
  [neighbor addSubview:iconClose];
  
  
  [self.scrollView addSubview:neighbor];
  
  self.lastScrollChildrenX += neighbor.frame.size.width + 5;
  
  [self changeScrollViewContentSize];
}

- (void)removeNeighborsForSearch:(id)sender{
  [sender removeFromSuperview];
  
  [self changeScrollViewContentSize];
  
  self.lastScrollChildrenX = 0;
  CGRect frame;
  for (UIView *v in [self.scrollView subviews]) {
    if ([v class] == [UIButton class]){
      frame = v.frame;
      frame.origin.x = self.lastScrollChildrenX;
      [v setFrame:frame];
      self.lastScrollChildrenX += frame.size.width + 5;
    }
  }
  
  
  NSInteger index = ([sender tag] - 600);
  if (self.neighsForSearchAds && index < [self.neighsForSearchAds count]){
      [self.neighsForSearchAds removeObjectAtIndex:index];
  }
  
}


- (void)changeScrollViewContentSize{
  CGFloat scrollContentWidth = 15;
  for (UIView *v in [self.scrollView subviews]) {
    if ([v class] == [UIButton class]){
      scrollContentWidth += v.frame.size.width;
    }
    
  }
  
  [self.scrollView setContentSize:CGSizeMake(scrollContentWidth,self.scrollView.frame.size.height)];
}

- (void)searchAdvertises{
  if ([self.neighsForSearchAds count] == 0){
    [Constants alertWithMessage:@"Selecione pelo menos um bairro"];
    return;
  }
  
  [[Neighborhood instance] setExtraNeighborhoodName:[self.neighsForSearchAds componentsJoinedByString:@","]];
  
  [(BaseViewController *)[[self nextResponder] nextResponder] searchAdvertisesFromPickerNeighborhood];
}

- (void)closeKeyboard{
  [self.boxSearch resignFirstResponder];
}
@end
