//
//  MenuView.h
//  GuiaJK
//
//  Created by pierreabreup on 7/19/15.
//  Copyright (c) 2015 Hand Mob. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MenuView : UIView <UITextViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) UIViewController *baseViewController;
- (void)doCall:(id)sender;

@end
