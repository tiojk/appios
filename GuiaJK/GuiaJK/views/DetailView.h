//
//  DetailView.h
//  GuiaJK
//
//  Created by pierreabreup on 8/2/15.
//  Copyright (c) 2015 Hand Mob. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhotoBrowser.h"
#define DETAIL_VIEW_TAG 500
#define BG_STARS_VIEW 700

//Users/pierreabreu/developer/android

@import Contacts;
@import ContactsUI;

@interface DetailView : UIView <UIWebViewDelegate, UITextViewDelegate, UITextFieldDelegate, UICollectionViewDataSource, UICollectionViewDelegate, MWPhotoBrowserDelegate, CNContactViewControllerDelegate>
  @property (assign,nonatomic) NSInteger advertiseIndex;
  @property (assign,nonatomic) BOOL isDetailFromID;


- (void)showPromotionCondition:(id)sender;
- (void)handleBackgroundModalTap:(UITapGestureRecognizer *)recognizer;
- (void)showContentModal:(UIView *)content;
- (void)changeStarsImage:(id)sender;
@end


