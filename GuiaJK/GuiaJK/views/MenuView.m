//
//  MenuView.m
//  GuiaJK
//
//  Created by pierreabreup on 7/19/15.
//  Copyright (c) 2015 Hand Mob. All rights reserved.
//

#import "MenuView.h"
#import "Constants.h"
#import "BaseViewController.h"
#import "AFNetworking.h"
//#import "AFHTTPRequestOperationManager.h"

@interface MenuView ()
@property UIView *aboutView;
@property UIScrollView *requestAdView;
@property UIButton *menuButton;
@property UIButton *aboutButton;
@property UIButton *requestAdvertiseButton;
@property UITextField *nameField;
@property UITextField *mailField;
@property UITextView *msgField;
@end

@implementation MenuView

@synthesize baseViewController;

- (void)drawRect:(CGRect)rect {
  CGFloat marginLeft = 20;
  CGFloat marginTop = 30;
  
  self.menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
  [self.menuButton setBackgroundImage:[UIImage imageNamed:@"icon-menu"] forState:UIControlStateNormal];
  [self.menuButton setFrame:CGRectMake(self.frame.size.width - 45, 10, 30, 23)];
  [self.menuButton addTarget:baseViewController action:@selector(HideMenu) forControlEvents:UIControlEventTouchUpInside];
  [self addSubview:self.menuButton];
  
  CGFloat lastTopElement = SumFrameYHeight(self.menuButton.frame) + marginTop;
  
  self.aboutButton = [UIButton buttonWithType:UIButtonTypeCustom];
  [self.aboutButton setBackgroundImage:[UIImage imageNamed:@"icon-about"] forState:UIControlStateNormal];
  [self.aboutButton setFrame:CGRectMake(marginLeft, lastTopElement, 144, 32)];
  [self.aboutButton addTarget:self action:@selector(showAbout) forControlEvents:UIControlEventTouchUpInside];
  [self addSubview:self.aboutButton];
  
  lastTopElement = SumFrameYHeight(self.aboutButton.frame) + marginTop;
  
  
  self.requestAdvertiseButton = [UIButton buttonWithType:UIButtonTypeCustom];
  [self.requestAdvertiseButton setBackgroundImage:[UIImage imageNamed:@"icon-request-advertise"] forState:UIControlStateNormal];
  [self.requestAdvertiseButton setFrame:CGRectMake(marginLeft, lastTopElement, 144, 32)];
  [self.requestAdvertiseButton addTarget:self action:@selector(requestAdvertise) forControlEvents:UIControlEventTouchUpInside];
  [self addSubview:self.requestAdvertiseButton];
  
  UILabel *publish1 = [[UILabel alloc] initWithFrame:CGRectMake(marginLeft+5, SumFrameYHeight(self.requestAdvertiseButton.frame) + marginTop, 205, 20)];
  [publish1 setNumberOfLines:1];
  [publish1 setBackgroundColor:[UIColor clearColor]];
  [publish1 setFont:[UIFont fontWithName:@"HelveticaNeue" size:19.f]];
  [publish1 setTextColor:[UIColor whiteColor]];
  [publish1 setTag:2000];
  [publish1 setText:@"Anuncie"];
  [self addSubview:publish1];
  
  marginTop = 15;
  
  UILabel *publish2 = [[UILabel alloc] initWithFrame:CGRectMake(publish1.frame.origin.x, SumFrameYHeight(publish1.frame) + marginTop, 220, 15)];
  [publish2 setBackgroundColor:[UIColor clearColor]];
  [publish2 setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.f]];
  [publish2 setTextColor:[UIColor whiteColor]];
  [publish2 setText:@"Taquara (Matriz)"];
  [publish2 setTag:2000];
  [self addSubview:publish2];
  
  UIButton *publish3 = [UIButton buttonWithType:UIButtonTypeCustom];
  [publish3 setBackgroundColor:[UIColor clearColor]];
  [publish3 setFrame:CGRectMake(publish1.frame.origin.x, SumFrameYHeight(publish2.frame) + marginTop, publish2.frame.size.width, publish2.frame.size.height)];
  [[publish3 titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.f]];
  [publish3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [publish3 setTitle:@"   (21) 3342-7650 ou (21) 3342-8525" forState:UIControlStateNormal];
  [publish3 setTag:700];
  [publish3 addTarget:self action:@selector(doCall:) forControlEvents:UIControlEventTouchUpInside];
  [self addSubview:publish3];
  
  UILabel *publish4 = [[UILabel alloc] initWithFrame:CGRectMake(publish2.frame.origin.x, SumFrameYHeight(publish3.frame) + marginTop, publish2.frame.size.width, publish2.frame.size.height)];
  [publish4 setBackgroundColor:[UIColor clearColor]];
  [publish4 setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.f]];
  [publish4 setTextColor:[UIColor whiteColor]];
  [publish4 setText:@"Campo Grande"];
  [publish4 setTag:2000];
  [self addSubview:publish4];
  
  UIButton *publish5 = [UIButton buttonWithType:UIButtonTypeCustom];
  [publish5 setBackgroundColor:[UIColor clearColor]];
  [publish5 setFrame:CGRectMake(publish2.frame.origin.x, SumFrameYHeight(publish4.frame) + marginTop, 120, publish2.frame.size.height)];
  [[publish5 titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.f]];
  [[publish5 titleLabel] setTextAlignment:NSTextAlignmentLeft];
  [publish5 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [publish5 setTitle:@"(21) 3738-7138" forState:UIControlStateNormal];
  [publish5 setTag:701];
  [publish5 addTarget:self action:@selector(doCall:) forControlEvents:UIControlEventTouchUpInside];
  [self addSubview:publish5];
  
  UILabel *publish6 = [[UILabel alloc] initWithFrame:CGRectMake(publish2.frame.origin.x, SumFrameYHeight(publish5.frame) + marginTop, publish2.frame.size.width, publish2.frame.size.height)];
  [publish6 setBackgroundColor:[UIColor clearColor]];
  [publish6 setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.f]];
  [publish6 setTextColor:[UIColor whiteColor]];
  [publish6 setText:@"Vila Isabel"];
  [publish6 setTag:2000];
  [self addSubview:publish6];
  
  UIButton *publish7 = [UIButton buttonWithType:UIButtonTypeCustom];
  [publish7 setBackgroundColor:[UIColor clearColor]];
  [publish7 setFrame:CGRectMake(publish2.frame.origin.x, SumFrameYHeight(publish6.frame) + marginTop, publish5.frame.size.width, publish2.frame.size.height)];
  [[publish7 titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.f]];
  [publish7 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [publish7 setTitle:@"(21) 2578-4592" forState:UIControlStateNormal];
  [publish7 setTag:702];
  [publish7 addTarget:self action:@selector(doCall:) forControlEvents:UIControlEventTouchUpInside];
  [self addSubview:publish7];
}

- (void)doCall:(id)sender{
  NSInteger index = [sender tag];
  NSString *toCall;
  switch (index) {
    case 700:
      toCall = @"+552133427650";
      break;
    case 800:
      toCall = @"+552133427650";
      break;
    case 701:
      toCall = @"+552137387138";
      break;
    case 801:
      toCall = @"+552137387138";
      break;
    case 702:
      toCall = @"+552125784592";
      break;
    case 802:
      toCall = @"+552125784592";
      break;
    default:
      break;
  }

  NSString *phoneNumber = [@"tel://" stringByAppendingString:toCall];
  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}


- (void)showAbout{
  CGRect aboutViewFrame = self.frame;
  aboutViewFrame.origin.x = self.frame.size.width;
  
  self.aboutView = [[UIView alloc] initWithFrame:aboutViewFrame];
  [self.aboutView setUserInteractionEnabled:YES];
  
  UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
  [closeButton setBackgroundImage:[UIImage imageNamed:@"icon-close-white"] forState:UIControlStateNormal];
  [closeButton setFrame:CGRectMake(self.frame.size.width - 50, 10, 30, 30)];
  [closeButton addTarget:self action:@selector(closeAbout) forControlEvents:UIControlEventTouchUpInside];
  [self.aboutView addSubview:closeButton];
  
  
  NSString *textViewA = @"  Fundada no fim de 1999, O JK Revista de Bairros foi pioneira em Jacarepaguá a inserir o serviço de Guia do Comércio local. \ Apresentada em um formato inovador com 10cm x 28cm possibilitando que a revista seja entregue diretamente nas caixas de correios de prédios e  \ residências. Inicialmente, a revista abrangeria somente a área da Taquara, mas com o grande sucesso se viu necessário a expansão para novas regiões.  \ Abrangendo atualmente, as áreas da Freguesia, Tanque, Pechincha, Anil, Gardênia Azul, Praça Seca, Vila Valqueire, Realengo, Sulacap, Curicica, Rio  \ Centro, Vargem Pequena, Vargem Grande, Barra da Tijuca, Grajaú, Andaraí, Vila Isabel, Tijuca, Recreio dos Bandeirantes, Campo Grande, Taquara, Boiuna, \  Madureira, Rio das Pedras, Muzema, Tijuquinha e Itanhangá.";
  
  NSString *textViewB = @"  Há 16 anos no mercado, estamos sempre fazendo melhorias, como aumento de tiragens e expansão das nossas rotas de entrega. Essa \ necessidade de fazer sempre o melhor para nosso cliente garantiu que fossemos, em Jacarepaguá, a revista que proporciona o melhor retorno aos nossos  \ anunciantes.";
  
  UITextView *aboutTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, SumFrameYHeight(closeButton.frame),
                                                                           self.frame.size.width - 20,
                                                                           self.aboutView.frame.size.height - SumFrameYHeight(closeButton.frame) - 110)];
  [aboutTextView setBackgroundColor:[UIColor clearColor]];
  [aboutTextView setFont:[UIFont systemFontOfSize:13]];
  [aboutTextView setScrollEnabled:YES];
  [aboutTextView setUserInteractionEnabled:YES];
  [aboutTextView setTextColor:[UIColor whiteColor]];
  aboutTextView.textContainerInset = UIEdgeInsetsZero;
  aboutTextView.textContainer.lineFragmentPadding = 0;
  [aboutTextView setText:[NSString stringWithFormat:@"%@\r\n\n%@",textViewA,textViewB]];
  
  [self.aboutView addSubview:aboutTextView];
  [self addSubview:self.aboutView];
  
  [self.menuButton setHidden:YES];
  [self.aboutButton setHidden:YES];
  [self.requestAdvertiseButton setHidden:YES];
  
  for (UIView *v in [self subviews]) {
    if ([v tag] == 2000 || ([v tag] >= 700 && [v tag] < 703)){
      [v setHidden:YES];
    }
  }
  
  aboutViewFrame.origin.x = 10;
  [UIView animateWithDuration:0.2
                   animations:^{[self.aboutView setFrame:aboutViewFrame];
                   }
   
                   completion:nil];
  
}

- (void)closeAbout{
  CGRect aboutViewFrame = self.aboutView.frame;
  aboutViewFrame.origin.x = self.frame.size.width;
  
  [UIView animateWithDuration:0.2
                   animations:^{[self.aboutView setFrame:aboutViewFrame];
                   }
   
                   completion:nil];
  
  
  [self performSelector:@selector(resetAboutView) withObject:self afterDelay:1];
  
  [self.menuButton setHidden:NO];
  [self.aboutButton setHidden:NO];
  [self.requestAdvertiseButton setHidden:NO];
  
  for (UIView *v in [self subviews]) {
    if ([v tag] == 2000 || ([v tag] >= 700 && [v tag] < 703)){
      [v setHidden:NO];
    }
  }
}

- (void)resetAboutView{
  [self.aboutView removeFromSuperview];
  self.aboutView = nil;
  
}

- (void)requestAdvertise{
  CGRect requestAdViewFrame = self.frame;
  requestAdViewFrame.origin.x = self.frame.size.width;
  
  CGFloat marginTop = 15;
  CGFloat marginLeft = 0;
  
  self.requestAdView = [[UIScrollView alloc] initWithFrame:requestAdViewFrame];
  [self.requestAdView setHidden:NO];
  [self.requestAdView setScrollEnabled:YES];
  [self.requestAdView setUserInteractionEnabled:YES];
  [self.requestAdView setContentSize:CGSizeMake(self.requestAdView.frame.size.width, self.requestAdView.frame.size.height + 70)];
  
  UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
  [closeButton setBackgroundImage:[UIImage imageNamed:@"icon-close-white"] forState:UIControlStateNormal];
  [closeButton setFrame:CGRectMake(self.frame.size.width - 50, 0, 30, 30)];
  [closeButton addTarget:self action:@selector(closeRequestAd) forControlEvents:UIControlEventTouchUpInside];
  [self.requestAdView addSubview:closeButton];
  
  UILabel *publish1 = [[UILabel alloc] initWithFrame:CGRectMake(marginLeft+5, SumFrameYHeight(closeButton.frame) + marginTop, 215, 20)];
  [publish1 setNumberOfLines:1];
  [publish1 setBackgroundColor:[UIColor clearColor]];
  [publish1 setFont:[UIFont fontWithName:@"HelveticaNeue" size:15.f]];
  [publish1 setTextColor:[UIColor whiteColor]];
  [publish1 setText:@"PARA ANUNCIAR LIGUE"];
  [publish1 setTag:3000];
  [self.requestAdView addSubview:publish1];
  
  UILabel *publish2 = [[UILabel alloc] initWithFrame:CGRectMake(publish1.frame.origin.x, SumFrameYHeight(publish1.frame) + marginTop, 220, 15)];
  [publish2 setBackgroundColor:[UIColor clearColor]];
  [publish2 setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.f]];
  [publish2 setTextColor:[UIColor whiteColor]];
  [publish2 setText:@"Taquara (Matriz)"];
  [publish2 setTag:3000];
  [self.requestAdView addSubview:publish2];
  
  UIButton *publish3 = [UIButton buttonWithType:UIButtonTypeCustom];
  [publish3 setBackgroundColor:[UIColor clearColor]];
  [publish3 setFrame:CGRectMake(publish1.frame.origin.x, SumFrameYHeight(publish2.frame) + marginTop, publish2.frame.size.width, publish2.frame.size.height)];
  [[publish3 titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.f]];
  [publish3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [publish3 setTitle:@"   (21) 3342-7650 ou (21) 3342-8525" forState:UIControlStateNormal];
  [publish3 setTag:800];
  [publish3 addTarget:self action:@selector(doCall:) forControlEvents:UIControlEventTouchUpInside];
  [self.requestAdView addSubview:publish3];
  
  UILabel *publish4 = [[UILabel alloc] initWithFrame:CGRectMake(publish2.frame.origin.x, SumFrameYHeight(publish3.frame) + marginTop, publish2.frame.size.width, publish2.frame.size.height)];
  [publish4 setBackgroundColor:[UIColor clearColor]];
  [publish4 setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.f]];
  [publish4 setTextColor:[UIColor whiteColor]];
  [publish4 setText:@"Campo Grande"];
  [publish4 setTag:3000];
  [self.requestAdView addSubview:publish4];
  
  UIButton *publish5 = [UIButton buttonWithType:UIButtonTypeCustom];
  [publish5 setBackgroundColor:[UIColor clearColor]];
  [publish5 setFrame:CGRectMake(publish2.frame.origin.x, SumFrameYHeight(publish4.frame) + marginTop, 120, publish2.frame.size.height)];
  [[publish5 titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.f]];
  [[publish5 titleLabel] setTextAlignment:NSTextAlignmentLeft];
  [publish5 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [publish5 setTitle:@"(21) 3738-7138" forState:UIControlStateNormal];
  [publish5 setTag:801];
  [publish5 addTarget:self action:@selector(doCall:) forControlEvents:UIControlEventTouchUpInside];
  [self.requestAdView addSubview:publish5];
  
  UILabel *publish6 = [[UILabel alloc] initWithFrame:CGRectMake(publish2.frame.origin.x, SumFrameYHeight(publish5.frame) + marginTop, publish2.frame.size.width, publish2.frame.size.height)];
  [publish6 setBackgroundColor:[UIColor clearColor]];
  [publish6 setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.f]];
  [publish6 setTextColor:[UIColor whiteColor]];
  [publish6 setText:@"Vila Isabel"];
  [publish6 setTag:3000];
  [self.requestAdView addSubview:publish6];
  
  UIButton *publish7 = [UIButton buttonWithType:UIButtonTypeCustom];
  [publish7 setBackgroundColor:[UIColor clearColor]];
  [publish7 setFrame:CGRectMake(publish2.frame.origin.x, SumFrameYHeight(publish6.frame) + marginTop, publish5.frame.size.width, publish2.frame.size.height)];
  [[publish7 titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.f]];
  [publish7 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [publish7 setTitle:@"(21) 2578-4592" forState:UIControlStateNormal];
  [publish7 setTag:802];
  [publish7 addTarget:self action:@selector(doCall:) forControlEvents:UIControlEventTouchUpInside];
  [self.requestAdView addSubview:publish7];
  
  
  UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(0, SumFrameYHeight(publish7.frame) + marginTop, 205, 60)];
  [header setNumberOfLines:2];
  [header setBackgroundColor:[UIColor clearColor]];
  [header setFont:[UIFont fontWithName:@"HelveticaNeue" size:15.f]];
  [header setTextColor:[UIColor whiteColor]];
  [header setText:@"ou preencha seus dados que entraremos em contato."];
  [self.requestAdView addSubview:header];
  
  CGRect fieldFrame = CGRectMake(0, SumFrameYHeight(header.frame) + marginTop, 200, 30);
  
  self.nameField = [[UITextField alloc] initWithFrame:fieldFrame];
  [self.nameField setFont:[UIFont fontWithName:@"HelveticaNeue" size:14.f]];
  [self.nameField setReturnKeyType:UIReturnKeyDone];
  [self.nameField setDelegate:self];
  self.nameField.borderStyle = UITextBorderStyleRoundedRect;
  self.nameField.placeholder = @"Nome";
  [self.requestAdView addSubview:self.nameField];
  
  fieldFrame.origin.y = SumFrameYHeight(self.nameField.frame) + marginTop;
  
  self.mailField = [[UITextField alloc] initWithFrame:fieldFrame];
  [self.mailField setFont:[UIFont fontWithName:@"HelveticaNeue" size:14.f]];
  [self.mailField setKeyboardType:UIKeyboardTypeEmailAddress];
  [self.mailField setReturnKeyType:UIReturnKeyDone];
  [self.mailField setDelegate:self];
  self.mailField.borderStyle = UITextBorderStyleRoundedRect;
  self.mailField.placeholder = @"E-mail";
  [self.requestAdView addSubview:self.mailField];
  
  fieldFrame.origin.y = SumFrameYHeight(self.mailField.frame) + marginTop;
  fieldFrame.size.height = 80;
  
  self.msgField = [[UITextView alloc] initWithFrame:fieldFrame];
  [self.msgField setBackgroundColor:[UIColor whiteColor]];
  [self.msgField setFont:[UIFont fontWithName:@"HelveticaNeue" size:14.f]];
  [self.msgField setEditable:YES];
  [self.msgField setText:@"Mensagem"];
  [self.msgField setReturnKeyType:UIReturnKeyDone];
  [self.msgField setTextColor:Rgb2UIColor(207, 207, 213)];
  [self.msgField setDelegate:self];
  
  self.msgField.layer.cornerRadius = 5;
  self.msgField.layer.borderWidth = 1;
  self.msgField.layer.borderColor = [UIColor whiteColor].CGColor;
  [self.requestAdView addSubview:self.msgField];
  
  
  UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeSystem];
  [sendButton setTitle:@"Enviar" forState:UIControlStateNormal];
  [sendButton setTitleColor:[UIColor colorWithWhite:1 alpha:1] forState:UIControlStateNormal];
  [sendButton setBackgroundColor:Rgb2UIColor(27, 116, 183)];
  [sendButton setFrame:CGRectMake(0, SumFrameYHeight(self.msgField.frame) + marginTop, 100, 30)];
  [sendButton addTarget:self action:@selector(postRequestAdvertiseForm) forControlEvents:UIControlEventTouchUpInside];
  sendButton.layer.cornerRadius = 5;
  sendButton.layer.borderWidth = 1;
  sendButton.layer.borderColor = [UIColor whiteColor].CGColor;
  sendButton.clipsToBounds = YES;
  
  [self.requestAdView addSubview:sendButton];
  
  
  
  
  [self addSubview:self.requestAdView];
  
  
  [self.menuButton setHidden:YES];
  [self.aboutButton setHidden:YES];
  [self.requestAdvertiseButton setHidden:YES];
  
  for (UIView *v in [self subviews]) {
    if ([v tag] == 2000 || ([v tag] >= 700 && [v tag] < 703)){
      [v setHidden:YES];
    }
  }
  
  requestAdViewFrame.origin.x = 10;
  [UIView animateWithDuration:0.2
                   animations:^{[self.requestAdView setFrame:requestAdViewFrame];
                   }
   
                   completion:nil];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
  [textField resignFirstResponder];
  
  CGSize content = self.requestAdView.contentSize;
  CGFloat delta = 230;
  content.height = (content.height - delta);
  [self.requestAdView setContentSize:content];
  [self.requestAdView setContentOffset:CGPointMake(0,0) animated:YES];
  
  return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
  CGSize content = self.requestAdView.contentSize;
  CGFloat delta = 230;
  content.height = (content.height + delta);
  [self.requestAdView setContentSize:content];
  [self.requestAdView setContentOffset:CGPointMake(0,delta+50) animated:YES];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
  if ([textView.text isEqualToString:@"Mensagem"]) {
    textView.text = @"";
    textView.textColor = [UIColor blackColor]; //optional
  }
  
  CGSize content = self.requestAdView.contentSize;
  CGFloat delta = 230;
  content.height = (content.height + delta);
  [self.requestAdView setContentSize:content];
  [self.requestAdView setContentOffset:CGPointMake(0,delta+50) animated:YES];
  
  [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
  if ([textView.text isEqualToString:@""]) {
    textView.text = @"Mensagem";
    textView.textColor = [UIColor lightGrayColor]; //optional
  }
  [textView resignFirstResponder];
  
  CGSize content = self.requestAdView.contentSize;
  CGFloat delta = 230;
  content.height = (content.height - delta);
  [self.requestAdView setContentSize:content];
  [self.requestAdView setContentOffset:CGPointMake(0,0) animated:YES];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
  
  if([text isEqualToString:@"\n"]) {
    [textView resignFirstResponder];
    CGSize content = self.requestAdView.contentSize;
    CGFloat delta = 230;
    content.height = (content.height - delta);
    [self.requestAdView setContentSize:content];
    [self.requestAdView setContentOffset:CGPointMake(0,0) animated:YES];
    return NO;
  }
  
  return YES;
}

- (void)postRequestAdvertiseForm{
  [Constants showIndicatorAtView:self];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  NSDictionary *parameters = @{@"contact[name]":self.nameField.text,
                               @"contact[email]":self.mailField.text,
                               @"contact[message]":self.msgField.text};
  
  [manager POST:URL_SEND_MAIL parameters:parameters success:^(NSURLSessionTask *operation, id responseObject) {
    [Constants hideIndicator];
    [Constants alertWithMessage:@"Mensagem enviada com sucesso!"];
    [self closeRequestAd];
    
  } failure:^(NSURLSessionTask *operation, NSError *error) {
    [Constants hideIndicator];
    [Constants alertWithMessage:@"Ocorreu um problema ao tentar enviar a mensagem. Tente mais tarde."];
    NSLog(@"postRequestAdvertiseForm Error: %@", error);
  }];
}

- (void)closeRequestAd{
  CGRect requestAdViewFrame = self.requestAdView.frame;
  requestAdViewFrame.origin.x = self.frame.size.width;
  
  [UIView animateWithDuration:0.2
                   animations:^{[self.requestAdView setFrame:requestAdViewFrame];
                   }
   
                   completion:nil];
  
  
  [self performSelector:@selector(resetRequestAdView) withObject:self afterDelay:1];
  
  [self.menuButton setHidden:NO];
  [self.aboutButton setHidden:NO];
  [self.requestAdvertiseButton setHidden:NO];
  
  for (UIView *v in [self subviews]) {
    if ([v tag] == 2000 || ([v tag] >= 700 && [v tag] < 703)){
      [v setHidden:NO];
    }
  }
}

- (void)resetRequestAdView{
  [self.requestAdView removeFromSuperview];
  self.requestAdView = nil;
  
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
  [Constants hideIndicator];
}



@end
