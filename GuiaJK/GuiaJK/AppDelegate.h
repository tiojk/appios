//
//  AppDelegate.h
//  GuiaJK
//
//  Created by pierreabreup on 7/12/15.
//  Copyright (c) 2015 Hand Mob. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigation;


@end

