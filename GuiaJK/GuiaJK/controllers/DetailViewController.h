//
//  DetailViewController.h
//  GuiaJK
//
//  Created by pierreabreup on 5/22/16.
//  Copyright © 2016 Hand Mob. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface DetailViewController : BaseViewController
  @property (assign,nonatomic) NSInteger advertiseIndex;
  @property (assign,nonatomic) NSInteger advertiseID;
@end
