//
//  SearchResultViewController.h
//  GuiaJK
//
//  Created by pierreabreup on 8/2/15.
//  Copyright (c) 2015 Hand Mob. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "BaseViewController.h"
#import "SearchResultView.h"

#define DETAIL_ON_MAP_VIEW 301

@import GoogleMaps;

@interface SearchResultViewController : BaseViewController <CLLocationManagerDelegate, GMSMapViewDelegate>
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong,nonatomic) NSString *byCategory;
@property (strong,nonatomic) NSString *byText;
@property (assign,nonatomic) BOOL byHasPromotion;
@property (assign,nonatomic) BOOL enableSearchLocation;
@property (assign,nonatomic) BOOL enableShowMap;
@property (assign,nonatomic) BOOL shouldResetFilters;
@property (strong,nonatomic) NSDictionary *selectedCategoryArea;
@property (strong,nonatomic) NSString *byCurrentLocation;

- (void)searchAdvertises;
- (void)showResultAsMap;
- (void)showCategoriesList;
- (void)showIndicator;
- (SearchResultView *)contentView;
@end
