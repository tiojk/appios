//
//  LocationViewController.m
//  GuiaJK
//
//  Created by pierreabreup on 7/25/15.
//  Copyright (c) 2015 Hand Mob. All rights reserved.
//

#import "LocationViewController.h"
#import "Constants.h"
#import "AFNetworking.h"
//#import "AFHTTPSessionManager.h"
#import "HomeViewController.h"
#import "Neighborhood.h"

@interface LocationViewController ()
  @property UIButton *okButton;
  @property UIActivityIndicatorView *loading;
  @property BOOL beganSearch;
@property UIImageView *searchPositionImage;
@end

@implementation LocationViewController

- (void)viewDidLoad {
  self.beganSearch = NO;
  [super viewDidLoad];
  
  UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Default-568h@2x.png"]];
  [background setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
  [self.view addSubview:background];
  
  self.searchPositionImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search-position"]];
  [self.searchPositionImage setFrame:CGRectMake(69.5, self.view.frame.size.height / 2, 178, 107)];
  [self.view addSubview:self.searchPositionImage];
  
  self.locationManager = [[CLLocationManager alloc] init];
  [self.locationManager setDelegate:self];
  self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
  self.locationManager.distanceFilter = 500;
  
  [self.locationManager startUpdatingLocation];
  if(IS_OS_8_OR_LATER) {
    [self.locationManager requestWhenInUseAuthorization];
  }
  
  self.loading = [[UIActivityIndicatorView alloc]
             initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge] ;
  [self.loading setFrame:CGRectMake((self.view.frame.size.width - 150) / 2, (self.view.frame.size.height - 120) / 2, 150, 120)];
  [self.view addSubview:self.loading];
  [self.loading startAnimating];

}

- (void)viewWillAppear:(BOOL)animated{
  [self.navigationController.navigationBar setHidden:YES];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
  [manager stopUpdatingLocation];
  [self searchAdvertisesByLatLong:[NSString stringWithFormat:@"%f,%f",newLocation.coordinate.latitude,newLocation.coordinate.longitude]];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
  CLLocation* location = [locations lastObject];
  if (!self.beganSearch){
    self.beganSearch = YES;
    [manager stopUpdatingLocation];
    [self searchAdvertisesByLatLong:[NSString stringWithFormat:@"%f,%f",location.coordinate.latitude,location.coordinate.longitude]];
  }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
  [manager stopUpdatingLocation];
  [self searchAdvertisesByLatLong:@"0,0"];
}

- (void)loadAllNeighborhoods{
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  
  [manager GET:URL_ALL_NEIGHBORHOODS parameters:[NSDictionary dictionary] success:^(NSURLSessionTask *operation, id responseObject) {
    NSDictionary *jsonData = [NSDictionary dictionaryWithDictionary:responseObject];
    [[Neighborhood instance] setNeighborhoodsList:(NSArray *)[jsonData objectForKey:@"neighborhoods"]];
    
    [self hideIndicator];
    
    NSString *message = @"Não consegui descobrir onde você está :(, mas não se preocupe, basta selecionar um dos nossos bairros para ver nossos anúncios";
    [self showNeighborhoodPicker: message];
  } failure:^(NSURLSessionTask *operation, NSError *error) {
    [self hideIndicator];
    [Constants alertRequestError];
  }];
}

- (void)searchAdvertisesByLatLong:(NSString *)latlong{
  NSDictionary *parameters = @{@"l": latlong};
  //NSDictionary *parameters = @{@"l": @"-23.0135862,-43.3103468"};
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  
  [manager GET:URL_CHECK_FOR_NEIGHBORHOOD parameters:parameters success:^(NSURLSessionTask *operation, id responseObject) {
    NSDictionary *jsonData = [NSDictionary dictionaryWithDictionary:responseObject];
    [[Neighborhood instance] setNeighborhoodsList:(NSArray *)[jsonData objectForKey:@"neighborhoods"]];
    [[Neighborhood instance] setCurrentNeighborhoodName:(NSString *)[jsonData objectForKey:@"current_neighborhood"]];
    
    [self hideIndicator];
    
    if ([[jsonData objectForKey:@"advertises_count"] integerValue] == 0){
      NSString *message = [NSString stringWithFormat:@"Não existem anúncios disponíveis para [ %@ ]. Por favor, selecione um dos nossos bairros.",[[Neighborhood instance] currentNeighborhoodName]];
      [self showNeighborhoodPicker:message];
    }
    else{
      [self showHome];
    }
  } failure:^(NSURLSessionTask *operation, NSError *error) {
    [self loadAllNeighborhoods];
  }];
}

- (void) hideIndicator{
  [self.loading stopAnimating];
  [self.loading removeFromSuperview];
  self.loading = nil;
}


- (void)showNeighborhoodPicker:(NSString *)message{
  [self.searchPositionImage setHidden:YES];
  
  [Constants alertWithMessage:message];
  
  UIPickerView *pickerNeighborhood = [[UIPickerView alloc] initWithFrame:CGRectMake(0,
                                                                           (self.view.frame.size.height/2) - 40,
                                                                           self.view.frame.size.width,
                                                                           210)];
  [pickerNeighborhood setBackgroundColor:[UIColor whiteColor]];
  [pickerNeighborhood setShowsSelectionIndicator:YES];
  [pickerNeighborhood setDelegate:self];
  [pickerNeighborhood setDataSource:self];
  
  [self.view addSubview:pickerNeighborhood];
  
  [[Neighborhood instance] setCurrentNeighborhoodName:[[[Neighborhood instance] neighborhoodsList] objectAtIndex:0]];
  NSString *buttonTitle = [NSString stringWithFormat:@"Ir para o bairro %@",[[Neighborhood instance] currentNeighborhoodName]];
  
  CGFloat okButtonHeight = 30;
  CGFloat okButtonMarginLeft = 10;
  self.okButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
  [self.okButton setFrame:CGRectMake(okButtonMarginLeft,
                                SumFrameYHeight(pickerNeighborhood.frame) + 10,
                                self.view.frame.size.width - okButtonMarginLeft * 2,
                                okButtonHeight)];
  [self.okButton setTitle:buttonTitle forState:UIControlStateNormal];
  [self.okButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  [self.okButton setBackgroundColor:Rgb2UIColor(184, 75, 76)];
  [self.okButton addTarget:self action:@selector(showHome) forControlEvents:UIControlEventTouchUpInside];
  [self.okButton.titleLabel setFont:[UIFont systemFontOfSize:13]];
  
  self.okButton.layer.cornerRadius = 5;
  self.okButton.layer.borderWidth = 1;
  self.okButton.layer.borderColor = Rgb2UIColor(12, 103, 171).CGColor;
  
  [self.view addSubview:self.okButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
  return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
  return [[[Neighborhood instance] neighborhoodsList] count];
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
  return [[[Neighborhood instance] neighborhoodsList] objectAtIndex:row];
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
  [[Neighborhood instance] setCurrentNeighborhoodName:[[[Neighborhood instance] neighborhoodsList] objectAtIndex:row]];
  
  NSString *buttonTitle = [NSString stringWithFormat:@"Ir para o bairro %@",[[Neighborhood instance] currentNeighborhoodName]];
  
  [self.okButton setTitle:buttonTitle forState:UIControlStateNormal];
}

- (void)showHome{
  [[Neighborhood instance] setExtraNeighborhoodName:@""];
  
  HomeViewController *homeVC = [HomeViewController new];
  [homeVC setShouldLoadContent:YES];
  [self.navigationController.navigationBar setHidden:NO];
  [self.navigationController pushViewController:homeVC animated:YES];
}

- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
  return UIInterfaceOrientationMaskPortrait;
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
  return NO;
}

- (BOOL) shouldAutorotate
{
  return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
  return UIInterfaceOrientationPortrait;
}

@end
