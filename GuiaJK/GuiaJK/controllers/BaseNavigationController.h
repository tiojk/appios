//
//  BaseNavigationController.h
//  GuiaJK
//
//  Created by pierreabreup on 6/18/16.
//  Copyright © 2016 Hand Mob. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavigationController : UINavigationController

@end
