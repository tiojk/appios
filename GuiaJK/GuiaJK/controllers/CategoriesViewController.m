//
//  CategoriesViewController.m
//  GuiaJK
//
//  Created by pierreabreup on 6/25/16.
//  Copyright © 2016 Hand Mob. All rights reserved.
//

#import "CategoriesViewController.h"
#import "Neighborhood.h"
#import "UIImageView+WebCache.h"
#import "SearchResultViewController.h"

@interface CategoriesViewController ()
  @property UIView *categoriesVIew;
  @property NSMutableIndexSet *expandedSections;
@end

@implementation CategoriesViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  [self showBackButton];
  
  CGFloat detailY = self.baseOriginTop - self.neighborhoodButton.frame.size.height;
  self.categoriesVIew = [[UIView alloc] initWithFrame:CGRectMake(0, detailY,
                                                                 self.view.frame.size.width,
                                                                 self.view.frame.size.height)];
  
  [self.categoriesVIew setUserInteractionEnabled:YES];
  [self.categoriesVIew setBackgroundColor:Rgb2UIColor(245, 245, 245)];
  [self.view addSubview:self.categoriesVIew];
  
  if ([[Neighborhood instance] advertisesCategories] && [[[Neighborhood instance] advertisesCategories] count] > 0){
    [self addElementsToView];
  }
  else{
    [Constants showIndicatorAtView:self.view];
    [[Neighborhood instance] loadCategoriesFromCurrentLatLngSuccess:^(AFHTTPRequestSerializer *operation, id response){
      
      if ([response isEqual:[NSNull null]]){
        [Constants hideIndicator];
        [Constants alertTechnicalProblems];
        [self.navigationController popViewControllerAnimated:YES];
      }else{
        [self addElementsToView];
        [Constants hideIndicator];
      }
      
    }failure:^(AFHTTPRequestSerializer *operation, NSError *error) {
      [Constants hideIndicator];
      [Constants alertTechnicalProblems];
      [self.navigationController popViewControllerAnimated:YES];
    }];
  }
}

- (void)addElementsToView{
  UIImageView *selectCategory = [[UIImageView alloc]  initWithFrame:CGRectMake(10, 10, 282, 30)];
  [selectCategory setImage:[UIImage imageNamed:@"title-categories-big"]];
  [self.categoriesVIew addSubview:selectCategory];
  
  if (!self.expandedSections){
    self.expandedSections = [[NSMutableIndexSet alloc] init];
  }
  
  CGFloat tableViewY = SumFrameYHeight(selectCategory.frame)+10;
  UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(
                                                                         selectCategory.frame.origin.x,
                                                                         tableViewY,
                                                                         selectCategory.frame.size.width,
                                                                         self.view.frame.size.height - tableViewY - 70)];
  [tableView setScrollEnabled:YES];
  [tableView setBackgroundColor:[UIColor clearColor]];
  tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
  tableView.delegate = self;
  tableView.dataSource = self;
  tableView.sectionHeaderHeight = 0.0;
  tableView.sectionFooterHeight = 0.0;
  
  [self.categoriesVIew addSubview:tableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
  return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return [[[Neighborhood instance] advertisesCategories] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  if ([self tableView:tableView canCollapseSection:section])
  {
    if ([self.expandedSections containsIndex:section])
    {
      NSDictionary *category = (NSDictionary *)[[[Neighborhood instance] advertisesCategories] objectAtIndex:section];
      return [(NSArray *)[category objectForKey:@"children"] count] + 1; // return rows when expanded
    }
    
    return 1; // only top row showing
  }
  
  // Return the number of rows in the section.
  return 1;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
  return 30;
}


- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
  if (section == 0)
    return CGFLOAT_MIN;
  
  return tableView.sectionHeaderHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *CellIdentifier = @"CategoryCell";
  
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
  }
  
  if ([cell viewWithTag:CELL_ICON]){
    [[cell viewWithTag:CELL_ICON] removeFromSuperview];
  }
  
  
  if (!indexPath.row)
  {
    NSDictionary *category = (NSDictionary *)[[[Neighborhood instance] advertisesCategories] objectAtIndex:indexPath.section];
    cell.textLabel.text = [NSString stringWithFormat:@"      %@",(NSString *)[category objectForKey:@"name"]];
    
    NSString *iconURLString = (NSString *)[category objectForKey:@"icon"];
    if (iconURLString && ![iconURLString isEqual:[NSNull null]]){
      UIImageView *imv = [[UIImageView alloc]initWithFrame:CGRectMake(5,5, 33.5, 33.5)];
      [imv setTag:CELL_ICON];
      [imv sd_setImageWithURL:[NSURL URLWithString:iconURLString] placeholderImage:[UIImage imageNamed:@"blank"]];
      [cell addSubview:imv];
    }
    
    [cell setBackgroundColor:[Constants colorFromHexString:[category objectForKey:@"bgcolor"]]];
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-BlkIt" size:15.f]];
    
    if ([self.expandedSections containsIndex:indexPath.section])
    {
      
      UIImageView *imView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-minus"]];
      cell.accessoryView = imView;
    }
    else
    {
      
      UIImageView *imView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-plus"]];
      cell.accessoryView = imView;
    }
  }
  else
  {
    NSDictionary *category = (NSDictionary *)[[[Neighborhood instance] advertisesCategories] objectAtIndex:indexPath.section];
    NSArray *children = (NSArray *)[category objectForKey:@"children"];
    cell.textLabel.text = [children objectAtIndex:(indexPath.row - 1)];
    [cell.textLabel setTextColor:Rgb2UIColor(117, 117, 117)];
    cell.accessoryView = nil;
    cell.accessoryType = UITableViewCellAccessoryNone;
    [cell setBackgroundColor:[UIColor whiteColor]];
    [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-It" size:15.f]];
  }
  
  return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  if ([self tableView:tableView canCollapseSection:indexPath.section])
  {
    if (!indexPath.row)
    {
      // only first row toggles exapand/collapse
      [tableView deselectRowAtIndexPath:indexPath animated:YES];
      
      NSInteger section = indexPath.section;
      BOOL currentlyExpanded = [self.expandedSections containsIndex:section];
      NSInteger rows;
      
      
      NSMutableArray *tmpArray = [NSMutableArray array];
      if (currentlyExpanded)
      {
        rows = [self tableView:tableView numberOfRowsInSection:section];
        [self.expandedSections removeIndex:section];
      }
      else
      {
        [self.expandedSections addIndex:section];
        rows = [self tableView:tableView numberOfRowsInSection:section];
      }
      
      for (int i=1; i<rows; i++)
      {
        NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i
                                                       inSection:section];
        [tmpArray addObject:tmpIndexPath];
      }
      
      UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
      
      if (currentlyExpanded)
      {
        [tableView deleteRowsAtIndexPaths:tmpArray
                         withRowAnimation:UITableViewRowAnimationTop];
        
        UIImageView *imView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-plus"]];
        cell.accessoryView = imView;
      }
      else
      {
        [tableView insertRowsAtIndexPaths:tmpArray
                         withRowAnimation:UITableViewRowAnimationTop];
        
        UIImageView *imView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-minus"]];
        cell.accessoryView = imView;
      }
    }
    else{
      NSDictionary *category = (NSDictionary *)[[[Neighborhood instance] advertisesCategories] objectAtIndex:indexPath.section];
      NSArray *children = (NSArray *)[category objectForKey:@"children"];
      
      SearchResultViewController *searchResult = (SearchResultViewController *)[self.navigationController.viewControllers objectAtIndex:2];
      [searchResult setByCategory:[children objectAtIndex:(indexPath.row - 1)]];
      [searchResult setSelectedCategoryArea:@{
                                              @"title":(NSString *)[category objectForKey:@"name"],
                                              @"icon":(NSString *)[category objectForKey:@"icon"],
                                              @"bgcolor":(NSString *)[category objectForKey:@"bgcolor"],}];
      
      [searchResult showIndicator];
      [searchResult searchAdvertises];
      
      [self.navigationController popViewControllerAnimated:YES];
    }
  }
  
}
@end
