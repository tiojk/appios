//
//  LocationViewController.h
//  GuiaJK
//
//  Created by pierreabreup on 7/25/15.
//  Copyright (c) 2015 Hand Mob. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationViewController : UIViewController <CLLocationManagerDelegate, UIPickerViewDataSource,UIPickerViewDelegate>
@property (strong, nonatomic) CLLocationManager *locationManager;

@end
