//
//  ViewController.m
//  GuiaJK
//
//  Created by pierreabreup on 7/12/15.
//  Copyright (c) 2015 Hand Mob. All rights reserved.
//

#import "HomeViewController.h"
#import "HomeView.h"
#import "Flurry.h"
#import "Neighborhood.h"

@interface HomeViewController ()
  @property HomeView *homeView;
@end

@implementation HomeViewController

- (void)viewDidLoad {
  [super viewDidLoad];

  self.homeView = [[HomeView alloc] initWithFrame:CGRectMake(0,
                                                                    self.baseOriginTop,
                                                                    self.view.frame.size.width,
                                                                    self.view.frame.size.height - self.baseOriginTop)];
  [self.homeView setTag:HOME_VIEW_TAG];
  [self.homeView setUserInteractionEnabled:YES];
  [self.homeView setBackgroundColor:[UIColor clearColor]];
  [self.view addSubview:self.homeView];
  self.automaticallyAdjustsScrollViewInsets = NO;
  
  NSDictionary *context = [NSDictionary dictionaryWithObjectsAndKeys:
                                 @"Home", @"Page",
                           [[Neighborhood instance] currentNeighborhoodName],@"Neighborhood",nil];
  
  [Flurry logEvent:@"PageView" withParameters:context];
  

}


- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  
}

- (void)reloadViewElements{
  [self.homeView reloadElements];
}

- (void)viewWillAppear:(BOOL)animated{
  [self showPinButton];
}


@end
