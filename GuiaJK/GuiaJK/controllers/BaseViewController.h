//
//  BaseViewController.h
//  GuiaJK
//
//  Created by pierreabreup on 7/12/15.
//  Copyright (c) 2015 Hand Mob. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Constants.h"
#import "MenuView.h"
#import "PickerNeighborhoodView.h"

@interface BaseViewController : UIViewController <CLLocationManagerDelegate>
@property (assign,nonatomic) CGFloat baseOriginTop;
@property (strong, nonatomic) MenuView *menuView;
@property (strong, nonatomic) UIButton *neighborhoodButton;
@property (strong, nonatomic) PickerNeighborhoodView *pickerNeighborhood;
@property (assign,nonatomic) BOOL shouldLoadContent;
@property (strong, nonatomic) UIImageView *iconArrowDown;


- (void)HideMenu;
- (void)showPinButton;
- (void)showBackButton;
- (void)searchAdvertisesByCurrentLocation;
- (void)searchAdvertisesFromPickerNeighborhood;
@end

