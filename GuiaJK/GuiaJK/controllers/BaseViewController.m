//
//  BaseViewController.m
//  GuiaJK
//
//  Created by pierreabreup on 7/12/15.
//  Copyright (c) 2015 Hand Mob. All rights reserved.
//

#import "BaseViewController.h"
#import "Neighborhood.h"
#import "HomeView.h"
#import "SearchResultViewController.h"

@interface BaseViewController ()

@property BOOL pickerNeighborhoodShowed;
@property  UIImageView *iconNeighborhood;
@property CLLocationManager *locationManager;
@end

@implementation BaseViewController;
@synthesize shouldLoadContent;

- (void)viewDidLoad {
  [super viewDidLoad];
  [self setNeedsStatusBarAppearanceUpdate];
  
  self.view.backgroundColor = Rgb2UIColor(245, 245, 245);
  self.pickerNeighborhoodShowed = NO;
  
  
  if (self.shouldLoadContent){
    NSLog(@"AQUI 1");
    [self LoadConversAndCategories];
  }
  
  [self drawNavigationBar];
  [self drawNeighborhoodButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)drawNavigationBar{
  UINavigationBar *navBar = self.navigationController.navigationBar;

  
  [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

  UIImage *image = [UIImage imageNamed:@"bar-logo"];
  [navBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
  
  [self showPinButton];
  
  UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
  [menuButton setBackgroundImage:[UIImage imageNamed:@"icon-menu"] forState:UIControlStateNormal];
  [menuButton setFrame:CGRectMake(0, 0, 30, 23)];
  [menuButton addTarget:self action:@selector(showMenu) forControlEvents:UIControlEventTouchUpInside];
  self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
  
  self.baseOriginTop = (navBar.frame.origin.y + navBar.frame.size.height);
}

- (void)showPinButton{
  UIButton *geoButton = [UIButton buttonWithType:UIButtonTypeCustom];
  [geoButton setBackgroundImage:[UIImage imageNamed:@"icon-pin-geo"] forState:UIControlStateNormal];
  [geoButton setFrame:CGRectMake(0, 0, 23, 29)];
  [geoButton addTarget:self action:@selector(showUserPosition) forControlEvents:UIControlEventTouchUpInside];
  self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:geoButton];
}

- (void)showBackButton{
  UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
  [backButton setBackgroundImage:[UIImage imageNamed:@"icon-back"] forState:UIControlStateNormal];
  [backButton setFrame:CGRectMake(0, 0, 38.5, 35.5)];
  [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
  
  self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
}

- (void)backAction{
  [self.navigationController popViewControllerAnimated:YES];
}

- (void)showMenu{
  CGFloat top = self.navigationController.navigationBar.frame.origin.y;
  CGFloat widthDelta = 70;
  if (!self.menuView){
    self.menuView = [[MenuView alloc] initWithFrame:CGRectMake(self.view.frame.size.width,
                                                               top,
                                                               self.view.frame.size.width - widthDelta,
                                                               self.view.frame.size.height)];
    [self.menuView setBaseViewController:self];
    [self.menuView setBackgroundColor:Rgb2UIColorAlpha(28, 59, 113,0.9)];
    
    [[[UIApplication sharedApplication] keyWindow] addSubview:self.menuView];
  }
  CGRect menuFrame = self.menuView.frame;
  menuFrame.origin.x = widthDelta;
  
  [self animateView:self.menuView withFrame:menuFrame];
}

- (void)HideMenu{
  CGRect menuFrame = self.menuView.frame;
  menuFrame.origin.x = self.view.frame.size.width;
  
  [self animateView:self.menuView withFrame:menuFrame];
  
}

- (void)drawNeighborhoodButton{
  CGFloat neighborhoodButtonHeight = 40;
  self.neighborhoodButton = [UIButton buttonWithType:UIButtonTypeCustom];
  [self.neighborhoodButton setFrame:CGRectMake(0, self.baseOriginTop, self.view.frame.size.width, neighborhoodButtonHeight)];
  [self.neighborhoodButton setTitle:@"selecionar bairros" forState:UIControlStateNormal];
  [self.neighborhoodButton setTitleColor:[UIColor colorWithWhite:1 alpha:1] forState:UIControlStateNormal];
  [self.neighborhoodButton setBackgroundColor:Rgb2UIColor(209, 209, 209)];
  [self.neighborhoodButton addTarget:self action:@selector(showNeighborhoodPicker) forControlEvents:UIControlEventTouchUpInside];
  [self.neighborhoodButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-BdIt" size:12.f]];
  [self.view addSubview:self.neighborhoodButton];
  
  
  self.iconNeighborhood = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-neighborhood"]];
  [self.iconNeighborhood setFrame:CGRectMake(50, 5, 35, 28)];
  [self.neighborhoodButton addSubview:self.iconNeighborhood];
  
  CGSize titleSize = [[self.neighborhoodButton.titleLabel text] sizeWithAttributes:@{NSFontAttributeName:[self.neighborhoodButton.titleLabel font]}];
  
  self.iconArrowDown = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon-arrow-down"]];
  int delta = 50;
  if (self.neighborhoodButton.titleLabel.text.length < 10){
    delta = 65;
  }
  
  if (self.neighborhoodButton.titleLabel.text.length < 6){
    delta = 70;
  }
  
  [self.iconArrowDown setFrame:CGRectMake(titleSize.width + self.iconNeighborhood.frame.origin.x + self.iconNeighborhood.frame.size.width + delta,
                                     (self.neighborhoodButton.frame.size.height/2) - 5,
                                     10.5, 9.5)];
  [self.neighborhoodButton addSubview:self.iconArrowDown];
  
  self.baseOriginTop += neighborhoodButtonHeight;
}

- (void)showNeighborhoodPicker{
  if (!self.pickerNeighborhood){
    CGFloat picketHeight = 300;
    self.pickerNeighborhood = [[PickerNeighborhoodView alloc] initWithFrame:CGRectMake(0,
                                                                                       (self.baseOriginTop - picketHeight),
                                                                                       self.view.frame.size.width,
                                                                                       picketHeight)];
    [self.pickerNeighborhood setBackgroundColor:[UIColor whiteColor]];
    self.pickerNeighborhood.layer.borderColor = Rgb2UIColor(182, 182, 182).CGColor;
    self.pickerNeighborhood.layer.borderWidth = 1;
    [self.view addSubview:self.pickerNeighborhood];
  }
  
  
  
  
  if (self.pickerNeighborhoodShowed){
    self.pickerNeighborhoodShowed = NO;
    [self hideNeighborhoodPicker];
  }else{
    [self.view endEditing:YES];
    self.pickerNeighborhoodShowed = YES;
    CGRect pickerFrame = self.pickerNeighborhood.frame;
    pickerFrame.origin.y = self.baseOriginTop;
    
    [self.pickerNeighborhood addDefaultSelectedNeighborhoods];
    [self animateView:self.pickerNeighborhood withFrame:pickerFrame];
  }
}

- (void)hideNeighborhoodPicker{
  CGRect pickerFrame = self.pickerNeighborhood.frame;
  pickerFrame.origin.y = self.baseOriginTop - pickerFrame.size.height;
  
  [self animateView:self.pickerNeighborhood withFrame:pickerFrame];
  [self.view bringSubviewToFront:self.neighborhoodButton];
}

- (void)searchAdvertisesByCurrentLocation{
  if ([self.view viewWithTag:HOME_VIEW_TAG]){
    [(HomeView *)[self.view viewWithTag:HOME_VIEW_TAG] showSearchResultByLocation];
  }
}


- (void)searchAdvertisesFromPickerNeighborhood{
  [self hideNeighborhoodPicker];
  
  if ([[self.navigationController topViewController] isKindOfClass:[SearchResultViewController class]]){
    SearchResultViewController *searchResultController = (SearchResultViewController *)[self.navigationController topViewController];
    
    if ([searchResultController enableSearchLocation]){
      [searchResultController setEnableSearchLocation:NO];
      [searchResultController setByCurrentLocation:nil];
    }
    
    [searchResultController setByText:nil];
    [searchResultController setByCategory:nil];
    [searchResultController setByText:nil];
    [searchResultController setByHasPromotion:false];
    [searchResultController showIndicator];
    [searchResultController searchAdvertises];
    
    //[self performSelector:@selector(LoadConversAndCategoriesDelayed) withObject:self afterDelay:2];
  }
  else{
    NSLog(@"AQUI 2");
    [self LoadConversAndCategories];
  }
}

- (void)LoadConversAndCategoriesDelayed{
  NSLog(@"AQUI 3");
  [[self.navigationController.viewControllers objectAtIndex:1] LoadConversAndCategories];
}


- (void)LoadConversAndCategories{
  [Constants showIndicatorAtView:self.view];
  [self.view setUserInteractionEnabled:NO];
  [[Neighborhood instance] loadCoversAndCategoriesSuccess:^(AFHTTPRequestSerializer *operation, id response){
    [self setShouldLoadContent:NO];
    
    if ([response isEqual:[NSNull null]]){
      [Constants hideIndicator];
      [self.view setUserInteractionEnabled:YES];
      [Constants alertTechnicalProblems];
      [self.navigationController popViewControllerAnimated:YES];
    }else{
      [self reloadViewElements];
      
      [Constants hideIndicator];
      [self.view setUserInteractionEnabled:YES];
    }

    
    
  }failure:^(AFHTTPRequestSerializer *operation, NSError *error) {
    [Constants hideIndicator];
    [self setShouldLoadContent:NO];
    [self.view setUserInteractionEnabled:YES];
    [Constants alertTechnicalProblems];
    [self.navigationController popViewControllerAnimated:YES];
  }];
  
}

- (void)reloadViewElements{
  if ([self.view viewWithTag:HOME_VIEW_TAG]){
    [(HomeView *)[self.view viewWithTag:HOME_VIEW_TAG] reloadElements];
  }
}


- (void)animateView:(UIView *)view withFrame:(CGRect)viewFrame{
  [UIView animateWithDuration:0.2
                   animations:^{[view setFrame:viewFrame];
                   }
   
                   completion:nil];
}

- (UIBarPosition)positionForBar:(id <UIBarPositioning>)bar {
  return UIBarPositionTopAttached;
}

- (UIStatusBarStyle) preferredStatusBarStyle {
  return UIStatusBarStyleLightContent;
}

- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
  return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
  return UIInterfaceOrientationPortrait;
}

- (void)showUserPosition{
  [Constants showIndicatorAtView:self.view];
  
  self.locationManager = [[CLLocationManager alloc] init];
  [self.locationManager setDelegate:self];
  self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
  self.locationManager.distanceFilter = 500;
  
  [self.locationManager startUpdatingLocation];
  if(IS_OS_8_OR_LATER) {
    [self.locationManager requestWhenInUseAuthorization];
  }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
  [manager stopUpdatingLocation];
  NSString *pos = [NSString stringWithFormat:@"%f,%f",newLocation.coordinate.latitude,newLocation.coordinate.longitude];
  [self searchUserNeighborhood:pos];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
  CLLocation* location = [locations lastObject];
  [manager stopUpdatingLocation];
  NSString *pos = [NSString stringWithFormat:@"%f,%f",location.coordinate.latitude,location.coordinate.longitude];
  [self searchUserNeighborhood:pos];

}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
  [manager stopUpdatingLocation];
  
  [Constants alertWithMessage:@"Não foi possível obter sua posição atual. Por favor tente mais tarde."];
  [self.navigationController popViewControllerAnimated:YES];
}

/* - (void)searchUserNeighborhood:(NSString *)latlon{
AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager]; */

- (void)searchUserNeighborhood:(NSString *)latlon{
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

  NSDictionary *parameters = @{@"l": latlon};
    [manager GET:URL_SEARCH_USER_NEIGHBORHOOD parameters:nil progress: nil success:^(NSURLSessionTask *operation, id responseObject) {
    [Constants hideIndicator];
    NSDictionary *jsonData = [NSDictionary dictionaryWithDictionary: responseObject];
    
    [Constants alertWithMessage:[NSString stringWithFormat:@"Você está em %@",[jsonData objectForKey:@"msg"]]];
    
  }
    failure:^(NSURLSessionTask *operation, NSError *error) {
    [Constants hideIndicator];
    [Constants alertRequestError];
    NSLog(@"FAIL searchUserNeighborhood: %@", error);
  
   
  }];
}


@end
