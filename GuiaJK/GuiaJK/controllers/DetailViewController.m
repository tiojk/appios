//
//  DetailViewController.m
//  GuiaJK
//
//  Created by pierreabreup on 5/22/16.
//  Copyright © 2016 Hand Mob. All rights reserved.
//

#import "DetailViewController.h"
#import "DetailView.h"
#import "Neighborhood.h"

@interface DetailViewController ()
  @property DetailView *detailView;
@end

@implementation DetailViewController
@synthesize advertiseIndex, advertiseID;

- (void)viewDidLoad {
    [super viewDidLoad];
  
  [self.neighborhoodButton setHidden:YES];
  
  CGFloat detailY = self.baseOriginTop - self.neighborhoodButton.frame.size.height;
  self.detailView = [[DetailView alloc] initWithFrame:CGRectMake(0,
                                                                 detailY,
                                                                 self.view.frame.size.width,
                                                                 self.view.frame.size.height)];
  [self.detailView setUserInteractionEnabled:YES];
  [self.detailView setBackgroundColor:[UIColor clearColor]];
  
  
  if (advertiseID > 0){
    [Constants showIndicatorAtView:self.view];
    [[Neighborhood instance] setAdvertisesByCategory:nil];
    [[Neighborhood instance] setAdvertisesByText:nil];
    [[Neighborhood instance] setAdvertisesByLatLong:nil];
    [[Neighborhood instance] setAdvertiseByID:advertiseID];
    
    [[Neighborhood instance] searchAdvertisesSuccess:^(AFHTTPRequestSerializer *operation, id response){
      [Constants hideIndicator];
      
      if ([response isEqual:[NSNull null]]){
        [Constants alertTechnicalProblems];
        [self.navigationController popViewControllerAnimated:YES];
      }else{
        [self.detailView setIsDetailFromID:YES];
        [self.view addSubview:self.detailView];
      }
      
    }failure:^(AFHTTPRequestSerializer *operation, NSError *error) {
      [Constants hideIndicator];
      [Constants alertTechnicalProblems];
      [self.navigationController popViewControllerAnimated:YES];
    }];
  }
  else{
    [self.detailView setAdvertiseIndex:self.advertiseIndex];
    [self.view addSubview:self.detailView];
  }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
  [self showBackButton];
}

@end
