//
//  SearchResultViewController.m
//  GuiaJK
//
//  Created by pierreabreup on 8/2/15.
//  Copyright (c) 2015 Hand Mob. All rights reserved.
//

#import "SearchResultViewController.h"
#import "Neighborhood.h"
#import "Flurry.h"
#import "DetailOnMapView.h"
#import "CategoriesViewController.h"
#import "UIImageView+WebCache.h"

@interface SearchResultViewController ()
  @property SearchResultView *searchResultView;
  @property CGRect mapFrame;
  @property GMSMapView *gMapView;
  @property DetailOnMapView *detailOnMap;
  @property UIView *categoryArea;
  @property NSMutableArray *mapMarkers;
@end

@implementation SearchResultViewController
@synthesize byCategory,byText, byHasPromotion,enableSearchLocation,enableShowMap,selectedCategoryArea,byCurrentLocation,shouldResetFilters;

- (void)viewDidLoad {
  [super viewDidLoad];
  
  [Constants showIndicatorAtView:self.view];
  CGRect viewFrame = CGRectMake(0,
                                self.baseOriginTop,
                                self.view.frame.size.width,
                                self.view.frame.size.height - self.baseOriginTop);
  
  
  if (self.shouldResetFilters){
    [Constants resetGlobalFilters];
  }
  if (self.enableSearchLocation){
    self.locationManager = [[CLLocationManager alloc] init];
    [self.locationManager setDelegate:self];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.distanceFilter = 500;
    
    [self.locationManager startUpdatingLocation];
    self.enableShowMap = true;
    if(IS_OS_8_OR_LATER) {
      [self.locationManager requestWhenInUseAuthorization];
    }
  }
  else{
    [self searchAdvertises];
  }
  
  if (self.enableShowMap){
    self.mapMarkers = [NSMutableArray array];
    self.mapFrame = viewFrame;
  }else{
    [self addSearchResultViewAtFrame:viewFrame];
  }
  
}

- (SearchResultView *)contentView{
  return self.searchResultView;
}

- (void)addSearchResultViewAtFrame:(CGRect)searchResultViewFrame{
  self.searchResultView = [[SearchResultView alloc] initWithFrame:searchResultViewFrame];
  [self.searchResultView setUserInteractionEnabled:YES];
  [self.searchResultView setBackgroundColor:Rgb2UIColor(245, 245, 245)];
  [self.view addSubview:self.searchResultView];
  [self.view sendSubviewToBack:self.searchResultView];
}

- (void)showMapWithLat:(double)lat andLong:(double)lng{
  if (self.gMapView){
    [self.gMapView removeFromSuperview];
    self.gMapView = nil;
  }
  GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat
                                                          longitude: lng
                                                               zoom:15];
  
  self.gMapView = [GMSMapView mapWithFrame:self.mapFrame camera:camera];
  self.gMapView.myLocationEnabled = YES;
  self.gMapView.delegate = self;
  [self.view addSubview:self.gMapView];
  [self.view sendSubviewToBack:self.gMapView];
  
  if (self.enableSearchLocation && !self.categoryArea){
    self.categoryArea = [[UIView alloc] initWithFrame:CGRectMake(0, self.mapFrame.size.height + 67, self.mapFrame.size.width, 37)];
    [self.categoryArea setBackgroundColor:[UIColor redColor]];
    [self.view addSubview:self.categoryArea];
    
    UIButton *btCategory = [UIButton buttonWithType:UIButtonTypeCustom];
    [btCategory setBackgroundImage:[UIImage imageNamed:@"bt-category"] forState:UIControlStateNormal];
    [btCategory setFrame:CGRectMake(0, 0, self.categoryArea.frame.size.width, self.categoryArea.frame.size.height)];
    [btCategory addTarget:self action:@selector(showCategoriesList) forControlEvents:UIControlEventTouchUpInside];
    [self.categoryArea addSubview:btCategory];
  }
}

- (void)addMapMarkers{
  [self.gMapView clear];
  
  if (self.detailOnMap){
    [self.detailOnMap removeFromSuperview];
    self.detailOnMap = nil;
  }
  
  GMSMutablePath *path = [GMSMutablePath path];
  
  double lat = 0;
  double lng = 0;
  
  NSInteger index = 0;
  for (NSDictionary* advertise in [[Neighborhood instance] neighborhoodAdvertises]){
    GMSMarker *marker = [[GMSMarker alloc] init];
    NSInteger isPayed = 0;
    if ([[advertise objectForKey:@"payed"] boolValue]){
      [marker setIcon:[UIImage imageNamed:@"icon-ping-geo-red"]];
      isPayed = 1;
    }
    else{
      [marker setIcon:[UIImage imageNamed:@"icon-ping-geo-blue"]];
    }
    
    lat = [[[advertise objectForKey:@"location"] objectForKey:@"lat"] doubleValue];
    lng = [[[advertise objectForKey:@"location"] objectForKey:@"lon"] doubleValue];
    
    if (lat != 0 && lng != 0){
      marker.position = CLLocationCoordinate2DMake(
                                                   [[[advertise objectForKey:@"location"] objectForKey:@"lat"] doubleValue],
                                                   [[[advertise objectForKey:@"location"] objectForKey:@"lon"] doubleValue]);
      marker.title = [[advertise objectForKey:@"store"] objectForKey:@"name"];
      marker.map = self.gMapView;
      marker.userData = @{@"index":[NSNumber numberWithInteger:index], @"payed":[NSNumber numberWithInteger:isPayed]};
      
      [self.mapMarkers addObject:marker];
      
      [path addCoordinate:marker.position];
    }
    
    index += 1;
  }
  
  if (self.byCurrentLocation){
    NSArray *currentLocationComponents = [self.byCurrentLocation componentsSeparatedByString:@","];
    [path addCoordinate:CLLocationCoordinate2DMake(
                                                   [[currentLocationComponents objectAtIndex:0] doubleValue],
                                                   [[currentLocationComponents objectAtIndex:1] doubleValue])];
    
  }
  
  GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:path];
  GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds withPadding:40];
  [self.gMapView animateWithCameraUpdate:update];
}

- (void)viewWillAppear:(BOOL)animated{
  [self showBackButton];
  [self.neighborhoodButton setHidden:NO];
  
  if (self.selectedCategoryArea){
    for (UIView *v in [self.categoryArea subviews]) {
      [v removeFromSuperview];
    }
    
    UIButton *btCategory = [UIButton buttonWithType:UIButtonTypeCustom];
    [btCategory setBackgroundColor:[Constants colorFromHexString:[self.selectedCategoryArea objectForKey:@"bgcolor"]]];
    [btCategory setTitle:[self.selectedCategoryArea objectForKey:@"title"] forState:UIControlStateNormal];
    [btCategory setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btCategory setFrame:CGRectMake(0, 0, self.categoryArea.frame.size.width, self.categoryArea.frame.size.height)];
    [btCategory addTarget:self action:@selector(showCategoriesList) forControlEvents:UIControlEventTouchUpInside];
    [btCategory.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-BlkIt" size:16.f]];
    btCategory.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    btCategory.contentEdgeInsets = UIEdgeInsetsMake(3, 50, 0, 0);
    [self.categoryArea addSubview:btCategory];
    
    UIImageView *changeCategory = [[UIImageView alloc] initWithFrame:CGRectMake(btCategory.frame.size.width - 60, 8, 53.5, 22)];
    [changeCategory setImage:[UIImage imageNamed:@"icon-change-category"]];
    [btCategory addSubview:changeCategory];
    
    UIImageView *imv = [[UIImageView alloc]initWithFrame:CGRectMake(10,5, 27, 27)];
    [imv sd_setImageWithURL:[NSURL URLWithString:(NSString *)[self.selectedCategoryArea objectForKey:@"icon"]] placeholderImage:[UIImage imageNamed:@"blank"]];
    [btCategory addSubview:imv];
  }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showIndicator{
  [Constants showIndicatorAtView:self.view];
}

- (void)searchAdvertises{
  [[Neighborhood instance] setAdvertiseByID:0];
  
  NSMutableDictionary *context = [NSMutableDictionary dictionary];
  [context setObject:@"Advertises List" forKey:@"Page"];

  if (self.byCategory){
    [[Neighborhood instance] setAdvertisesByCategory:self.byCategory];
    [context setObject:self.byCategory forKey:@"byCategory"];
  }
  
  if (self.byText){
    [[Neighborhood instance] setAdvertisesByText:self.byText];
    [context setObject:self.byText forKey:@"byText"];
  }
  
  [[Neighborhood instance] setAdvertisesWithPromotion:self.byHasPromotion];
  if (self.byHasPromotion){
    [context setObject:@"YES" forKey:@"byHasPromotion"];
  }
  
  if (self.byCurrentLocation){
    [[Neighborhood instance] setAdvertisesByLatLong:self.byCurrentLocation];
    [context setObject:[[Neighborhood instance] currentNeighborhoodName] forKey:@"byLatLong"];
  }
  else{
    [[Neighborhood instance] setAdvertisesByLatLong:nil];
  }
  
  [context setObject:[[Neighborhood instance] currentNeighborhoodName] forKey:@"Neighborhood"];
  
  [Flurry logEvent:@"PageView" withParameters:context];
  
  [[Neighborhood instance] searchAdvertisesSuccess:^(AFHTTPRequestSerializer *operation, id response){
    [Constants hideIndicator];
    
    if ([response isEqual:[NSNull null]]){
      [Constants alertTechnicalProblems];
      [self.navigationController popViewControllerAnimated:YES];
    }
    else{
      if ([[[Neighborhood instance] neighborhoodAdvertises] count] == 0 && self.enableSearchLocation){
        [Constants alertWithMessage:@"Não existem anúncios perto de você."];
        [self.navigationController popViewControllerAnimated:YES];
      }
      else{
        if (self.enableShowMap){
          if (!self.enableSearchLocation){
            double lat = 0;
            double lng = 0;
            for (NSDictionary* advertise in [[Neighborhood instance] neighborhoodAdvertises]){
              lat = [[[advertise objectForKey:@"location"] objectForKey:@"lat"] doubleValue];
              lng = [[[advertise objectForKey:@"location"] objectForKey:@"lon"] doubleValue];
              
              if (lat != 0 && lng != 0){
                [self showMapWithLat:lat andLong:lng];
                break;
              }
            }
          }
          [self addMapMarkers];
        }
        else{
          [self.searchResultView reloadElements];
        }
      }
    }
    
    
    
  }failure:^(AFHTTPRequestSerializer *operation, NSError *error) {
    [Constants hideIndicator];
    [Constants alertTechnicalProblems];
    [self.navigationController popViewControllerAnimated:YES];
  }];
}

- (void)showResultAsMap{
  SearchResultViewController *searchVC = [SearchResultViewController new];
  [searchVC setByCategory:self.byCategory];
  [searchVC setByText:self.byText];
  [searchVC setByHasPromotion:self.byHasPromotion];
  [searchVC setEnableShowMap:YES];
  [self.navigationController pushViewController:searchVC animated:YES];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
  [manager stopUpdatingLocation];
  self.byCurrentLocation = [NSString stringWithFormat:@"%f,%f",newLocation.coordinate.latitude,newLocation.coordinate.longitude];
  [self showMapWithLat:newLocation.coordinate.latitude andLong:newLocation.coordinate.longitude];
  [self searchAdvertises];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
  CLLocation* location = [locations lastObject];
  [manager stopUpdatingLocation];
  self.byCurrentLocation = [NSString stringWithFormat:@"%f,%f",location.coordinate.latitude,location.coordinate.longitude];
  [self showMapWithLat:location.coordinate.latitude andLong:location.coordinate.longitude];
  [self searchAdvertises];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
  [manager stopUpdatingLocation];
  [Constants hideIndicator];
  [Constants alertWithMessage:@"Não foi possível obter sua posição atual. Por favor tente mais tarde."];
  [self.navigationController popViewControllerAnimated:YES];
}


- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
  [self showDetailAtMarker:marker];
  return YES;
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
 [self showDetailAtMarker:marker];
}

- (void)showDetailAtMarker:(GMSMarker *)selectedMarker{
  for (GMSMarker *marker in self.mapMarkers) {
    NSInteger payed = [(NSNumber *)marker.userData[@"payed"] integerValue];
    if (payed == 1){
      [marker setIcon:[UIImage imageNamed:@"icon-ping-geo-red"]];
    }else{
      [marker setIcon:[UIImage imageNamed:@"icon-ping-geo-blue"]];
    }
  }
  
  if (self.detailOnMap){
    [self.detailOnMap removeFromSuperview];
    self.detailOnMap = nil;
  }

  NSInteger index = [(NSNumber *)selectedMarker.userData[@"index"] integerValue];
  NSDictionary *advertise = [[[Neighborhood instance] neighborhoodAdvertises] objectAtIndex:index];
  
  CGFloat detailHeight = 56;
  if ([[advertise objectForKey:@"payed"] boolValue]){
    detailHeight = 102;
    [selectedMarker setIcon:[UIImage imageNamed:@"icon-pin-geo-red-dark"]];
  }
  else{
    [selectedMarker setIcon:[UIImage imageNamed:@"icon-pin-geo-blue-dark"]];
  }
  
  self.detailOnMap = [[DetailOnMapView alloc] initWithFrame:CGRectMake(0, self.baseOriginTop, self.view.frame.size.width, detailHeight)];
  [self.detailOnMap setBackgroundColor:[UIColor clearColor]];
  [self.detailOnMap setAdvertiseIndex:index];
  [self.view addSubview:self.detailOnMap];
}

- (void)showCategoriesList{
  [self.navigationController pushViewController:[CategoriesViewController new] animated:YES];
}


@end
