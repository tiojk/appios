//
//  CategoriesViewController.h
//  GuiaJK
//
//  Created by pierreabreup on 6/25/16.
//  Copyright © 2016 Hand Mob. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

#define CELL_ICON 299

@interface CategoriesViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource>

@end
