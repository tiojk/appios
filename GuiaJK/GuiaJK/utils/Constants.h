//
//  NSObject+Constants.h
//  GuiaJK
//
//  Created by pierreabreup on 7/12/15.
//  Copyright (c) 2015 Hand Mob. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PickerNeighborhoodView.h"

#define Rgb2UIColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]
#define Rgb2UIColorAlpha(r, g, b,a)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:a]
#define SumFrameYHeight(frame) frame.origin.y+frame.size.height
#define SumFrameXWidth(frame) frame.origin.x+frame.size.width

#ifdef DEBUG
      #define URL_APP_HOST "http://ec2-35-161-102-48.us-west-2.compute.amazonaws.com:8080"
    //#define URL_APP_HOST "http://35.161.102.48:8080"
    //#define URL_APP_HOST "http://192.168.0.9:3000"
#else
      #define URL_APP_HOST "http://ec2-35-161-102-48.us-west-2.compute.amazonaws.com:8080"
    //#define URL_APP_HOST "http://35.161.102.48:8080"
#endif

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define URL_SEARCH_ADVERTISES @URL_APP_HOST "/advertises/search"
#define URL_CATEGORIES_BY_AD_IDS @URL_APP_HOST "/advertises/categories_by_adids"
#define URL_AUTOCOMPLETE_SEARCH_ADVERTISES @URL_APP_HOST "/advertises/autocomplete_search"
#define URL_AUTOCOMPLETE_NEIGHBORHOOD @URL_APP_HOST "/advertises/autocomplete_neighborhood"
#define URL_NEIGHBORHOOD_COVERS_AND_CATEGORIES @URL_APP_HOST "/advertises/covers_and_categories"
#define URL_CHECK_FOR_NEIGHBORHOOD @URL_APP_HOST "/advertises/check_for_neighborhood"
#define URL_ALL_NEIGHBORHOODS @URL_APP_HOST "/advertises/neighborhoods"
#define URL_SEARCH_USER_NEIGHBORHOOD @URL_APP_HOST "/advertises/search_user_neighborhood"
#define URL_SEND_MAIL @URL_APP_HOST "/advertises/register.json"
#define URL_RATE @URL_APP_HOST "/rate.json"
#define HOME_VIEW_TAG 100

@interface Constants : NSObject
+ (void)alertWithMessage:(NSString*)message;
+ (void)showIndicatorAtView:(UIView *)view;
+ (void)hideIndicator;
+ (void)alertRequestError;
+ (void)alertTechnicalProblems;
+ (void)autocompleteResultFor:(UISearchBar *)searchbar showAbove:(BOOL)isAbove;
+ (void)autocompleteNeighborResultFor:(UISearchBar *)searchbar andPickerView:(PickerNeighborhoodView *)pickerNeigh showAbove:(BOOL)isAbove;
+ (void)removeAutocompleteResult;
+ (UIColor *)colorFromHexString:(NSString *)hexString;
+ (NSInteger)currentWeekNumber;
+ (void)resetGlobalFilters;

@end
