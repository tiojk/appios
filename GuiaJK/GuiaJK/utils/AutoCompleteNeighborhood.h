//
//  AutoCompleteNeighborhood.h
//  GuiaJK
//
//  Created by pierreabreup on 6/26/16.
//  Copyright © 2016 Hand Mob. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickerNeighborhoodView.h"

@interface AutoCompleteNeighborhood : UIView <UITableViewDataSource,UITableViewDelegate>
  @property (strong, nonatomic) UITableView *result;
  @property (strong, nonatomic) NSArray *neighborhoods;
  @property (strong, nonatomic) PickerNeighborhoodView *pickerNeighborhoodView;
@end
