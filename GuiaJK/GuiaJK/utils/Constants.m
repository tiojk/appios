//
//  NSObject+Constants.m
//  GuiaJK
//
//  Created by pierreabreup on 7/12/15.
//  Copyright (c) 2015 Hand Mob. All rights reserved.
//

#import "Constants.h"
#import "Neighborhood.h"
#import "AutoCompleteResult.h"
#import "AutoCompleteNeighborhood.h"

static UIActivityIndicatorView *loading = nil;
static AutoCompleteResult *autoCompleteView;
static AutoCompleteNeighborhood *autoCompleteNeighborhoodView;

@implementation Constants

+ (void)alertWithMessage:(NSString*)message{
  
  [[[UIAlertView alloc]initWithTitle:@"Revista O JK" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
}

+ (void)showIndicatorAtView:(UIView *)view{
  if (!loading){
    loading = [[UIActivityIndicatorView alloc]
               initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge] ;
    [loading setFrame:CGRectMake((view.frame.size.width - 150) / 2, (view.frame.size.height - 120) / 2, 150, 120)];
    [loading setColor:Rgb2UIColor(27, 116, 183)];
    [view addSubview:loading];
    [view bringSubviewToFront:loading];
    [loading startAnimating];

  }
}

+ (void)hideIndicator{
  [loading stopAnimating];
  [loading removeFromSuperview];
  loading = nil;
}

+ (void)alertRequestError{
  [self alertWithMessage:@"Desculpe-nos, mas não foi possivel conectar o GuiaJK. Por favor, verifique sua conexão com a internet ou tente mais tarde"];
}

+ (void)alertTechnicalProblems{
  [self alertWithMessage:@"Desculpe-nos, estamos temporariamente com problemas técnicos. Por favor, tente mais tarde"];
}

+ (void)autocompleteResultFor:(UISearchBar *)searchbar showAbove:(BOOL)isAbove{
  UIView *searchBarParentView = (UIView *)[(UIView *)[searchbar nextResponder] nextResponder];
  [Constants showIndicatorAtView:searchBarParentView];
  
  [self resetGlobalFilters];
  
  [[Neighborhood instance] setAdvertiseByID:0];
  [[Neighborhood instance] setAdvertisesByText:[searchbar text]];
  [[Neighborhood instance] searchAdvertisesAutocompleteSuccess:^(AFHTTPRequestSerializer *operation, id response){
    [Constants hideIndicator];
    
    if ([response isEqual:[NSNull null]]){
      [Constants alertTechnicalProblems];
    }else{
      NSInteger total = [[[Neighborhood instance] neighborhoodAdvertisesAutoComplete] count];
      if (autoCompleteView){
        [autoCompleteView removeFromSuperview];
        autoCompleteView = nil;
      }
      autoCompleteView = [[AutoCompleteResult alloc] initWithFrame:CGRectMake(searchbar.frame.origin.x + 8, 0, searchbar.frame.size.width - 15, 0)];
      [autoCompleteView setBackgroundColor:[UIColor whiteColor]];
      autoCompleteView.layer.borderWidth = 1;
      autoCompleteView.layer.borderColor = Rgb2UIColor(222, 222, 222).CGColor;
      
      CGFloat autoCompleteViewTop;
      CGFloat autoCompleteHeight = 130;
      CGFloat minimumTotal = 3;
      if (isAbove){
        autoCompleteViewTop = (searchbar.frame.origin.y - autoCompleteHeight - 120);
        if (total < minimumTotal){
          autoCompleteViewTop = autoCompleteViewTop + ((autoCompleteHeight / minimumTotal) * (minimumTotal - total));
        }
      }
      else{
        minimumTotal = 4;
        autoCompleteViewTop = (SumFrameYHeight(searchbar.frame) + autoCompleteHeight - 30);
        autoCompleteHeight += 40;
      }
      
      if (total < minimumTotal){
        autoCompleteHeight = (autoCompleteHeight / minimumTotal) * total;
      }
      
      CGRect frame = autoCompleteView.frame;
      frame.origin.y = autoCompleteViewTop;
      frame.size.height = autoCompleteHeight;
      [autoCompleteView setFrame:frame];
      [searchBarParentView addSubview:autoCompleteView];
    }
    
  }failure:^(AFHTTPRequestSerializer *operation, NSError *error) {
    [Constants hideIndicator];
    [Constants alertTechnicalProblems];
  }];
}

+ (void)autocompleteNeighborResultFor:(UISearchBar *)searchbar andPickerView:(PickerNeighborhoodView *)pickerNeigh showAbove:(BOOL)isAbove{
  UIView *searchBarParentView = (UIView *)[(UIView *)[searchbar nextResponder] nextResponder];
  [Constants showIndicatorAtView:searchBarParentView];
  
  
  [[Neighborhood instance] searchNeighborhoodAutocomplete:[searchbar text] success:^(AFHTTPRequestSerializer *operation, id response){
    [Constants hideIndicator];
    
    if ([response isEqual:[NSNull null]]){
      [Constants alertTechnicalProblems];
    }else{
      NSArray *result = (NSArray *)response;
      NSInteger total = [result count];
      if (autoCompleteNeighborhoodView){
        [autoCompleteNeighborhoodView removeFromSuperview];
        autoCompleteNeighborhoodView = nil;
      }
      autoCompleteNeighborhoodView = [[AutoCompleteNeighborhood alloc] initWithFrame:CGRectMake(searchbar.frame.origin.x + 8, 0, searchbar.frame.size.width - 15, 0)];
      [autoCompleteNeighborhoodView setNeighborhoods:result];
      [autoCompleteNeighborhoodView setPickerNeighborhoodView:pickerNeigh];
      [autoCompleteNeighborhoodView setBackgroundColor:[UIColor whiteColor]];
      autoCompleteNeighborhoodView.layer.borderWidth = 1;
      autoCompleteNeighborhoodView.layer.borderColor = Rgb2UIColor(222, 222, 222).CGColor;
      
      CGFloat autoCompleteViewTop;
      CGFloat autoCompleteHeight = 130;
      CGFloat minimumTotal = 3;
      if (isAbove){
        autoCompleteViewTop = (searchbar.frame.origin.y - autoCompleteHeight - 120);
        if (total < minimumTotal){
          autoCompleteViewTop = autoCompleteViewTop + ((autoCompleteHeight / minimumTotal) * (minimumTotal - total));
        }
      }
      else{
        minimumTotal = 4;
        autoCompleteViewTop = (SumFrameYHeight(searchbar.frame) + autoCompleteHeight - 30);
        autoCompleteHeight += 40;
      }
      
      if (total < minimumTotal){
        autoCompleteHeight = (autoCompleteHeight / minimumTotal) * total;
      }
      
      CGRect frame = autoCompleteNeighborhoodView.frame;
      frame.origin.y = autoCompleteViewTop;
      frame.size.height = autoCompleteHeight;
      [autoCompleteNeighborhoodView setFrame:frame];
      [searchBarParentView addSubview:autoCompleteNeighborhoodView];
    }
    
  }failure:^(AFHTTPRequestSerializer *operation, NSError *error) {
    [Constants hideIndicator];
    [Constants alertTechnicalProblems];
  }];
}

+ (void)removeAutocompleteResult{
  if (autoCompleteView){
    [autoCompleteView removeFromSuperview];
    autoCompleteView = nil;
  }
  if (autoCompleteNeighborhoodView){
    [autoCompleteNeighborhoodView removeFromSuperview];
    autoCompleteNeighborhoodView = nil;
  }
}

+ (NSInteger)currentWeekNumber{
  NSCalendar *calendar = [NSCalendar currentCalendar];
  NSInteger weekNumber = [[calendar components:(NSCalendarUnitWeekday) fromDate:[NSDate date]] weekday];
  if (weekNumber == 1){
    weekNumber = 6;
  }
  else{
    weekNumber = (weekNumber - 2);
  }
  return weekNumber;
}

+ (UIColor *)colorFromHexString:(NSString *)hexString {
  unsigned rgbValue = 0;
  NSScanner *scanner = [NSScanner scannerWithString:hexString];
  [scanner setScanLocation:1]; // bypass '#' character
  [scanner scanHexInt:&rgbValue];
  return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

+ (void)resetGlobalFilters{
  [[Neighborhood instance] setAdvertisesWithPromotion: NO];
  [[Neighborhood instance] setAdvertisesWithOpened: NO];
  [[Neighborhood instance] setAdvertisesWithMoreDistance: NO];
  [[Neighborhood instance] setAdvertisesWithDelivery:NO];
  [[Neighborhood instance] setAdvertisesSort: nil];
  [[Neighborhood instance] setAdvertisesMaxDistance: 0];
}
@end
