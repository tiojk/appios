//
//  AutoCompleteNeighborhood.m
//  GuiaJK
//
//  Created by pierreabreup on 6/26/16.
//  Copyright © 2016 Hand Mob. All rights reserved.
//

#import "AutoCompleteNeighborhood.h"
#import "Constants.h"

@implementation AutoCompleteNeighborhood
@synthesize neighborhoods,result,pickerNeighborhoodView;

- (void)drawRect:(CGRect)rect {
  self.result = [[UITableView alloc] initWithFrame:CGRectMake(0,0,
                                                              self.frame.size.width,
                                                              self.frame.size.height) style:UITableViewStyleGrouped];
  self.result.delegate = self;
  self.result.dataSource = self;
  self.result.sectionHeaderHeight = 0.0;
  self.result.sectionFooterHeight = 0.0;
  [self.result setBackgroundColor:[UIColor whiteColor]];
  [self addSubview:result];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  return [neighborhoods count];
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
  if (section == 0)
    return CGFLOAT_MIN;
  
  return tableView.sectionHeaderHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *CellIdentifier = @"autoCompleteNeighCell";
  
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
  }
  
  cell.textLabel.text = [neighborhoods objectAtIndex:indexPath.row];
  [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:10.f]];
  [cell.textLabel setTextColor:Rgb2UIColor(114, 114, 114)];
  
  return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  [self.pickerNeighborhoodView addNeighborsForSearchByName:[neighborhoods objectAtIndex:indexPath.row]];
}

@end
