//
//  AppDelegate.m
//  GuiaJK
//
//  Created by pierreabreup on 7/12/15.
//  Copyright (c) 2015 Hand Mob. All rights reserved.
//

#import "AppDelegate.h"
#import "LocationViewController.h"
#import "BaseNavigationController.h"
#import "Flurry.h"
//#import "TestFairy.h"

@import GoogleMaps;


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  //[TestFairy begin:@"2cbeb33502b5ad7639b5614a424d4b9a62be39f6"];
  
  [GMSServices provideAPIKey:@"AIzaSyA9U-LeaBPxUQudf4ruk6F2LR5PILbmct4"];
  
  //antigo
  //[Flurry startSession:@"GKPC2SPPR8GFHDDMGNXP"];
  
  [Flurry startSession:@"47699PZW6QC696XQHDT6"];
  
  [application setStatusBarHidden:NO];
  [application setStatusBarStyle:UIStatusBarStyleLightContent];
  
  LocationViewController *locationVC = [[LocationViewController alloc]initWithNibName:nil bundle:nil];
  
  self.navigation = [[BaseNavigationController alloc]initWithRootViewController:locationVC];
  self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
  self.window.rootViewController = self.navigation;
  self.window.backgroundColor = Rgb2UIColor(245, 245, 245);
  [self.window makeKeyAndVisible];
  return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {

}

- (void)applicationDidEnterBackground:(UIApplication *)application {

}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {

}

- (void)applicationWillTerminate:(UIApplication *)application {

}

@end
